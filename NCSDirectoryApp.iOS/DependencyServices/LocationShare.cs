﻿using System;
using CoreLocation;
using Foundation;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.iOS.DependencyServices;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(LocationShare))]
namespace NCSDirectoryApp.iOS.DependencyServices
{
    public class LocationShare : ILocSettings
    {
        public bool isGpsAvailable()
        {
            bool status = CLLocationManager.LocationServicesEnabled;
            return status;
            //how to check the GPS is on or off here
        }
        public void OpenSettings()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(UIApplication.OpenSettingsUrlString));
        }
    }
}