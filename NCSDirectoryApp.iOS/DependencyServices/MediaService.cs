﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Foundation;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.iOS.DependencyServices;
using NCSDirectoryApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using UIKit;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(MediaService))]
namespace NCSDirectoryApp.iOS.DependencyServices
{
	public class MediaService : IMediaService
	{
		public async Task OpenGallery()
		{
			var picker = ELCImagePickerViewController.Create(maxImages: 5);
			picker.MaximumImagesCount = 5;

			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;
			while (topController.PresentedViewController != null)
			{
				topController = topController.PresentedViewController;
			}
			topController.PresentViewController(picker, true, null);

			await picker.Completion.ContinueWith(t =>
			{
				picker.BeginInvokeOnMainThread(() =>
				{
					//dismiss the picker
					picker.DismissViewController(true, null);

					if (t.IsCanceled || t.Exception != null)
					{
					}
					else
					{
						ObservableCollection<BusinessPhotos> images = new ObservableCollection<BusinessPhotos>();

						var items = t.Result as List<Plugin.Media.Abstractions.MediaFile>;
						foreach (var item in items)
						{
							//var path = Save(item.i, item.Name);
							images.Add(new BusinessPhotos() { FilePath = item.Path, UploadedImageSource = item.Path });
							//CleanPath(path);
						}

						MessagingCenter.Send<ObservableCollection<BusinessPhotos>>(images, "ImagesSelected");
					}
				});
			});
		}

		string Save(UIImage image, string name)
		{
			var documentsDirectory = Environment.GetFolderPath
								  (Environment.SpecialFolder.Personal);
			string jpgFilename = System.IO.Path.Combine(documentsDirectory, name); // hardcoded filename, overwritten each time
			NSData imgData = image.AsJPEG();
			NSError err = null;
			if (imgData.Save(jpgFilename, false, out err))
			{
				return jpgFilename;
			}
			else
			{
				Console.WriteLine("NOT saved as " + jpgFilename + " because" + err.LocalizedDescription);
				return null;
			}
		}
	}
}