﻿using System;
using System.Threading.Tasks;
using Foundation;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.iOS.DependencyServices;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(Statusbar))]
namespace NCSDirectoryApp.iOS.DependencyServices
{
    public class Statusbar : IStatusBar
    {
        public Statusbar()
        {
        }

        public void DarkStatusBarColor()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                {
                    UIView statusBar = new UIView(UIApplication.SharedApplication.KeyWindow.WindowScene.StatusBarManager.StatusBarFrame);
                    statusBar.BackgroundColor = UIColor.FromRGB(27, 55, 139);
                    UIApplication.SharedApplication.KeyWindow.AddSubview(statusBar);
                }
                else
                {
                    UIView statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBarWindow")).ValueForKey(new NSString("statusBar")) as UIView;
                    statusBar.BackgroundColor = UIColor.FromRGB(27, 55, 139);
                }
                UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
                GetCurrentViewController().SetNeedsStatusBarAppearanceUpdate();
            });
        }

        public void SetLightStatusBarColor()
        {
            //UIApplication.SharedApplication.StatusBarHidden = true;
            if (App.IsHomePageAppear)
                DarkStatusBarColor();
            else
                LightTheme();
        }

        public void LightTheme()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
            {
                UIView statusBar = new UIView(UIApplication.SharedApplication.KeyWindow.WindowScene.StatusBarManager.StatusBarFrame);
                statusBar.BackgroundColor = UIColor.FromRGB(252, 252, 252);
                UIApplication.SharedApplication.KeyWindow.AddSubview(statusBar);
            }
            else
            {
                UIView statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBarWindow")).ValueForKey(new NSString("statusBar")) as UIView;
                statusBar.BackgroundColor = UIColor.FromRGB(252, 252, 252);
            }
            Device.BeginInvokeOnMainThread(() =>
            {
                UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.Default, false);
                GetCurrentViewController().SetNeedsStatusBarAppearanceUpdate();
            });
        }

        UIViewController GetCurrentViewController()
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
                vc = vc.PresentedViewController;
            return vc;
        }
    }
}
