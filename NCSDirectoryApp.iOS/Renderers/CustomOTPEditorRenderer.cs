﻿using System;
using NCSDirectoryApp.iOS.Renderers;
using NCSDirectoryApp.Views.CustomViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomOtpEditor), typeof(CustomOTPEditorRenderer))]
namespace NCSDirectoryApp.iOS.Renderers
{
    public class CustomOTPEditorRenderer : EditorRenderer
    {
        public CustomOTPEditorRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
                Control.TintColor = UIColor.White;

        }
    }
}