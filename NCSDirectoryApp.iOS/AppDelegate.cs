﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreLocation;
using FFImageLoading;
using FFImageLoading.Forms.Platform;
using Foundation;
using NCSDirectoryApp.Utilities;
using UIKit;
using Xamarin.Essentials;

namespace NCSDirectoryApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        CLLocationManager locMan;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            Rg.Plugins.Popup.Popup.Init();
            CachedImageRenderer.Init();
            CachedImageRenderer.InitImageSourceHandler();

            var config = new FFImageLoading.Config.Configuration()
            {
                VerboseLogging = false,
                VerbosePerformanceLogging = false,
                VerboseMemoryCacheLogging = false,
                VerboseLoadingCancelledLogging = false,
                Logger = new CustomLogger(),
            };
            ImageService.Instance.Initialize(config);
            Xamarin.FormsMaps.Init();
            this.locMan = new CLLocationManager
            {
                ActivityType = CLActivityType.OtherNavigation,
                DesiredAccuracy = 1,
                DistanceFilter = 1,
                PausesLocationUpdatesAutomatically = false
            };
            this.locMan.RequestAlwaysAuthorization();
            this.locMan.RequestWhenInUseAuthorization();
            this.locMan.StartMonitoringSignificantLocationChanges();
            App.DeviceHeight = UIScreen.MainScreen.Bounds.Height;
            App.DeviceWidth = UIScreen.MainScreen.Bounds.Width;
            VersionTracking.Track();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

        public class CustomLogger : FFImageLoading.Helpers.IMiniLogger
        {
            public void Debug(string message)
            {
                Console.WriteLine(message);
            }

            public void Error(string errorMessage)
            {
                Console.WriteLine(errorMessage);
            }

            public void Error(string errorMessage, Exception ex)
            {
                Error(errorMessage + System.Environment.NewLine + ex.ToString());
            }
        }

        [Export("applicationWillTerminate:")]
        public void WillTerminate(UIApplication application)
        {
            Constants.SetIsLogin(false);
            Constants.SaveUserData(null);
            Constants.SaveRegisterUserId(null);
        }
    }
}
