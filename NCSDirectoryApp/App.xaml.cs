﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models.BusinessModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.Views.Authentication;
using Plugin.Connectivity;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp
{
    public partial class App : Application
    {
        public static App CurrentInstance { get; set; }
        public static bool IsInternet { get; set; }
        public static bool isLocationPermission { get; set; }
        public static string SelectedPage { get; set; }
        public static List<ParentCategoryDetails> AllBusinessParentCategories { get; set; }
        public static List<ChildCategoryDetails> AllBusinessSubCategories { get; set; }
        public static bool IsFromForgotPassword { get; set; }
        public static bool IsFromSearch { get; set; } = false;
        public static bool IsPopupOpen { get; set; } = false;
        public static double DeviceHeight { get; set; }
        public static double DeviceWidth { get; set; }
        public static bool IsHomePageAppear { get; set; } = true;
        public static List<ChildCategoryDetails> HomeScreenCategories { get; set; }
        public static bool IsFirstTimeLauch { get; set; } = true;
        public static byte[] LogoImageBytes { get; set; } = null;
        public static double userLat { get; set; }
        public static double userLong { get; set; }

        public App()
        {
            InitializeComponent();
            CurrentInstance = this;
            HomeScreenCategories = new List<ChildCategoryDetails>();
            MainPage = new NavigationPage(new MainPage());
            Task.Run(async () => await CheckInternetConnection()).Wait();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                try
                {
                    if (args.IsConnected)
                    {
                        IsInternet = true;
                        if (IsHomePageAppear)
                            MessagingCenter.Send(EventArgs.Empty, "ReloadHomePage");
                    }
                    else
                    {
                        IsInternet = false;
                        await Helper.DisplayAlert("Connection Lost! Please check your internet connaction.");
                    }
                }
                catch (Exception ex)
                {
                    IsInternet = false;
                }
            };
        }
        async Task CheckInternetConnection()
        {
            try
            {
                IsInternet = CrossConnectivity.Current.IsConnected;
                
            }
            catch (Exception ex)
            {
                IsInternet = false;
            }
        }

        public async Task LoadLoginPage()
        {
            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

        public async Task LoadHomePage()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
        }

        public async Task LoadAccountSetting()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
        }

        public async Task CheckLocation()
        {
            var hasPermission = await Utils.CheckPermissions(Permission.Location);
            if (!hasPermission)
                return;
            if (Device.RuntimePlatform == Device.Android)
            {
                bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                if (!gpsStatus)
                {
                    await Helper.DisplayAlert("Please turn on your location.!");
                    DependencyService.Get<ILocSettings>().OpenSettings();
                }
            }
        }

        public async Task InitializeLocation()
        {
            try
            {
                IGeolocator locator = Plugin.Geolocator.CrossGeolocator.Current;
                locator.DesiredAccuracy = 5;
                if (!locator.IsListening)
                {
                    await locator.StartListeningAsync(TimeSpan.FromSeconds(10), 10, true, new ListenerSettings
                    {
                        ActivityType = ActivityType.AutomotiveNavigation,
                        AllowBackgroundUpdates = true,
                        DeferLocationUpdates = true,
                        DeferralDistanceMeters = 1,
                        DeferralTime = TimeSpan.FromSeconds(1),
                        ListenForSignificantChanges = false,
                        PauseLocationUpdatesAutomatically = false,
                    });
                }
                locator.PositionChanged += PositionChanged;
                locator.PositionError += PositionError;
            }
            catch (Exception ex)
            { }
        }

        public void PositionChanged(object sender, PositionEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    //If updating the UI, ensure you invoke on main thread
                    var position = e.Position;
                    var output = "Full: Lat: " + position.Latitude + " Long: " + position.Longitude;
                    output += "\n" + $"Time: {position.Timestamp}";
                    output += "\n" + $"Heading: {position.Heading}";
                    output += "\n" + $"Speed: {position.Speed}";
                    output += "\n" + $"Accuracy: {position.Accuracy}";
                    output += "\n" + $"Altitude: {position.Altitude}";
                    output += "\n" + $"Altitude Accuracy: {position.AltitudeAccuracy}";
                    userLat = position.Latitude;
                    userLong = position.Longitude;
                    //App.userLat = -33.90411630834143;
                    //App.userLong = 18.648686395971804;
                    Console.WriteLine("Location Changed : Lat. => " + userLat + " Long. => " + userLong);
                }
                catch (Exception ex)
                {
                }
            });
        }

        private void PositionError(object sender, PositionErrorEventArgs e)
        {
        }

        protected override void OnStart()
        {
            var firstLaunch = VersionTracking.IsFirstLaunchEver;
            IsFirstTimeLauch = firstLaunch;
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
