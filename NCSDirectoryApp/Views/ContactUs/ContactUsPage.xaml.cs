﻿using NCSDirectoryApp.ViewModels.ContactUs;
using NCSDirectoryApp.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.ContactUs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactUsPage : BaseContentPage
    {
        ContactUsPageViewModel viewModel;
        public ContactUsPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ContactUsPageViewModel();
        }
    }
}