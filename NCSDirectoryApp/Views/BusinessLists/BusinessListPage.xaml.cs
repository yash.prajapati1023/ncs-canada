﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.BusinessLists;
using NCSDirectoryApp.Views.Base;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.BusinessLists
{
    public partial class BusinessListPage : BaseContentPage
    {
        BusinessListPageViewModel viewModel;

        public BusinessListPage(ChildCategoryDetails categoryDetails)
        {
            InitializeComponent();
            App.IsFromSearch = false;
            BindingContext = viewModel = new BusinessListPageViewModel(categoryDetails);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.BusinessList = new ObservableCollection<BusinessData>();
            await viewModel.LoadBusinessList(1);
        }

        async void ListView_ItemAppearing(System.Object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            await viewModel.ItemAppearanceAction(e.Item);
        }

        protected override bool OnBackButtonPressed()
        {
            if (App.IsPopupOpen)
            {
                PopupNavigation.Instance.PopAsync();
                App.IsPopupOpen = false;
                return true;
            }
            else if (App.IsFromSearch)
            {
                viewModel.PageCount = 1;
                viewModel.LoadBusinessList(1);
                App.IsFromSearch = false;
                return true;
            }
            else
                return false;
        }
    }
}
