﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.BusinessLists;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.BusinessLists
{
    public partial class UsersBusinessListPage : BaseContentPage
    {
        UsersBusinessListPageViewModel viewModel;
        public UsersBusinessListPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new UsersBusinessListPageViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            viewModel.BusinessList = new ObservableCollection<BusinessData>();
            await viewModel.LoadBusinessList(1);
        }

        async void ListView_ItemAppearing(System.Object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            await viewModel.ItemAppearanceAction(e.Item);
        }

        protected override bool OnBackButtonPressed()
        {
            App.CurrentInstance.LoadHomePage();
            return true;
        }
    }
}
