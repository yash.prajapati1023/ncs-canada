﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Home;
using NCSDirectoryApp.Views.Base;
using NCSDirectoryApp.Views.SearchPage;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Home
{
    public partial class HomePage : BaseContentPage
    {
        HomePageViewModel viewModel;

        public HomePage()
        {
            InitializeComponent();
            BindingContext = viewModel = new HomePageViewModel();
            App.IsHomePageAppear = true;
            MessagingCenter.Subscribe<EventArgs>(this, "ReloadHomePage", async (args) =>
            {
                await viewModel.GetAllBusinessList();
            });

            listBusinessFrame.PropertyChanged += (s, e) =>
            {
                var frame = s as Frame;
                if (e.PropertyName.Equals(nameof(Width)))
                {
                    listBusinessLabel.FontSize = (int)frame.Width / 19;
                }
            };
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            App.IsHomePageAppear = true;
            if (Device.RuntimePlatform == Device.Android)
            {
                bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                if (!gpsStatus)
                {
                    await Helper.DisplayAlert("Please turn on your location.!", true);
                }
                else
                    await viewModel.GetAllBusinessList();
            }
            else
                await viewModel.GetAllBusinessList();

            await App.CurrentInstance.InitializeLocation();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsHomePageAppear = false;
        }

        async void OnSearchEntryFocused(System.Object sender, Xamarin.Forms.FocusEventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new SearchViewPage());
        }
    }
}
