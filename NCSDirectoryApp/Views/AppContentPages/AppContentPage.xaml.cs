﻿using NCSDirectoryApp.ViewModels.AppContentPages;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.AppContentPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppContentPage : BaseContentPage
    {
        public AppContentPageViewModel viewModel;

        public AppContentPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new AppContentPageViewModel();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.SetPageContent();
        }
    }
}