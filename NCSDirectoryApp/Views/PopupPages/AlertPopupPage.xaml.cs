﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.PopupPages;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.PopupPages
{
    public partial class AlertPopupPage : PopupPage
    {
        AlertPopupViewModel viewModel;
        

        public AlertPopupPage(string message, bool isLoginFirstAlert = false)
        {
            InitializeComponent();
            BindingContext = viewModel = new AlertPopupViewModel(message, isLoginFirstAlert);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsPopupOpen = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsPopupOpen = false;
        }
    }
}
