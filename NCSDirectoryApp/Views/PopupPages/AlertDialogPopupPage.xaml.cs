﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Enums;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.BusinessLists;
using NCSDirectoryApp.ViewModels.PopupPages;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.PopupPages
{
    public partial class AlertDialogPopupPage : PopupPage
    {
        AlertDialogPopupViewModel viewModel;
        public AlertDialogPopupPage(BusinessData business, UsersBusinessListPageViewModel pageViewModel, PageType pageType = PageType.Default)
        {
            InitializeComponent();
            BindingContext = viewModel = new AlertDialogPopupViewModel(business, pageViewModel, pageType);
        }
    }
}