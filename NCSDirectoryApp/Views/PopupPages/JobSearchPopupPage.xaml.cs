﻿using NCSDirectoryApp.ViewModels.JobList;
using NCSDirectoryApp.ViewModels.PopupPages;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.PopupPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobSearchPopupPage : PopupPage
    {
        JobSearchViewModel viewModel;
        JobListPageViewModel jobListPageViewModel;
        public JobSearchPopupPage(JobListPageViewModel _viewModel)
        {
            InitializeComponent();
            this.jobListPageViewModel = _viewModel;
            BindingContext = viewModel = new JobSearchViewModel(_viewModel);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsPopupOpen = true;
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsPopupOpen = false;
            //await jobListPageViewModel.ApplyJobSearch();
        }
    }
}