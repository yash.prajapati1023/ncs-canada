﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.BusinessLists;
using NCSDirectoryApp.ViewModels.PopupPages;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.PopupPages
{
    public partial class BusinessSearchPopupPage : PopupPage
    {
        BusinessSearchViewModel viewModel;
        public BusinessSearchPopupPage(BusinessListPageViewModel _viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel = new BusinessSearchViewModel(_viewModel);
            viewModel.PageTitle = "Search " + _viewModel.PageTitle;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            App.IsPopupOpen = true;
            var hasPermission = await Utils.CheckPermissions(Permission.Location);
            if (Device.RuntimePlatform == Device.Android)
            {
                bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                if (!gpsStatus)
                {
                    await Helper.DisplayAlert("Please turn on your location.!");
                    DependencyService.Get<ILocSettings>().OpenSettings();
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsPopupOpen = false;
        }
    }
}