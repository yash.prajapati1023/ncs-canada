﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.ViewModels.PopupPages;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.PopupPages
{
    public partial class FullScreenImagePopupPage : PopupPage
    {
        FullScreenImageViewModel viewModel;
        public FullScreenImagePopupPage(ObservableCollection<BusinessPhotos> photosList, BusinessPhotos selectedPhoto)
        {
            InitializeComponent();
            BindingContext = viewModel = new FullScreenImageViewModel(photosList, selectedPhoto);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsPopupOpen = true;
            viewModel.LoadPhotosList();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsPopupOpen = false;
        }
    }
}
