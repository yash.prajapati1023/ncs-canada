﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.BusinessListing;
using NCSDirectoryApp.ViewModels.PopupPages;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace NCSDirectoryApp.Views.PopupPages
{
    public partial class SelectCategoryPopupPage : PopupPage
    {

        SelectCategoryPopupViewModel viewModel;

        public SelectCategoryPopupPage(ObservableCollection<ChildCategoryDetails> subCategoryList, BusinessListingPageViewModel viewPage)
        {
            InitializeComponent();
            BindingContext = viewModel = new SelectCategoryPopupViewModel(subCategoryList, viewPage);

            popupView.PropertyChanged += (s, e) =>
            {
                var button = s as PancakeView;
                if (e.PropertyName.Equals(nameof(Height)))
                {
                    if (button.Height > App.DeviceHeight / 1.6)
                        popupView.HeightRequest = App.DeviceHeight / 1.6;
                }
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.LoadCategoryList();
            //var isAnySeleted = viewModel.BusinessSubCategoryList.Any(a => a.IsSelected);
            //if (isAnySeleted)
            //{
            //    viewModel.SelectedSubCategory = viewModel.BusinessSubCategoryList.Where(a => a.IsSelected).FirstOrDefault();
            //    //if(viewModel.SelectedSubCategory != null)
            //    //    categoryListView.ScrollTo(viewModel.SelectedSubCategory, ScrollToPosition.Center, false);
            //}
        }
    }
}
