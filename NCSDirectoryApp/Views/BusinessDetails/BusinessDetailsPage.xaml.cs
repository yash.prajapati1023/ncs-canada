﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.BusinessDetails;
using NCSDirectoryApp.Views.Base;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.BusinessDetails
{
    public partial class BusinessDetailsPage : BaseContentPage
    {
        BusinessDetailsPageViewModel viewModel;
        public BusinessDetailsPage(BusinessData business)
        {
            InitializeComponent();
            BindingContext = viewModel = new BusinessDetailsPageViewModel(business);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.SetBusinessData();
        }

        protected override bool OnBackButtonPressed()
        {
            if (App.IsPopupOpen)
            {
                PopupNavigation.Instance.PopAsync();
                App.IsPopupOpen = false;
                return true;
            }
            else
                return false;
        }
    }
}
