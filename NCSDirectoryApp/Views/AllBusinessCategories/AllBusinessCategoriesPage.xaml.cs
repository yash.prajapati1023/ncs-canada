﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.AllBusinessCategories;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.AllBusinessCategories
{
    public partial class AllBusinessCategoriesPage : BaseContentPage
    {
        AllBusinessCategoriesViewModel viewModel;
        public AllBusinessCategoriesPage()
        {
            InitializeComponent();
            this.BindingContext = viewModel = new AllBusinessCategoriesViewModel();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.ShowAllCategories();
        }
    }
}
