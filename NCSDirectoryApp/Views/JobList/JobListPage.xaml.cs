﻿using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.ViewModels.JobList;
using NCSDirectoryApp.Views.Base;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.JobList
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobListPage : BaseContentPage
    {
        JobListPageViewModel viewModel;
        public JobListPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new JobListPageViewModel();
        }

        private async void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await viewModel.ItemAppearanceAction(e.Item);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.JobsList = new ObservableCollection<JobDetails>();
            await viewModel.ShowAllJobsList(1);
        }

        protected override bool OnBackButtonPressed()
        {
            if (App.IsPopupOpen)
            {
                PopupNavigation.Instance.PopAsync();
                App.IsPopupOpen = false;
                return true;
            }
            else if (App.IsFromSearch)
            {
                viewModel.Latitude = viewModel.Longitude = viewModel.Radius = viewModel.SearchKeyword = null;
                viewModel.PageCount = 1;
                viewModel.ShowAllJobsList(1);
                App.IsFromSearch = false;
                return true;
            }
            else
                return false;
        }
    }
}