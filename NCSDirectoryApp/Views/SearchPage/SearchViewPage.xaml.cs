﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.SearchPage;
using NCSDirectoryApp.Views.Base;
using NCSDirectoryApp.Views.PopupPages;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.SearchPage
{
    public partial class SearchViewPage : BaseContentPage
    {
        SearchViewPageViewModel viewModel;
        public SearchViewPage()
        {
            InitializeComponent();
            this.BindingContext = viewModel = new SearchViewPageViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var hasPermission = await Utils.CheckPermissions(Permission.Location);
            if (!hasPermission)
                return;
            if (Device.RuntimePlatform == Device.Android)
            {
                bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                if (!gpsStatus)
                {
                    await Helper.DisplayAlert("Please turn on your location.!");
                    DependencyService.Get<ILocSettings>().OpenSettings();
                }
            }
            
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 100;

            var position = await locator.GetLastKnownLocationAsync();

            if (position != null)
            {
                viewModel.Latitude = position.Latitude.ToString();
                viewModel.Longitude = position.Longitude.ToString();
            }
            searchEntry.Focus();
        }

        async void ListView_ItemAppearing(System.Object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            await viewModel.ItemAppearanceAction(e.Item);
        }
        public string tempString = "";
        void SearchTextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            //Entry entry = (Entry)sender;
            //if ((e.NewTextValue.Length + tempString.Length) % 3 == 0)
            //{
            //    tempString = e.NewTextValue;
            //    Console.WriteLine("Old Value: " + e.OldTextValue);
            //    Console.WriteLine("New Value: " + e.NewTextValue);

            //    viewModel.LoadBusinessList(1, entry.Text);
            //}
        }

        async void SearchTextChangeCompleted(System.Object sender, System.EventArgs e)
        {
            //Entry entry = (Entry)sender;
            //await Task.Run(() => Loading.IsLoading(true));
            //await viewModel.LoadBusinessList(1, entry.Text);
            //await Task.Run(() => Loading.IsLoading(false));
        }
    }
}
