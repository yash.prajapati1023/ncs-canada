﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.ViewModels.BusinessListing;
using NCSDirectoryApp.Views.Base;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.BusinessListing
{
    public partial class BusinessListingPage : BaseContentPage
    {
        BusinessListingPageViewModel viewModel;
        public BusinessListingPage(BusinessData business = null)
        {
            InitializeComponent();
            BindingContext = viewModel = new BusinessListingPageViewModel(business);
            viewModel.EditorAutoSizeType = EditorAutoSizeOption.TextChanges;
            businessAboutEdtr.PropertyChanged += (s, e) =>
            {
                var editor = s as Editor;
                if (e.PropertyName.Equals(nameof(Height)))
                {
                    if (editor.Height > 160)
                        viewModel.EditorHeight = 160;
                    else
                        viewModel.EditorHeight = editor.Height;
                }
            };
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.SetBusinessCategories();
            if(viewModel.SelectedBusinessData != null)
            {
                await viewModel.LoadBusinessData();
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            
        }

    }
}