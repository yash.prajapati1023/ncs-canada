﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.Master;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Master
{
    public partial class SideMenuPage : ContentPage
    {
        SideMenuPageViewModel viewModel;

        public SideMenuPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SideMenuPageViewModel();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.SetSideMenu();
        }
    }
}
