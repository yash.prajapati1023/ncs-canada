﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationViewWithSearch : Grid
    {
        public NavigationViewWithSearch()
        {
            InitializeComponent();
        }
    }
}