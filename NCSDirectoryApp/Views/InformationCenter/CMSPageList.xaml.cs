﻿using NCSDirectoryApp.ViewModels.InformationCenter;
using NCSDirectoryApp.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.InformationCenter
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CMSPageList : BaseContentPage
    {

        CMSPageListViewModel viewModel;
        public CMSPageList()
        {
            InitializeComponent();
            BindingContext = viewModel = new CMSPageListViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.LoadPageList();
        }
    }
}