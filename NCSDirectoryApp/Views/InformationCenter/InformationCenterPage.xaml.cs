﻿using NCSDirectoryApp.Models;
using NCSDirectoryApp.ViewModels.InformationCenter;
using NCSDirectoryApp.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.InformationCenter
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InformationCenterPage : BaseContentPage
    {
        InformationCenterViewModel viewModel;

        public InformationCenterPage(CMSPageData cMSPage)
        {
            InitializeComponent();
            BindingContext = viewModel = new InformationCenterViewModel(cMSPage);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.CallUsefulItemApi(viewModel.CurrentPage.page_id);
        }
    }
}