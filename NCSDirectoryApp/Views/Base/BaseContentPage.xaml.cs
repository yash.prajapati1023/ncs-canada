﻿using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Resources;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace NCSDirectoryApp.Views.Base
{
    public partial class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            InitializeComponent();
            On<iOS>().SetUseSafeArea(true);
            //this.BackgroundImageSource = ImageStrings.background_image;
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //DependencyService.Get<IStatusBar>().SetLightStatusBarColor();
        }
    }
}
