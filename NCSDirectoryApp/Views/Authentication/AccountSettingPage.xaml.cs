﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.Authentication;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Authentication
{
    public partial class AccountSettingPage : BaseContentPage
    {
        AccountSettingViewModel viewModel;
        public AccountSettingPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new AccountSettingViewModel();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.GetUserData();
        }

        protected override bool OnBackButtonPressed()
        {
            App.CurrentInstance.LoadHomePage();
            return true;
        }
    }
}
