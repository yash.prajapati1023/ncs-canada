﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.Authentication;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Authentication
{
    public partial class SignupPage : BaseContentPage
    {
        SignupPageViewModel viewModel;

        public SignupPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SignupPageViewModel();
        }
    }
}
