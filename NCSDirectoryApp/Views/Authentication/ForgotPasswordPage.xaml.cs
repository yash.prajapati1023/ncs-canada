﻿using NCSDirectoryApp.ViewModels.Authentication;
using NCSDirectoryApp.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NCSDirectoryApp.Views.Authentication
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPage : BaseContentPage
    {
        ForgotPasswordViewModel viewModel;
        public ForgotPasswordPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ForgotPasswordViewModel();
        }
    }
}