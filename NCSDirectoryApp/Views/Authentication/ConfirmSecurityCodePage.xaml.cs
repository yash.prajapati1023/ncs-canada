﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.ViewModels.Authentication;
using NCSDirectoryApp.Views.Base;
using NCSDirectoryApp.Views.CustomViews;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Authentication
{
    public partial class ConfirmSecurityCodePage : BaseContentPage
    {
        ConfirmSecurityCodeViewModel viewModel;
        List<Label> labels;

        public ConfirmSecurityCodePage(string otp)
        {
            InitializeComponent();
            BindingContext = viewModel = new ConfirmSecurityCodeViewModel(otp);
            labels = new List<Label>();
            labels.Add(OtpCode1);
            labels.Add(OtpCode2);
            labels.Add(OtpCode3);
            labels.Add(OtpCode4);
        }

        void OTP_Editor_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            CustomOtpEditor editor = (CustomOtpEditor)sender;

            string editorStr = editor.Text;
            if (!string.IsNullOrWhiteSpace(editorStr))
            {
                if (editorStr.Length > 4)
                {
                    editor.Text = editorStr.Substring(0, 4);
                }
                if (editorStr.Length >= 4)
                    editor.Unfocus();
                for (int i = 0; i < labels.Count; i++)
                {
                    Label lb = labels[i];

                    if (i < editorStr.Length)
                        lb.Text = editorStr.Substring(i, 1);
                    else
                        lb.Text = "";
                }
            }
            else
                for (int i = 0; i < labels.Count; i++)
                {
                    Label lb = labels[i];
                    lb.Text = "";
                }
        }

        void OnFocused_OtpEditor(System.Object sender, Xamarin.Forms.FocusEventArgs e)
        {
            var editor = (CustomOtpEditor)sender;
            string editorText = editor.Text;
            if (string.IsNullOrWhiteSpace(editorText))
            {
                viewModel.OtpCode1TextColor = ColorStrings.dark_red;
                viewModel.OtpCode2TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode3TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode4TextColor = ColorStrings.dark_blue;
            }
            else if (editorText.Length == 1)
            {
                viewModel.OtpCode1TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode2TextColor = ColorStrings.dark_red;
                viewModel.OtpCode3TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode4TextColor = ColorStrings.dark_blue;
            }
            else if (editorText.Length == 2)
            {
                viewModel.OtpCode1TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode2TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode3TextColor = ColorStrings.dark_red;
                viewModel.OtpCode4TextColor = ColorStrings.dark_blue;
            }
            else
            {
                viewModel.OtpCode1TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode2TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode3TextColor = ColorStrings.dark_blue;
                viewModel.OtpCode4TextColor = ColorStrings.dark_red;
            }
        }

        void TextChangeCompleted_OtpEditor(System.Object sender, System.EventArgs e)
        {
            viewModel.OtpCode1TextColor = ColorStrings.dark_blue;
            viewModel.OtpCode2TextColor = ColorStrings.dark_blue;
            viewModel.OtpCode3TextColor = ColorStrings.dark_blue;
            viewModel.OtpCode4TextColor = ColorStrings.dark_blue;
        }
    }
}