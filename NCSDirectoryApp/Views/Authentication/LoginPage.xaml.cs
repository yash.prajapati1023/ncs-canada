﻿using System;
using System.Collections.Generic;
using NCSDirectoryApp.ViewModels.Authentication;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Views.Authentication
{
    public partial class LoginPage : BaseContentPage
    {
        LoginPageViewModel viewModel;
        public LoginPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new LoginPageViewModel();
        }

        protected override bool OnBackButtonPressed()
        {
            App.CurrentInstance.LoadHomePage();
            return true;
        }
    }
}