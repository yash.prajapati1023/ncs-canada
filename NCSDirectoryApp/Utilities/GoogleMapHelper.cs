﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace NCSDirectoryApp.Utilities
{
	public static class GoogleMapHelper
	{
		#region Navigation
		public static async Task ShowNavigationBuiltInMapAsync(double latitude, double longitude)
		{
			// ReSharper disable CompareOfFloatsByEqualityOperator
			if (latitude == 0 || longitude == 0)
				return;
			// ReSharper restore CompareOfFloatsByEqualityOperator


			var location = new Location(latitude, longitude);
			var options = new MapLaunchOptions { NavigationMode = NavigationMode.Driving };

			await Map.OpenAsync(location, options);
		}
		#endregion
	}
}