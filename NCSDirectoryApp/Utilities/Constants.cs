﻿using System;
using Xamarin.Essentials;

namespace NCSDirectoryApp.Utilities
{
    public static class Constants
    {
        public static void SaveRegisterUserId(string id)
        {
            Preferences.Set("RegisterUserId", id);
        }

        public static string GetRegisterUserId()
        {
            return Preferences.Get("RegisterUserId", null);
        }

        public static void SaveUserData(string userData)
        {
            Preferences.Set("UserData", userData);
        }

        public static string GetUserData()
        {
            return Preferences.Get("UserData", null);
        }

        public static void SetIsLogin(bool isLogin)
        {
            Preferences.Set("IsLoggedIn", isLogin);
        }

        public static bool GetIsLogin()
        {
            return Preferences.Get("IsLoggedIn", false);
        }

        public static void SaveHomeScreenCategories(string categories)
        {
            Preferences.Set("HomeScreenCategories", categories);
        }

        public static string GetHomeScreenCategories()
        {
            return Preferences.Get("HomeScreenCategories", null);
        }
    }
}
