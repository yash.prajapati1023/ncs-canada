﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.UserDialogs;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.BusinessModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.Utilities
{
    public static class Helper
    {

        #region Method
        public async static Task DisplayAlert(string message, bool isLoginAlert = false)
        {
            //await Application.Current.MainPage.DisplayAlert(TextStrings.app_name_display, message, "OK");
            await PopupNavigation.Instance.PushAsync(new AlertPopupPage(message, isLoginAlert));
        }

        public static bool VerifyEmailAddress(string emailAddress)
        {
            try
            {
                string email = emailAddress.Trim();
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (match.Success)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public async static Task<List<BusinessSubCategory>> GetAllBusinessSubCategories()
        {
            //var result = await ApiService.GetAsync(ApiPathStrings.get_all_business_categories_url);
            //if (!string.IsNullOrWhiteSpace(result.Item2))
            //{
            //    if(result.Item1.ToLower() == "OK".ToLower())
            //    {
            //        BusinessCategoriesResponse response = JsonConvert.DeserializeObject<BusinessCategoriesResponse>(result.Item2);
            //    }
            //}
            //BusinessCategory requirement = new BusinessCategory();
            //requirement.BusinessCategoryName = "Recruitment and HR Hospital & Clinics Transport";
            //requirement.BusinessSubCategories = new List<BusinessSubCategory>()
            //{
            //    new BusinessSubCategory(){ BusinessSubCategoryName = "Jobs", SubCategoryIcon = ImageStrings.ic_job }
            //};
            //BusinessCategory travel = new BusinessCategory();
            //travel.BusinessCategoryName = "Travel";
            //travel.BusinessSubCategories = new List<BusinessSubCategory>()
            //{
            //    new BusinessSubCategory(){ BusinessSubCategoryName = "Travels", SubCategoryIcon = ImageStrings.ic_job }
            //};
            //BusinessCategory automotive = new BusinessCategory();
            //automotive.BusinessCategoryName = "Automotive";
            //automotive.BusinessSubCategories = new List<BusinessSubCategory>()
            //{
            //    new BusinessSubCategory(){ BusinessSubCategoryName = "IT Services", SubCategoryIcon = ImageStrings.ic_it_services }
            //};
            //BusinessCategory personalCare = new BusinessCategory();
            //personalCare.BusinessCategoryName = "Personal Care";
            //personalCare.BusinessSubCategories = new List<BusinessSubCategory>()
            //{
            //    new BusinessSubCategory(){ BusinessSubCategoryName = "Food", SubCategoryIcon = ImageStrings.ic_food },
            //    new BusinessSubCategory(){ BusinessSubCategoryName = "Groceries", SubCategoryIcon = ImageStrings.ic_groceries }
            //};
            //App.AllBusinessCategories = new List<BusinessCategory>();
            //App.AllBusinessCategories.Add(requirement);
            //App.AllBusinessCategories.Add(travel);
            //App.AllBusinessCategories.Add(automotive);
            //App.AllBusinessCategories.Add(personalCare);
            //App.AllBusinessCategories.Add(requirement);
            //App.AllBusinessCategories.Add(travel);
            //App.AllBusinessCategories.Add(automotive);
            //App.AllBusinessCategories.Add(personalCare);
            //App.AllBusinessCategories.Add(requirement);
            //App.AllBusinessCategories.Add(travel);
            //App.AllBusinessCategories.Add(automotive);
            //App.AllBusinessCategories.Add(personalCare);
            //App.AllBusinessCategories.Add(requirement);
            //App.AllBusinessCategories.Add(travel);
            //App.AllBusinessCategories.Add(automotive);
            //App.AllBusinessCategories.Add(personalCare);
            //App.AllBusinessCategories.Add(requirement);
            //App.AllBusinessCategories.Add(travel);
            //App.AllBusinessCategories.Add(automotive);
            //App.AllBusinessCategories.Add(personalCare);
            //List<BusinessSubCategory> allSubCategories = new List<BusinessSubCategory>();
            //foreach (var category in App.AllBusinessCategories)
            //{
            //    foreach (var subCategory in category.BusinessSubCategories)
            //    {
            //        allSubCategories.Add(subCategory);
            //    }
            //}
            return null;
        }
        #endregion
    }
}
