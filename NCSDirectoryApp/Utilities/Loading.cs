﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using NCSDirectoryApp.Views.PopupPages;
using Rg.Plugins.Popup.Services;

namespace NCSDirectoryApp.Utilities
{
    public class Loading
    {
        public static async Task IsLoading(bool flag)
        {
            if (flag)
            {
                //UserDialogs.Instance.ShowLoading("Loading..");
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
            }
            else
            {
                //UserDialogs.Instance.HideLoading();
                await PopupNavigation.Instance.PopAsync(true);
            }
        }
    }
}
