﻿using System;
namespace NCSDirectoryApp.Models.RequestModels
{
    public class ResendOtpRequest
    {
        public string id { get; set; }
        public string isForgot { get; set; }
    }
}
