﻿using System;
namespace NCSDirectoryApp.Models.RequestModels
{
    public class RegisterRequest
    {
        public string email { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
    }

    public class LoginRequest
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
