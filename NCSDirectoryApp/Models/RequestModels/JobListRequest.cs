﻿using System;
namespace NCSDirectoryApp.Models.RequestModels
{
    public class JobListRequest
    {
        public string lat { get; set; }
        public string _long { get; set; }
        public string radius { get; set; }
        public string search { get; set; }
        public string cat_id { get; set; }
        public string page { get; set; }
    }
}
