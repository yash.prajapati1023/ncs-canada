﻿using System;
namespace NCSDirectoryApp.Models.RequestModels
{
    public class ChangePasswordRequest
    {
        public string id { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
        public string profile_image { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}
