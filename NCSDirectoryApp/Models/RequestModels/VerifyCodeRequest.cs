﻿using System;
namespace NCSDirectoryApp.Models.RequestModels
{
    public class VerifyCodeRequest
    {
        public string user_id { get; set; }
        public string otp { get; set; }
    }
}
