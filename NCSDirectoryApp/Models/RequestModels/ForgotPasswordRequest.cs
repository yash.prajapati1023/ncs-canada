﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCSDirectoryApp.Models.RequestModels
{
    public class ForgotPasswordRequest
    {
        public string phone_email { get; set; }
    }

    public class VerifyForgotPasswordRequest
    {
        public string user_id { get; set; }
        public string otp { get; set; }
        public string password { get; set; }
    }
}
