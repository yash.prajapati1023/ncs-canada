﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCSDirectoryApp.Models.RequestModels
{
    public class ContactUsRequest
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
    }
}
