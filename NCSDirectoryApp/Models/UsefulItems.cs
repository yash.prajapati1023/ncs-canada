﻿using NCSDirectoryApp.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace NCSDirectoryApp.Models
{
    public class UsefulItems
    {
        public string UsefulItemTitle { get; set; }
        public string UsefulItemIcon { get; set; }
        public string UsefulLink { get; set; }
        public string Displayname
        {
            get
            {
                var result = UsefulItemTitle.EndsWith("/") ? UsefulItemTitle.Substring(0, UsefulItemTitle.Length - 1) : UsefulItemTitle;
                if(result.Contains("http://") || result.Contains("https://"))
                    result = result.Replace("http://", "").Replace("https://", "");
                return result;
            }
        }
    }

    public class CMSPageData
    {
        public string page_id { get; set; }
        public string menu_id { get; set; }
        public string page_title { get; set; }
        public string content { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_by { get; set; }
        public string created_on { get; set; }
        public string PageIconSource
        {
            get
            {
                if (page_title.ToLower() == "Useful Phone Numbers".ToLower() || page_title.ToLower().Contains("Phone".ToLower()))
                    return ImageStrings.ic_smartphone;
                else
                    return ImageStrings.ic_globe;
            }
        }
    }

    public class CMSPageResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<CMSPageData> data { get; set; }
    }
}
