﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NCSDirectoryApp.Resources;
using Xamarin.Forms;

namespace NCSDirectoryApp.Models
{
    public class MenuItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName]string propertyName= null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string MenuTitle { get; set; }

        private Color _menuTextColor = ColorStrings.dark_gray;
        public Color MenuTextColor
        {
            get { return _menuTextColor; }
            set
            {
                _menuTextColor = value;
                OnPropertyChanged(nameof(MenuTextColor));
            }
        }

        private string _menuIcon;
        public string MenuIcon
        {
            get { return _menuIcon; }
            set
            {
                _menuIcon = value;
                OnPropertyChanged(nameof(MenuIcon));
            }
        }

        private bool _isMenuSelected;
        public bool IsMenuSelected
        {
            get { return _isMenuSelected; }
            set
            {
                _isMenuSelected = value;
                OnPropertyChanged(nameof(IsMenuSelected));
                if (value)
                    this.MenuTextColor = ColorStrings.dark_blue;
                else
                    this.MenuTextColor = ColorStrings.dark_gray;
            }
        }
    }
}
