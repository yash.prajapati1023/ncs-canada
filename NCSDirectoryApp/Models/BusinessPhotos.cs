﻿using System;
using Xamarin.Forms;

namespace NCSDirectoryApp.Models
{
    public class BusinessPhotos
    {
        public ImageSource UploadedImageSource { get; set; }
        public string FilePath { get; set; }
        public byte[] ImageBytes { get; set; }
    }
}
