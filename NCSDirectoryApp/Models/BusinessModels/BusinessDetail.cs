﻿using System;
using System.Collections.Generic;

namespace NCSDirectoryApp.Models.BusinessModels
{
    public class BusinessDetail
    {
        
    }

    public class BusinessSubCategory
    {
        public string BusinessSubCategoryName { get; set; }
        public string SubCategoryIcon { get; set; }
    }

    public class BusinessCategory
    {
        public string BusinessCategoryName { get; set; }
        public List<BusinessSubCategory> BusinessSubCategories { get; set; }
    }
}
