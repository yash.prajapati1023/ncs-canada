﻿using System;
namespace NCSDirectoryApp.Models.ResponseModels
{
    public class ResponseModel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public bool data { get; set; }
    }
}
