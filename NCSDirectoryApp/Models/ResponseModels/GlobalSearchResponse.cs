﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class SearchData
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public string category_id { get; set; }
        public string business_name { get; set; }
        public string business_about { get; set; }
        public string business_logo { get; set; }
        public string upload_photos { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string contact_person { get; set; }
        public string contact_number { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalcode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string category_name { get; set; }
        public string IsJob { get; set; }
        public string job_title { get; set; }
        public string employer_name { get; set; }
        public string job_detail { get; set; }
        public string job_description { get; set; }
        public string salary { get; set; }
        public string experience { get; set; }
        public string education { get; set; }
        public int? job_type { get; set; }
        public string job_key { get; set; }
        public string address { get; set; }
        public string postal_code { get; set; }
        public string category { get; set; }
        public string upload_logo { get; set; }
        public int? parent_id { get; set; }
        public int? reviewed { get; set; }
        public string portal { get; set; }
        public string expireddate { get; set; }
        public int? latlongupdated { get; set; }
        public string TotalDays { get; set; }
        public byte[] LogoBytes { get; set; }
        public string distance { get; set; }
        public ObservableCollection<BusinessPhotos> BusinessPhotosList { get; set; }
        public DateTime CreatedDate
        {
            get
            {
                return Convert.ToDateTime(this.created_at, CultureInfo.InvariantCulture);
            }
        }
    }

    public class GlobalSearchResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string page { get; set; }
        public List<SearchData> data { get; set; }
    }
}
