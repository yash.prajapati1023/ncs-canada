﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class BusinessListResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string page { get; set; }
        public List<BusinessData> data { get; set; }
    }

    public class BusinessData
    {
        public string id { get; set; }
        public string business_id { get; set; }
        public string user_id { get; set; }
        public string category_id { get; set; }
        public string business_name { get; set; }
        public string business_about { get; set; }
        public string business_logo { get; set; }
        public string upload_photos { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string contact_person { get; set; }
        public string contact_number { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalcode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string category_name { get; set; }
        public string TotalDays { get; set; }
        public string distance { get; set; }
        public byte[] LogoBytes { get; set; }
        public ObservableCollection<BusinessPhotos> BusinessPhotosList { get; set; }
        public DateTime CreatedDate
        {
            get
            {
                return Convert.ToDateTime(this.created_at, CultureInfo.InvariantCulture);
            }
        }
    }
}
