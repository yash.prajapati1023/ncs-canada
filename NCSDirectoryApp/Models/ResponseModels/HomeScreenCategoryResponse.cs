﻿using System;
using System.Collections.Generic;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class HomeScreenCategoryResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<ChildCategoryDetails> data { get; set; }
    }
}
