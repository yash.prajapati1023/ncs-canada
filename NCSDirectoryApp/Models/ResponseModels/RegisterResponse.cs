﻿using System;
namespace NCSDirectoryApp.Models.ResponseModels
{
    public class RegisterResponse
    {
        public bool status { get; set; }
        public string otp { get; set; }
        public int id { get; set; }
        public string message { get; set; }
        public bool status2 { get; set; }
        public string message2 { get; set; }
    }
}
