﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class AppContentPageData
    {
        public string page_id { get; set; }
        public string menu_id { get; set; }
        public string page_title { get; set; }
        public string content { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_by { get; set; }
        public string created_on { get; set; }
    }

    public class AppContentPageResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public AppContentPageData data { get; set; }
    }
}
