﻿using System;
namespace NCSDirectoryApp.Models.ResponseModels
{
    public class LoginResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string id { get; set; }
        public User data { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string profile_image { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string otp { get; set; }
        public string device_token { get; set; }
        public string wallet { get; set; }
        public string first_login { get; set; }
        public string created_by { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string fullName
        {
            get { return this.firstname + " " + this.lastname; }
        }
    }
}