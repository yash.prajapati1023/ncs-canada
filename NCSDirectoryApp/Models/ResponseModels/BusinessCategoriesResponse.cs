﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.ViewModels.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class BusinessCategoriesResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public ListOfParentCategories data { get; set; }
    }
    public class ListOfParentCategories
    {
        public List<ParentCategoryDetails> level_one { get; set; }
    }
    public class ParentCategoryDetails : INotifyPropertyChanged
    {
        public string id { get; set; }
        public string parent_id { get; set; }
        public string category_name { get; set; }
        public string category_icon { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_by { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object parent_category_name { get; set; }
        public List<ChildCategoryDetails> level_two { get; set; }

        private Color _parentCategoryTextColor = ColorStrings.dark_gray;
        public Color ParentCatTextColor
        {
            get { return _parentCategoryTextColor; }
            set
            {
                _parentCategoryTextColor = value;
                OnPropertyChanged(nameof(ParentCatTextColor));
            }
        }

        private bool _isCategorySelected = false;
        public bool IsCategorySelected
        {
            get { return _isCategorySelected; }
            set
            {
                _isCategorySelected = value;
                OnPropertyChanged(nameof(IsCategorySelected));
                if (value)
                    this.ParentCatTextColor = ColorStrings.dark_blue;
                else
                    this.ParentCatTextColor = ColorStrings.dark_gray;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ChildCategoryDetails : BaseViewModel
    {
        public string id { get; set; }
        public string parent_id { get; set; }
        public string category_name { get; set; }
        public string category_icon { get; set; }
        public string is_active { get; set; }
        public string is_delete { get; set; }
        public string created_by { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string parent_category_name { get; set; }
        public DateTime UpdatedDate { get; set; }
        private string _selectIconImage = ImageStrings.ic_radio_un_fill;
        public string SelectIconImage
        {
            get { return _selectIconImage; }
            set
            {
                _selectIconImage = value;
                OnPropertyChanged(nameof(SelectIconImage));
            }
        }
        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
            }
        }
    }
}
