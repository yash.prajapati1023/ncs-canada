﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCSDirectoryApp.Models.ResponseModels
{
    public class JobsListResponse
    {
        public int total { get; set; }
        public int per_page { get; set; }
        public int current_page { get; set; }
        public int last_page { get; set; }
        public string next_page_url { get; set; }
        public object prev_page_url { get; set; }
        public int from { get; set; }
        public int to { get; set; }
        public List<JobDetails> data { get; set; }
    }

    public class JobDetails
    {
        public int id { get; set; }
        public string job_title { get; set; }
        public string employer_name { get; set; }
        public string job_detail { get; set; }
        public string job_description { get; set; }
        public string salary { get; set; }
        public string experience { get; set; }
        public string education { get; set; }
        public int job_type { get; set; }
        public string job_key { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postal_code { get; set; }
        public string category { get; set; }
        public object upload_logo { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public int parent_id { get; set; }
        public int is_delete { get; set; }
        public int is_active { get; set; }
        public int reviewed { get; set; }
        public string portal { get; set; }
        public object expireddate { get; set; }
    }
}