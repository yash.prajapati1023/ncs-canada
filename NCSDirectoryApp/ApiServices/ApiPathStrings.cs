﻿using System;
namespace NCSDirectoryApp.ApiServices
{
    public static class ApiPathStrings
    {
        //Live
        public static string base_url = "http://newcomersjobs.com/ncjapp/admin/api";

        //Temporary Server
        //public static string base_url = "http://boldy.in/ncjapp/admin/api";

        public static string registration_url = "/authentication/registration";
        public static string login_url = "/authentication/login";
        public static string verify_registration_otp_url = "/authentication/verifyregisteropt";
        public static string change_password = "/authentication/changepassword";
        public static string resend_otp = "/authentication/resendopt";
        public static string forgot_password = "/authentication/forgotpassword";
        public static string verify_forgot_password_otp = "/authentication/verifyforgotpasswordopt";
        public static string user_details_by_id_url = "/authentication/user/";
        
        public static string about_us_url = "/data/aboutus";
        public static string terms_of_services_url = "/data/termsofservice";
        public static string privacy_policy_url = "/data/privacypolicy";
        public static string usefulphonenumbers_url = "/data/usefulphonenumbers";
        public static string usefulwebsites_url = "/data/usefulwebsites";
        public static string contact_us_url = "/data/contactus";

        public static string get_job_list_url = "/data/getjobslist";
        public static string get_all_business_categories_url = "/data/getallbusinesscategories";

        public static string get_business_by_category = "/data/getbusinesslistbycat/";
        public static string free_business_listing = "/data/freebusinesslisting";
        public static string businessListSearchByLatLong = "/data/getBusinessListSearchByLatLong";
        public static string businessListByUserId = "/data/getbusinesslistbyuser/";

        public static string searchBusinessListing = "/data/getBusinessListSearchByLatLong";
        public static string deleteBusiness = "/data/deletebusiness/";

        public static string getHomeScreenCategories = "/data/gethomescreencategories";
        public static string globalSearch = "/data/globalsearch";

        public static string cmsPagesUrl = "/data/cmspages";
        
    }
}
