﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using RestSharp;
using Rg.Plugins.Popup.Services;

namespace NCSDirectoryApp.ApiServices
{
    public class ApiService
    {
        #region ResponseModelClass
        public class ResponseModel
        {
            public HttpStatusCode StatusCode { get; set; }
            public string Content { get; set; }
        }
        #endregion

        #region static Variable
        private static HttpClient BaseHttpClient
        {
            get
            {
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback += (sender, certificate, chain, errors) => { return true; };
                HttpClient client = new HttpClient(handler);
                client.Timeout = TimeSpan.FromSeconds(58);
                client.BaseAddress = new Uri(ApiPathStrings.base_url);
                client.DefaultRequestHeaders.Add("x-api-key", "admin@123");
                return client;
            }
        }
        #endregion

        #region Methods
        public static async Task<ResponseModel> Get(string methodName)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                HttpResponseMessage response = await BaseHttpClient.GetAsync(methodName);
                responseModel.Content = response.Content.ReadAsStringAsync().Result;
                responseModel.StatusCode = response.StatusCode;
                return responseModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return responseModel;
            }
        }
        public static async Task<ResponseModel> Post(string methodName, FormUrlEncodedContent formContent)
        {

            ResponseModel responseModel = new ResponseModel();
            try
            {
                HttpResponseMessage response = await BaseHttpClient.PostAsync(methodName, formContent);
                responseModel.Content = response.Content.ReadAsStringAsync().Result;
                responseModel.StatusCode = response.StatusCode;
                return responseModel;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return responseModel;
            }
        }


        public static async Task<ResponseModel> Post(string methodName, BusinessData business)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                MultipartFormDataContent content = new MultipartFormDataContent();
                content.Add(new StringContent(business.user_id), "user_id");
                content.Add(new StringContent(business.category_id), "category_id");
                content.Add(new StringContent(business.business_name), "business_name");
                if (!string.IsNullOrWhiteSpace(business.business_id))
                    content.Add(new StringContent(business.business_id), "business_id");
                content.Add(new StringContent(business.business_about), "business_about");
                if (!string.IsNullOrWhiteSpace(business.business_logo))
                    content.Add(new ByteArrayContent(File.ReadAllBytes(business.business_logo)), "business_logo", "filename.ext");
                if (!string.IsNullOrWhiteSpace(business.website))
                    content.Add(new StringContent(business.website), "website");

                if (business.BusinessPhotosList != null && business.BusinessPhotosList.Count > 0)
                {
                    for (int i = 0; i < business.BusinessPhotosList.Count; i++)
                    {
                        content.Add(new ByteArrayContent(File.ReadAllBytes(business.BusinessPhotosList[i].FilePath)), "upload_photos[]", "filename_" + i + ".ext");
                    }
                }
                content.Add(new StringContent(business.email), "email");
                content.Add(new StringContent(business.contact_person), "contact_person");
                content.Add(new StringContent(business.contact_number), "contact_number");
                content.Add(new StringContent(business.address_line1), "address_line1");

                if (!string.IsNullOrWhiteSpace(business.address_line2))
                    content.Add(new StringContent(business.address_line2), "address_line2");
                content.Add(new StringContent(business.city), "city");
                if (!string.IsNullOrWhiteSpace(business.province))
                    content.Add(new StringContent(business.province), "province");
                content.Add(new StringContent(business.postalcode), "postalcode");
                content.Add(new StringContent(business.latitude), "latitude");
                content.Add(new StringContent(business.longitude), "longitude");
                content.Add(new StringContent(business.is_active), "is_active");
                HttpResponseMessage response = await BaseHttpClient.PostAsync(methodName, content);
                responseModel.Content = response.Content.ReadAsStringAsync().Result;
                responseModel.StatusCode = response.StatusCode;
                return responseModel;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return responseModel;
            }
        }


        public static async Task<(string, string)> PostAsync(string methodName, string request_param)
        {
            var client = new RestClient(ApiPathStrings.base_url + methodName);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-api-key", "admin@123");
            request.AddHeader("Content-Type", "application/json");
            //string request_param = await formContent.ReadAsStringAsync();
            request.AddParameter("application/json", request_param, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //await PopupNavigation.Instance.PopAsync();
            Console.WriteLine(response.Content);
            return (response.StatusCode.ToString(), response.Content);
        }


        public static async Task<(string, string)> PostBusinessListingData(string methodName, BusinessData business)
        {
            var client = new RestClient("http://newcomersjobs.com/ncjapp/admin/api/data/freebusinesslisting");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-api-key", "admin@123");
            request.AddHeader("Cookie", "visid_incap_2465325=IYARObRDQP2G8FtHl41jfTOClWAAAAAAQUIPAAAAAAB7uoWrLMLINT6PIkRrCI+f; ci_session=1df1d955b059de8703670e074a4b3f8f0a1eb58a");

            request.AddParameter("user_id", business.user_id);
            request.AddParameter("category_id", business.category_id);
            request.AddParameter("business_name", business.business_name);
            request.AddParameter("business_id", business.business_id);
            request.AddParameter("id", business.business_id);

            request.AddParameter("business_about", business.business_about);
            //request.AddFile("business_logo", business.business_logo);
            if (!string.IsNullOrWhiteSpace(business.business_logo))
                request.AddFile("business_logo", File.ReadAllBytes(business.business_logo), "business_logo.jpg");
            request.AddParameter("website", business.website);
            if (business.BusinessPhotosList != null && business.BusinessPhotosList.Count > 0)
            {
                //request.AddFile("upload_photos[]", photo.FilePath);
                for (int i = 0; i < business.BusinessPhotosList.Count; i++)
                {
                    if (!string.IsNullOrWhiteSpace(business.BusinessPhotosList[i].FilePath))
                        request.AddFile("upload_photos[]", File.ReadAllBytes(business.BusinessPhotosList[i].FilePath), "business_photo_" + i + ".jpg");
                }
            }
            request.AddParameter("email", business.email);
            request.AddParameter("contact_person", business.contact_person);
            request.AddParameter("contact_number", business.contact_number);
            request.AddParameter("address_line1", business.address_line1);
            request.AddParameter("address_line2", business.address_line2);
            request.AddParameter("city", business.city);
            request.AddParameter("province", business.province);
            request.AddParameter("postalcode", business.postalcode);
            request.AddParameter("latitude", business.latitude);
            request.AddParameter("longitude", business.longitude);
            request.AddParameter("is_active", business.is_active);
            IRestResponse response = client.Execute(request);
            //await PopupNavigation.Instance.PopAsync();
            Console.WriteLine(response.Content);
            return (response.StatusCode.ToString(), response.Content);
        }



        public static async Task<(string, string)> PostAccountSettingData(string methodName, ChangePasswordRequest user)
        {
            var client = new RestClient(ApiPathStrings.base_url + methodName);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-api-key", "admin@123");
            request.AddParameter("id", user.id);
            if (!string.IsNullOrWhiteSpace(user.profile_image))
                request.AddFile("profile_image", File.ReadAllBytes(user.profile_image), "user_profile.jpg");
                
            request.AddParameter("firstName", user.firstName);
            request.AddParameter("lastName", user.lastName);
            request.AddParameter("oldPassword", user.oldPassword);
            request.AddParameter("newPassword", user.newPassword);
            request.AddParameter("confirmPassword", user.confirmPassword);
            IRestResponse response = client.Execute(request);
            //await PopupNavigation.Instance.PopAsync();
            Console.WriteLine(response.Content);
            return (response.StatusCode.ToString(), response.Content);
        }

        public static async Task<(string, string)> GetAsync(string methodName)
        {
            var client = new RestClient(ApiPathStrings.base_url + methodName);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-api-key", "admin@123");
            request.AlwaysMultipartFormData = true;
            IRestResponse response = client.Execute(request);
            //await PopupNavigation.Instance.PopAsync();
            Console.WriteLine(response.Content);
            return (response.StatusCode.ToString(), response.Content);
        }
        #endregion
    }
}