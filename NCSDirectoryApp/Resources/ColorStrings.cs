﻿using System;
using Xamarin.Forms;

namespace NCSDirectoryApp.Resources
{
    public static class ColorStrings
    {
        public static Color dark_blue = Color.FromHex("#1B378B");
        public static Color black = Color.FromHex("#000000");
        public static Color dark_gray = Color.FromHex("#474747");
        public static Color dark_red = Color.FromHex("#C8202E");
        public static Color light_red = Color.FromHex("#C8202EC2");
        public static Color light_blue = Color.FromHex("#1B378B8C");
        public static Color gray = Color.FromHex("#B4B5B7");
    }
}