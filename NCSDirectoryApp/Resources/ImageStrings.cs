﻿using System;
namespace NCSDirectoryApp.Resources
{
    public static class ImageStrings
    {
        public static string background_image = "background_image.png";
        public static string bottom_background = "bottom_bg.png";
        public static string ic_back_arrow = "ic_back_arrow.png";
        public static string ic_search_dark = "ic_search_dark.png";
        public static string ic_search_light = "ic_search_light.png";

        public static string ic_user = "ic_user.png";
        public static string ic_plus_white = "ic_plus_white.png";

        //Login Page
        public static string ic_email = "ic_email.png";
        public static string ic_login_title_blue = "ic_login_title_blue.png";
        public static string ic_password = "ic_password.png";
        public static string login_arrow_white = "login_arrow_white.png";
        public static string password_hide = "password_hide.png";
        public static string password_show = "password_show.png";

        //signup page
        public static string ic_phone = "ic_phone.png";

        //Change Password
        public static string ic_save = "ic_save.png";

        //Side Menu Page
        public static string side_menu_logo = "side_menu_logo.png";
        public static string ic_about_dark = "ic_about_dark.png";
        public static string ic_about_light = "ic_about_light.png";
        public static string ic_briefcase_dark = "ic_briefcase_dark.png";
        public static string ic_briefcase_light = "ic_briefcase_light.png";
        public static string ic_change_password_dark = "ic_change_password_dark.png";
        public static string ic_change_password_light = "ic_change_password_light.png";
        public static string ic_contact_dark = "ic_contact_dark.png";
        public static string ic_contact_light = "ic_contact_light.png";
        public static string ic_logout = "ic_logout.png";
        public static string ic_privacy_dark = "ic_privacy_dark.png";
        public static string ic_privacy_light = "ic_privacy_light.png";
        public static string ic_terms_dark = "ic_terms_dark.png";
        public static string ic_terms_light = "ic_terms_light.png";

        public static string ic_avatar_light = "ic_avatar_light.png";

        //Home Page
        public static string home_screen_banner = "home_screen_banner.png";
        public static string ic_food = "ic_food.png";
        public static string ic_groceries = "ic_groceries.png";
        public static string ic_home = "ic_home.png";
        public static string ic_it_services = "ic_it_services.png";
        public static string ic_job = "ic_job.png";
        public static string ic_search_gray = "ic_search_gray.png";
        public static string ic_menu = "menu.png";

        public static string ic_font_awasome = "\uf26e";

        public static string ic_globe = "ic_globe.png";
        public static string ic_link = "ic_link.png";
        public static string ic_right_arrow = "ic_right_arrow.png";
        public static string ic_smartphone = "ic_smartphone.png";

        //Contact Us Page
        public static string ic_avatar = "ic_avatar.png";
        public static string ic_subject = "ic_subject.png";
        public static string ic_comment = "ic_comment.png";

        public static string ic_job_blue = "ic_job_blue.png";
        public static string ic_location = "ic_location.png";
        public static string ic_view = "ic_view.png";
        public static string ic_search_circle = "ic_search_circle.png";

        public static string ic_keyword = "ic_keyword.png";
        public static string ic_category = "ic_category.png";
        public static string ic_city = "ic_city.png";
        //free business listing page
        public static string ic_briefcase_red = "ic_briefcase_red.png";
        public static string ic_down_arrow = "ic_down_arrow.png";
        public static string ic_postal_code = "ic_postal_code.png";
        public static string ic_province = "ic_province.png";
        public static string ic_upload = "ic_upload.png";
        public static string ic_upload_pic = "ic_upload_pic.png";
        public static string ic_radio_un_fill = "ic_radio_un_fill.png";
        public static string ic_radio_fill = "ic_radio_fill.png";
        public static string ic_remove = "ic_remove.png";

        //Business List Page
        public static string ic_direction = "ic_direction.png";
        public static string ic_phone_white = "ic_phone_white.png";

        public static string ic_close_white = "ic_close_white.png";
    }
}