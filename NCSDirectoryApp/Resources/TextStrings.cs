﻿using System;
namespace NCSDirectoryApp.Resources
{
    public static class TextStrings
    {
        public static string app_name_display = "Newcomers Services Canada";

        //Login Page
        public static string sign_in = "Sign In";
        public static string email_address = "Email Address";
        public static string email_address_or_mobile = "Email Address / Mobile Number";
        public static string password = "Password";
        public static string forgot_password = "Forgot Password?";
        public static string login = "LOGIN";
        public static string dont_have_account = "Don't you have an Business account?";

        //Signup Page
        public static string sign_up = "Sign Up";
        public static string mobile_number = "Mobile Number";
        public static string number = "Number";
        public static string create_an_account = "CREATE AN ACCOUNT";
        public static string already_have_account = "Already have an account? ";

        public static string emailBlankAlert = "Email Address can not be blank !";
        public static string passwordBlankAlert = "Password can not be blank !";
        public static string mobileNumberBlankAlert = "Mobile Number can not be blank !";
        public static string please_enter_valid_email = "Please enter valid Email Address!";

        //Confirm Otp Page
        public static string confirm_security_code = "CONFRIM YOUR SECURITY CODE";
        public static string otpBlankAlert = "Security code can not be blank !";
        public static string pleaseEnterValidOTP = "Invalid Security Code !";

        public static string didnt_get_otp = "Didn't get code? ";
        public static string resend_otp = "Resend";

        //ForgotPassword Page
        public static string forget_password_title = "Forgot Password";
        public static string send = "SEND";

        //Change Password Page
        public static string old_password = "Old Password";
        public static string new_password = "New Password";
        public static string confirm_new_password = "Confirm New Password";
        public static string save = "SAVE";
        public static string oldPasswordBlankAlert = "Old Password can not be blank !";
        public static string newPasswordBlankAlert = "New Password can not be blank !";
        public static string confirmPasswordBlankAlert = "Confirm New Password can not be blank !";
        public static string invalidConfirmPassword = "Please enter valid confirm password !";

        //Contact Us Page
        public static string messageBlankAlert = "Message can not be blank !";
        public static string subjectBlankAlert = "Subject can not be blank !";
        public static string nameBlankAlert = "Name can not be blank !";

        //Side menu page
        public static string sign_in_for_business = "SIGN IN FOR BUSINESS USER";
        public static string find = "Find";
        public static string list_a_business = "List a Business";
        public static string contact_us = "Contact Us";
        public static string about_us = "About Us";
        public static string information_center = "Information Center";
        public static string terms_of_service = "Terms of Service";
        public static string privacy_policy = "Privacy Policy";
        public static string edit_business_profile = "Edit Business Profile";
        public static string my_business = "My Business";
        public static string change_password = "Change Password";
        public static string logout = "LOGOUT";

        //Contact Us Page
        public static string name = "Name";
        public static string contact_number = "Contact Number";
        public static string subject = "Subject";
        public static string message = "Message";
        public static string type_here = "Type here...";

        //Home Page
        public static string welcome_to_ncs = "Welcome to Canada Newcomers Services";
        public static string search_here = "Search here..";
        public static string show_more = "SHOW MORE";
        public static string list_free_business = "List a Free Business With Us for Free";
        public static string contact_us_tab = "CONTACT US";
        public static string home = "HOME";
        public static string menu = "MENU";

        public static string useful_websites = "Useful Websites";
        public static string useful_phone_no = "Useful Phone Numbers";

        //Free Business Listing Page
        public static string free_business_lising = "Free Business Listing";
        public static string business_details = "Business Details";
        public static string logo_here = "Logo Here";
        public static string business_name = "Business Name*";
        public static string business_category = "Business Category";
        public static string about_business = "About Business / Service Offer*";
        public static string upload_photos = "Upload Photos";
        public static string website = "Website";
        public static string business_email = "Email Address*";
        public static string contact_person = "Contact Person*";
        public static string business_contact_number = "Contact Number*";
        public static string address_details = "Address Details";
        public static string address_line_1 = "Address Line1*";
        public static string address_line_2 = "Address Line2";
        public static string business_city = "City*";
        public static string province = "Provience";
        public static string postal_code = "Postal Code*";
    }
}