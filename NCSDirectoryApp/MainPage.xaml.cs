﻿using System;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.Views.Base;
using Xamarin.Forms;

namespace NCSDirectoryApp
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<EventArgs>(this, "Open Menu", args =>
            {
                this.IsPresented = !this.IsPresented;
            });
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            
        }
    }
}
