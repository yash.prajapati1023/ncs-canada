﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.BusinessDetails
{
    public class BusinessDetailsPageViewModel : BaseViewModel
    {
        public BusinessData SelectedBusiness { get; set; }

        public IFileService FileService;

        public BusinessDetailsPageViewModel(BusinessData business)
        {
            PageIcon = ImageStrings.ic_briefcase_light;
            PageTitle = "Business Details";
            this.SelectedBusiness = business;
            FileService = DependencyService.Get<IFileService>();
        }

        public ICommand CallNowCommand => new Command(async () => await CallNowAction());
        public ICommand MapDirectionCommand => new Command(async () => await MapDirectionAction());
        public ICommand OpenPhotoCommnad => new Command<object>(async (obj) => await OpenPhotoAction(obj));

        private string _businessLogo;
        public string BusinessLogo
        {
            get { return _businessLogo; }
            set
            {
                _businessLogo = value;
                OnPropertyChanged(nameof(BusinessLogo));
            }
        }

        private bool _isLogoTextVisible = true;
        public bool IsLogoTextVisible
        {
            get { return _isLogoTextVisible; }
            set
            {
                _isLogoTextVisible = value;
                OnPropertyChanged(nameof(IsLogoTextVisible));
            }
        }

        private bool _isLogoImgVisible = false;
        public bool IsLogoImgVisible
        {
            get { return _isLogoImgVisible; }
            set
            {
                _isLogoImgVisible = value;
                OnPropertyChanged(nameof(IsLogoImgVisible));
            }
        }

        private string _businessSubCategory;
        public string BusinessSubCategory
        {
            get { return _businessSubCategory; }
            set
            {
                _businessSubCategory = value;
                OnPropertyChanged(nameof(BusinessSubCategory));
            }
        }

        private string _businessTitle;
        public string BusinessTitle
        {
            get { return _businessTitle; }
            set
            {
                _businessTitle = value;
                OnPropertyChanged(nameof(BusinessTitle));
            }
        }

        private string _businessDescription;
        public string BusinessDescription
        {
            get { return _businessDescription; }
            set
            {
                _businessDescription = value;
                OnPropertyChanged(nameof(BusinessDescription));
            }
        }

        private string _businessWebsite;
        public string BusinessWebsite
        {
            get { return _businessWebsite; }
            set
            {
                _businessWebsite = value;
                OnPropertyChanged(nameof(BusinessWebsite));
            }
        }

        private string _businessEmail;
        public string BusinessEmail
        {
            get { return _businessEmail; }
            set
            {
                _businessEmail = value;
                OnPropertyChanged(nameof(BusinessEmail));
            }
        }

        private string _contactPerson;
        public string ContactPerson
        {
            get { return _contactPerson; }
            set
            {
                _contactPerson = value;
                OnPropertyChanged(nameof(ContactPerson));
            }
        }

        private string _contactNumber;
        public string ContactNumber
        {
            get { return _contactNumber; }
            set
            {
                _contactNumber = value;
                OnPropertyChanged(nameof(ContactNumber));
            }
        }

        private string _address;
        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                OnPropertyChanged(nameof(Address));
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private bool _isBusinessDescriptionVisible;
        public bool IsBusinessDescriptionVisible
        {
            get { return _isBusinessDescriptionVisible; }
            set
            {
                _isBusinessDescriptionVisible = value;
                OnPropertyChanged(nameof(IsBusinessDescriptionVisible));
            }
        }

        private bool _isBusinessWebsiteVisible;
        public bool IsBusinessWebsiteVisible
        {
            get { return _isBusinessWebsiteVisible; }
            set
            {
                _isBusinessWebsiteVisible = value;
                OnPropertyChanged(nameof(IsBusinessWebsiteVisible));
            }
        }

        private bool _isBusinessEmailVisible;
        public bool IsBusinessEmailVisible
        {
            get { return _isBusinessEmailVisible; }
            set
            {
                _isBusinessEmailVisible = value;
                OnPropertyChanged(nameof(IsBusinessEmailVisible));
            }
        }

        private bool _isContactPersonVisible;
        public bool IsContactPersonVisible
        {
            get { return _isContactPersonVisible; }
            set
            {
                _isContactPersonVisible = value;
                OnPropertyChanged(nameof(IsContactPersonVisible));
            }
        }

        private bool _isContactNumberVisible;
        public bool IsContactNumberVisible
        {
            get { return _isContactNumberVisible; }
            set
            {
                _isContactNumberVisible = value;
                OnPropertyChanged(nameof(IsContactNumberVisible));
            }
        }

        private bool _isAddressVisible;
        public bool IsAddressVisible
        {
            get { return _isAddressVisible; }
            set
            {
                _isAddressVisible = value;
                OnPropertyChanged(nameof(IsAddressVisible));
            }
        }

        private bool _isBusinessPhotosVisible;
        public bool IsBusinessPhotosVisible
        {
            get { return _isBusinessPhotosVisible; }
            set
            {
                _isBusinessPhotosVisible = value;
                OnPropertyChanged(nameof(IsBusinessPhotosVisible));
            }
        }

        private ObservableCollection<BusinessPhotos> _uploadedImagesList = new ObservableCollection<BusinessPhotos>();
        public ObservableCollection<BusinessPhotos> UploadedImagesList
        {
            get { return _uploadedImagesList; }
            set
            {
                _uploadedImagesList = value;
                OnPropertyChanged(nameof(UploadedImagesList));
            }
        }

        private async Task CallNowAction()
        {
            try
            {
                if (SelectedBusiness != null && !string.IsNullOrWhiteSpace(SelectedBusiness.contact_number))
                {
                    string number = SelectedBusiness.contact_number.Replace("-", "");
                    number = number.Replace(" ", "");
                    number = number.Replace(".", "");
                    if (number.Length > 10)
                        number = number.Substring(0, 10);
                    PhoneDialer.Open(number);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task MapDirectionAction()
        {
            try
            {
                if (SelectedBusiness != null)
                {
                    if (App.IsInternet)
                    {
                        if (!string.IsNullOrWhiteSpace(SelectedBusiness.latitude) && !string.IsNullOrWhiteSpace(SelectedBusiness.longitude))
                        {
                            var hasPermission = await Utils.CheckPermissions(Permission.Location);
                            if (!hasPermission)
                                return;
                            if (Device.RuntimePlatform == Device.Android)
                            {
                                bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                                if (!gpsStatus)
                                {
                                    await Helper.DisplayAlert("Please turn on your location.!");
                                    DependencyService.Get<ILocSettings>().OpenSettings();
                                }
                                else
                                {
                                    double lat;
                                    double _long;
                                    Double.TryParse(SelectedBusiness.latitude, out lat);
                                    Double.TryParse(SelectedBusiness.longitude, out _long);
                                    if (!double.IsNaN(lat) && !double.IsNaN(_long))
                                    {
                                        await GoogleMapHelper.ShowNavigationBuiltInMapAsync(Convert.ToDouble(SelectedBusiness.latitude, CultureInfo.InvariantCulture), Convert.ToDouble(SelectedBusiness.longitude, CultureInfo.InvariantCulture));
                                    }
                                }
                            }
                            else
                            {
                                double lat;
                                double _long;
                                Double.TryParse(SelectedBusiness.latitude, out lat);
                                Double.TryParse(SelectedBusiness.longitude, out _long);
                                if (!double.IsNaN(lat) && !double.IsNaN(_long))
                                {
                                    await GoogleMapHelper.ShowNavigationBuiltInMapAsync(Convert.ToDouble(SelectedBusiness.latitude, CultureInfo.InvariantCulture), Convert.ToDouble(SelectedBusiness.longitude, CultureInfo.InvariantCulture));
                                }
                            }
                            //double lat = Convert.ToDouble(business.latitude, CultureInfo.InvariantCulture);
                            
                        }
                    }
                    else
                        await Helper.DisplayAlert("Please check your internet connection.!");
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async Task OpenPhotoAction(object obj)
        {
            var selectedPhoto = (BusinessPhotos)obj;
            if(selectedPhoto != null)
            {
                await PopupNavigation.Instance.PushAsync(new FullScreenImagePopupPage(UploadedImagesList, selectedPhoto));
            }
        }

        public void SetBusinessData()
        {
            if (SelectedBusiness != null)
            {
                BusinessLogo = SelectedBusiness.business_logo;
                if (!string.IsNullOrWhiteSpace(BusinessLogo))
                {
                    IsLogoImgVisible = true;
                    IsLogoTextVisible = false;
                }
                else
                {
                    IsLogoImgVisible = false;
                    IsLogoTextVisible = true;
                }
                BusinessTitle = SelectedBusiness.business_name;
                BusinessSubCategory = SelectedBusiness.category_name;
                BusinessDescription = SelectedBusiness.business_about;
                IsBusinessDescriptionVisible = string.IsNullOrWhiteSpace(BusinessDescription) ? false : true;
                BusinessWebsite = SelectedBusiness.website;
                IsBusinessWebsiteVisible = string.IsNullOrWhiteSpace(BusinessWebsite) ? false : true;
                BusinessEmail = SelectedBusiness.email;
                IsBusinessEmailVisible = string.IsNullOrWhiteSpace(BusinessEmail) ? false : true;
                ContactPerson = SelectedBusiness.contact_person;
                IsContactPersonVisible = string.IsNullOrWhiteSpace(ContactPerson) ? false : true;
                
                ContactNumber = SelectedBusiness.contact_number;
                if (ContactNumber.Length > 12)
                    ContactNumber = ContactNumber.Substring(0, 12);
                IsContactNumberVisible = string.IsNullOrWhiteSpace(ContactNumber) ? false : true;
                Address = SelectedBusiness.address_line1 + ", " + SelectedBusiness.address_line2 + ", "
                    + SelectedBusiness.city + " - " + SelectedBusiness.postalcode;
                IsAddressVisible = string.IsNullOrWhiteSpace(Address) ? false : true;
                if (!string.IsNullOrWhiteSpace(SelectedBusiness.upload_photos))
                {
                    List<string> photos = JsonConvert.DeserializeObject<List<string>>(SelectedBusiness.upload_photos);

                    UploadedImagesList = new ObservableCollection<BusinessPhotos>();
                    if (photos != null && photos.Count > 0)
                    {
                        foreach (var img in photos)
                        {
                            UploadedImagesList.Add(new BusinessPhotos() { UploadedImageSource = img });
                        }
                    }
                    if (UploadedImagesList != null && UploadedImagesList.Count > 0)
                        IsBusinessPhotosVisible = true;
                    else
                        IsBusinessPhotosVisible = false;
                }
                else
                    IsBusinessPhotosVisible = false;
            }
        }
    }
}