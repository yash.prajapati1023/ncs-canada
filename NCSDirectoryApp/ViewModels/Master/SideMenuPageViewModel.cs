﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.AllBusinessCategories;
using NCSDirectoryApp.Views.AppContentPages;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.BusinessListing;
using NCSDirectoryApp.Views.BusinessLists;
using NCSDirectoryApp.Views.ContactUs;
using NCSDirectoryApp.Views.InformationCenter;
using NCSDirectoryApp.Views.JobList;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Master
{
    public class SideMenuPageViewModel : BaseViewModel
    {
        public SideMenuPageViewModel()
        {
        }

        public ICommand SignInCommand => new Command(async () => await SignInAction());
        public ICommand BackCommand => new Command(async () => await GoToBackAsync());
        public ICommand MenuSelectCommand => new Command<object>(async (obj) => await MenuSelectionAction(obj));

        private ObservableCollection<Models.MenuItem> _sideMenuList;
        public ObservableCollection<Models.MenuItem> SideMenuList
        {
            get { return _sideMenuList; }
            set
            {
                _sideMenuList = value;
                OnPropertyChanged(nameof(SideMenuList));
            }
        }

        private string _logoutIcon;
        public string LogoutIcon
        {
            get { return _logoutIcon; }
            set
            {
                _logoutIcon = value;
                OnPropertyChanged(nameof(LogoutIcon));
            }
        }

        private string _logoutText;
        public string LogoutText
        {
            get { return _logoutText; }
            set
            {
                _logoutText = value;
                OnPropertyChanged(nameof(LogoutText));
            }
        }

        private bool _isUserDetailVisible;
        public bool IsUserDetailVisible
        {
            get { return _isUserDetailVisible; }
            set
            {
                _isUserDetailVisible = value;
                OnPropertyChanged(nameof(IsUserDetailVisible));
            }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        private string _userProfile;
        public string UserProfile
        {
            get { return _userProfile; }
            set
            {
                _userProfile = value;
                OnPropertyChanged(nameof(UserProfile));
            }
        }

        private bool _isUserPhotoVisible;
        public bool IsUserPhotoVisible
        {
            get { return _isUserPhotoVisible; }
            set
            {
                _isUserPhotoVisible = value;
                OnPropertyChanged(nameof(IsUserPhotoVisible));
            }
        }

        private User _loggedInUser;
        public User LoggedInUser
        {
            get { return _loggedInUser; }
            set
            {
                _loggedInUser = value;
                OnPropertyChanged(nameof(LoggedInUser));
            }
        }

        private async Task GoToBackAsync()
        {
            MessagingCenter.Send(EventArgs.Empty, "Open Menu");
        }

        private async Task SignInAction()
        {
            if (Constants.GetIsLogin())
            {
                await PopupNavigation.Instance.PushAsync(new AlertDialogPopupPage(null, null, Enums.PageType.Logout));
            }
            else
            {
                MessagingCenter.Send(EventArgs.Empty, "Open Menu");
                await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
            }
        }

        private async Task MenuSelectionAction(object obj)
        {
            if (IsBusy)
                return;
            IsBusy = true;
            if (obj != null)
            {
                var selectedMenu = (Models.MenuItem)obj;
                if (selectedMenu != null && !string.IsNullOrWhiteSpace(selectedMenu.MenuTitle))
                {
                    MessagingCenter.Send(EventArgs.Empty, "Open Menu");
                    switch (selectedMenu.MenuTitle)
                    {
                        case "Find":
                            {
                                await Application.Current.MainPage.Navigation.PushAsync(new AllBusinessCategoriesPage());
                                break;
                            }
                        case "List a Business":
                            {
                                if (Constants.GetIsLogin())
                                    await Application.Current.MainPage.Navigation.PushAsync(new UsersBusinessListPage());
                                else
                                {
                                    //await Helper.DisplayAlert("Please Login First.!", true);
                                    await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
                                }
                                break;
                            }
                        case "Contact Us":
                            {
                                await Application.Current.MainPage.Navigation.PushAsync(new ContactUsPage());
                                break;
                            }
                        case "About Us":
                            {
                                App.SelectedPage = TextStrings.about_us;
                                await Application.Current.MainPage.Navigation.PushAsync(new AppContentPage());
                                break;
                            }
                        case "Information Center":
                            {
                                await Application.Current.MainPage.Navigation.PushAsync(new CMSPageList());
                                break;
                            }
                        case "Terms of Service":
                            {
                                App.SelectedPage = TextStrings.terms_of_service;
                                await Application.Current.MainPage.Navigation.PushAsync(new AppContentPage());
                                break;
                            }
                        case "Privacy Policy":
                            {
                                App.SelectedPage = TextStrings.privacy_policy;
                                await Application.Current.MainPage.Navigation.PushAsync(new AppContentPage());
                                break;
                            }
                        case "My Business":
                            {
                                //await Application.Current.MainPage.Navigation.PushAsync(new BusinessListingPage());
                                await Application.Current.MainPage.Navigation.PushAsync(new UsersBusinessListPage());
                                break;
                            }
                        case "Account Setting":
                            {
                                App.IsFromForgotPassword = false;
                                await Application.Current.MainPage.Navigation.PushAsync(new AccountSettingPage());
                                break;
                            }
                    }
                }
            }
            IsBusy = false;
        }

        public async Task SetSideMenu()
        {
            try
            {
                var isLogin = Constants.GetIsLogin();
                IsUserDetailVisible = isLogin;
                if (isLogin)
                    await SetSideMenuForLoggedInUser();
                else
                    await SetSideMenuForGuest();
            }
            catch (Exception ex)
            {
            }
        }

        public async Task SetSideMenuForLoggedInUser()
        {
            SideMenuList = new ObservableCollection<Models.MenuItem>()
            {
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_search_dark, IsMenuSelected = true, MenuTitle = TextStrings.find },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_briefcase_light, IsMenuSelected = false, MenuTitle = TextStrings.my_business },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_avatar_light, IsMenuSelected = false, MenuTitle = "Account Setting" },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_contact_light, IsMenuSelected = false, MenuTitle = TextStrings.contact_us },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_about_light, IsMenuSelected = false, MenuTitle = TextStrings.about_us },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_about_light, IsMenuSelected = false, MenuTitle = TextStrings.information_center },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_terms_light, IsMenuSelected = false, MenuTitle = TextStrings.terms_of_service },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_privacy_light, IsMenuSelected = false, MenuTitle = TextStrings.privacy_policy },
            };
            LogoutIcon = ImageStrings.ic_logout;
            LogoutText = TextStrings.logout;
            string userId = Constants.GetRegisterUserId();
            
            if (!App.IsInternet)
            {
                string userDataString = Constants.GetUserData();
                LoggedInUser = JsonConvert.DeserializeObject<User>(userDataString);
            }
            if(LoggedInUser == null && !string.IsNullOrWhiteSpace(userId))
            {
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.GetAsync(ApiPathStrings.user_details_by_id_url + userId);
                await PopupNavigation.Instance.PopAsync();
                if (!string.IsNullOrWhiteSpace(result.Item2) && result.Item1.ToLower() == "OK".ToLower())
                {
                    User userData = JsonConvert.DeserializeObject<User>(result.Item2);
                    LoggedInUser = userData;
                    Constants.SaveUserData(result.Item2);
                }
                else
                {
                    ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                    await Helper.DisplayAlert(response.message);
                }
            }
            if (LoggedInUser != null)
            {
                if (!string.IsNullOrWhiteSpace(LoggedInUser.fullName))
                    UserName = LoggedInUser.fullName;
                else
                    UserName = LoggedInUser.email;

                if (!string.IsNullOrWhiteSpace(LoggedInUser.profile_image))
                {
                    UserProfile = LoggedInUser.profile_image;
                    IsUserPhotoVisible = true;
                }
                else
                {
                    IsUserPhotoVisible = true;
                    UserProfile = ImageStrings.ic_user;
                }
            }
        }

        public async Task SetSideMenuForGuest()
        {
            SideMenuList = new ObservableCollection<Models.MenuItem>()
            {
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_search_dark, IsMenuSelected = true, MenuTitle = TextStrings.find },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_briefcase_light, IsMenuSelected = false, MenuTitle = TextStrings.list_a_business },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_contact_light, IsMenuSelected = false, MenuTitle = TextStrings.contact_us },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_about_light, IsMenuSelected = false, MenuTitle = TextStrings.about_us },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_about_light, IsMenuSelected = false, MenuTitle = TextStrings.information_center },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_terms_light, IsMenuSelected = false, MenuTitle = TextStrings.terms_of_service },
                new Models.MenuItem(){MenuIcon=ImageStrings.ic_privacy_light, IsMenuSelected = false, MenuTitle = TextStrings.privacy_policy },
            };
            LogoutIcon = ImageStrings.login_arrow_white;
            LogoutText = TextStrings.sign_in_for_business;
        }
    }
}
