﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.BusinessDetails;
using NCSDirectoryApp.Views.BusinessListing;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.SearchPage
{
    public class SearchViewPageViewModel : BaseViewModel
    {
        public bool IsNoMoreDataAvailable = false;

        public SearchViewPageViewModel()
        {
            PageIcon = ImageStrings.ic_search_light;
            PageTitle = "Search";
        }

        public ICommand BusinessSelectCommand => new Command<object>(async (obj) => await BusinessSelectAction(obj));
        public ICommand SearchCommand => new Command(async () => await SearchAction());

        private ObservableCollection<SearchData> _businessList = new ObservableCollection<SearchData>();
        public ObservableCollection<SearchData> BusinessList
        {
            get { return _businessList; }
            set
            {
                _businessList = value;
                OnPropertyChanged(nameof(BusinessList));
            }
        }

        private bool _isSearchBtnVisible = false;
        public bool IsSearchBtnVisible
        {
            get { return _isSearchBtnVisible; }
            set
            {
                _isSearchBtnVisible = value;
                OnPropertyChanged(nameof(IsSearchBtnVisible));
            }
        }

        private string _noDataText = "There is No Business Data Available.! Please enter keyword to search.";
        public string NoDataText
        {
            get { return _noDataText; }
            set
            {
                _noDataText = value;
                OnPropertyChanged(nameof(NoDataText));
            }
        }

        private string _userId;
        public string UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                OnPropertyChanged(nameof(UserId));
            }
        }

        private int _pageCount = 1;
        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged(nameof(PageCount));
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private string _radius;
        public string Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                OnPropertyChanged(nameof(Radius));
            }
        }

        public string tempSearchText { get; set; }

        private string _searchKeyword;
        public string SearchKeyword
        {
            get { return _searchKeyword; }
            set
            {
                _searchKeyword = value;
                if (!string.IsNullOrWhiteSpace(_searchKeyword))
                {
                    IsSearchBtnVisible = true;
                    OnPropertyChanged(nameof(IsSearchBtnVisible));
                }
                else
                {
                    IsSearchBtnVisible = false;
                    BusinessList = new ObservableCollection<SearchData>();
                    IsBusinessListVisible = false;
                    IsNoDataTxtVisible = true;
                    NoDataText = "No result found! Please try with different search.";
                    IsNoMoreDataAvailable = true;
                    OnPropertyChanged(nameof(IsSearchBtnVisible));
                }
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }

        private bool _isBusinessListVisible;
        public bool IsBusinessListVisible
        {
            get { return _isBusinessListVisible; }
            set
            {
                _isBusinessListVisible = value;
                OnPropertyChanged(nameof(IsBusinessListVisible));
            }
        }

        private bool _isNoDataTxtVisible;
        public bool IsNoDataTxtVisible
        {
            get { return _isNoDataTxtVisible; }
            set
            {
                _isNoDataTxtVisible = value;
                OnPropertyChanged(nameof(IsNoDataTxtVisible));
            }
        }

        private async Task BusinessSelectAction(object obj)
        {
            var selectedBusiness = (SearchData)obj;
            if (selectedBusiness != null)
            {

                if (selectedBusiness.IsJob.ToLower() == "yes".ToLower())
                {
                    await Browser.OpenAsync("https://www.newcomersjobs.com/employee/jobs/" + selectedBusiness.id, BrowserLaunchMode.SystemPreferred);
                }
                else
                {
                    BusinessData business = new BusinessData();
                    business.id = selectedBusiness.id;
                    business.business_id = business.id;
                    business.user_id = selectedBusiness.user_id;
                    business.category_id = selectedBusiness.category_id;
                    business.business_name = selectedBusiness.business_name;
                    business.business_about = selectedBusiness.business_about;
                    business.business_logo = selectedBusiness.business_logo;
                    business.upload_photos = selectedBusiness.upload_photos;
                    business.website = selectedBusiness.website;
                    business.email = selectedBusiness.email;
                    business.contact_person = selectedBusiness.contact_person;
                    business.contact_number = selectedBusiness.contact_number;
                    business.address_line1 = selectedBusiness.address_line1;
                    business.address_line2 = selectedBusiness.address_line2;
                    business.city = selectedBusiness.city;
                    business.province = selectedBusiness.province;
                    business.postalcode = selectedBusiness.postalcode;
                    business.latitude = selectedBusiness.latitude;
                    business.longitude = selectedBusiness.longitude;
                    business.is_active = selectedBusiness.is_active;
                    business.is_delete = selectedBusiness.is_delete;
                    business.created_at = selectedBusiness.created_at;
                    business.updated_at = selectedBusiness.updated_at;
                    business.category_name = selectedBusiness.category_name;
                    business.TotalDays = selectedBusiness.TotalDays;
                    await Application.Current.MainPage.Navigation.PushAsync(new BusinessDetailsPage(business));
                }
            }

        }

        private async Task SearchAction()
        {
            if(App.userLat == 0 || App.userLong == 0)
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                var position = await locator.GetLastKnownLocationAsync();
                if (position != null)
                {
                    Latitude = position.Latitude.ToString();
                    Longitude = position.Longitude.ToString();
                }
            }
            else
            {
                Latitude = App.userLat.ToString();
                Longitude = App.userLong.ToString();
            }
            
            if (!string.IsNullOrWhiteSpace(SearchKeyword))
                await LoadBusinessList(1, SearchKeyword);
            else
                await Helper.DisplayAlert("Please enter any keyword to search");
        }

        public async Task LoadBusinessList(int pagecount, string searchText)
        {
            try
            {
                if (pagecount == 1)
                    BusinessList = new ObservableCollection<SearchData>();
                if (!string.IsNullOrWhiteSpace(searchText))
                {

                    JobListRequest jobListRequest = new JobListRequest();
                    jobListRequest.cat_id = null;

                    jobListRequest.page = pagecount.ToString();
                    jobListRequest.lat = Latitude;
                    jobListRequest._long = Longitude;
                    if (searchText.ToLower() == "Near me".ToLower())
                    {
                        jobListRequest.lat = Latitude;
                        jobListRequest._long = Longitude;
                        jobListRequest.search = null;
                        jobListRequest.radius = "20";
                    }
                    else if (searchText.ToLower().Contains("Near Me".ToLower()))
                    {
                        jobListRequest.lat = Latitude;
                        jobListRequest._long = Longitude;
                        jobListRequest.radius = "20";
                        jobListRequest.search = searchText;
                    }
                    else
                    {
                        jobListRequest.search = searchText;
                        jobListRequest.radius = null;
                    }
                    string jobrequest = JsonConvert.SerializeObject(jobListRequest);
                    string request = jobrequest.Replace("_long", "long");
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.PostAsync(ApiPathStrings.globalSearch, request);
                    await PopupNavigation.Instance.PopAsync();
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        if (result.Item1.ToLower() == "OK".ToLower())
                        {
                            GlobalSearchResponse response = JsonConvert.DeserializeObject<GlobalSearchResponse>(result.Item2);
                            if (response.data != null && response.data.Count > 0)
                            {
                                IsBusinessListVisible = true;
                                IsNoMoreDataAvailable = false;
                                IsNoDataTxtVisible = false;
                                foreach (var business in response.data)
                                {
                                    if (business.IsJob.ToLower() == "yes".ToLower())
                                    {
                                        var createdDate = Convert.ToDateTime(business.created_at, CultureInfo.InvariantCulture);
                                        int diffDays = Convert.ToInt32((DateTime.Now - createdDate).TotalDays);
                                        if (diffDays == 0)
                                        {
                                            int diffhours = Convert.ToInt32((DateTime.Now - createdDate).TotalHours);
                                            if (diffhours == 0)
                                            {
                                                int diffMinutes = Convert.ToInt32((DateTime.Now - createdDate).TotalMinutes);
                                                if (diffMinutes == 0)
                                                {
                                                    int diffSeconds = Convert.ToInt32((DateTime.Now - createdDate).TotalSeconds);
                                                    business.TotalDays = diffSeconds + " seconds ago";
                                                }
                                                else
                                                    business.TotalDays = diffMinutes + " minutes ago";
                                            }
                                            else
                                                business.TotalDays = diffhours + " hours ago";
                                        }
                                        else
                                            business.TotalDays = diffDays + " days ago";
                                    }
                                    else
                                    {
                                        var distance = Convert.ToDouble(business.distance, CultureInfo.InvariantCulture);
                                        var round_distance = Math.Round(distance, 2);
                                        business.TotalDays = round_distance + " KM";
                                    }

                                    if (!string.IsNullOrWhiteSpace(business.contact_number) && business.contact_number.Length > 12)
                                        business.contact_number = business.contact_number.Substring(0, 12);
                                    BusinessList.Add(business);
                                }
                            }
                            else if (response.data == null || response.data.Count == 0)
                            {
                                IsNoMoreDataAvailable = true;
                                if (pagecount == 1)
                                {
                                    IsNoDataTxtVisible = true;
                                    if (searchText.ToLower().Contains("Near Me".ToLower()))
                                    {
                                        NoDataText = "No result found to near you within 20 km! Please try a global search.";
                                    }
                                    else
                                        NoDataText = "No result found! Please try with different search.";
                                    IsBusinessListVisible = false;
                                }
                            }
                        }
                        else
                        {
                            IsNoMoreDataAvailable = true;
                            if (pagecount == 1)
                            {
                                IsNoDataTxtVisible = true;
                                if (searchText.ToLower().Contains("Near Me".ToLower()))
                                {
                                    NoDataText = "No result found to near you within 20 km! Please try a global search.";
                                }
                                else
                                    NoDataText = "No result found! Please try with different search.";
                                IsBusinessListVisible = false;
                            }

                        }
                    }
                    else
                    {
                        IsNoMoreDataAvailable = true;
                        if (pagecount == 1)
                        {
                            IsNoDataTxtVisible = true;
                            if (searchText.ToLower().Contains("Near Me".ToLower()))
                            {
                                NoDataText = "No result found to near you within 20 km! Please try a global search.";
                            }
                            else
                                NoDataText = "No result found! Please try with different search.";
                            IsBusinessListVisible = false;
                        }
                        await Helper.DisplayAlert("Error.!");
                    }
                }
                else
                {
                    //await Application.Current.MainPage.Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task ItemAppearanceAction(object obj)
        {
            try
            {
                var business = (SearchData)obj;
                if (BusinessList != null && BusinessList.Count > 0)
                {
                    if (business != null && business == BusinessList[BusinessList.Count - 1])
                    {
                        if (!IsNoMoreDataAvailable)
                        {
                            PageCount = PageCount + 1;
                            await LoadBusinessList(PageCount + 1, SearchKeyword);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
