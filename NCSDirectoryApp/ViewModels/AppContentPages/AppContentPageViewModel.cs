﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NCSDirectoryApp.ViewModels.AppContentPages
{
    public class AppContentPageViewModel : BaseViewModel
    {

        public AppContentPageViewModel()
        {
        }

        private string _pageContent;
        public string PageContent
        {
            get { return _pageContent; }
            set
            {
                _pageContent = value;
                OnPropertyChanged(nameof(PageContent));
            }
        }

        public async Task SetPageContent()
        {
            if(App.SelectedPage == TextStrings.about_us)
            {
                PageTitle = TextStrings.about_us;
                PageIcon = ImageStrings.ic_about_light;
                await ShowPageData(ApiPathStrings.about_us_url);
            }
            else if(App.SelectedPage == TextStrings.privacy_policy)
            {
                PageTitle = TextStrings.privacy_policy;
                PageIcon = ImageStrings.ic_privacy_light;
                await ShowPageData(ApiPathStrings.privacy_policy_url);
            }
            else if(App.SelectedPage == TextStrings.terms_of_service)
            {
                PageTitle = TextStrings.terms_of_service;
                PageIcon = ImageStrings.ic_terms_light;
                await ShowPageData(ApiPathStrings.terms_of_services_url);
            }
        }

        public async Task ShowPageData(string pageName)
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.GetAsync(pageName);
                await PopupNavigation.Instance.PopAsync(true);
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    AppContentPageResponse response = JsonConvert.DeserializeObject<AppContentPageResponse>(result.Item2);
                    if (response != null)
                    {
                        if (result.Item1 == "OK")
                        {
                            if (response.data != null)
                            {
                                PageContent = response.data.content;
                            }
                            else
                                await Helper.DisplayAlert("User data not found.!");
                        }
                        else
                            await Helper.DisplayAlert(response.message);
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
                else
                    await Helper.DisplayAlert("Error.!");
                
            }
            catch (Exception ex)
            {
            }
        }
    }
}
