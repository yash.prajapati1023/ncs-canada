﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static NCSDirectoryApp.ApiServices.ApiService;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class SignupPageViewModel : BaseViewModel
    {
        public SignupPageViewModel()
        {
            PageIcon = ImageStrings.ic_login_title_blue;
            PageTitle = TextStrings.sign_up;
        }

        public ICommand PasswordShowCommand => new Command<string>(async (obj) => await PasswordShowAction(obj));
        public ICommand SignInCommand => new Command(async () => await SignInAsync());
        public ICommand CreateAccountCommand => new Command(async () => await CreateAccountAsync());

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                OnPropertyChanged(nameof(EmailAddress));
            }
        }

        private string _mobileNumber;
        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                _mobileNumber = value;
                OnPropertyChanged(nameof(MobileNumber));
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _passwordIcon = ImageStrings.password_hide;
        public string PasswordIcon
        {
            get { return _passwordIcon; }
            set
            {
                _passwordIcon = value;
                OnPropertyChanged(nameof(PasswordIcon));
            }
        }

        private bool _isPasswordHide = true;
        public bool IsPasswordHide
        {
            get { return _isPasswordHide; }
            set
            {
                _isPasswordHide = value;
                OnPropertyChanged(nameof(IsPasswordHide));
            }
        }

        private async Task SignInAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await App.CurrentInstance.LoadLoginPage();
            IsBusy = false;
        }

        private async Task CreateAccountAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    RegisterRequest registerRequest = new RegisterRequest();
                    registerRequest.email = EmailAddress;
                    registerRequest.password = Password;
                    registerRequest.phone = MobileNumber;
                    
                    string request = JsonConvert.SerializeObject(registerRequest);
                    
                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAsync(ApiPathStrings.registration_url, request);
                        await PopupNavigation.Instance.PopAsync();
                        //Response registerRespose = JsonConvert.DeserializeObject<Response>(response.Content);
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                            if (response != null)
                            {
                                if (result.Item1 == "OK")
                                {
                                    Constants.SaveRegisterUserId(response.id.ToString());
                                    App.IsFromForgotPassword = false;
                                    await Helper.DisplayAlert("Please check your email address to confirm security code.");
                                    await Application.Current.MainPage.Navigation.PushAsync(new ConfirmSecurityCodePage(response.otp));
                                    IsBusy = false;
                                }
                                else
                                    await Helper.DisplayAlert(response.message);
                            }
                            else
                                await Helper.DisplayAlert("Error.!");
                        }
                        else
                            await Helper.DisplayAlert("Error.!");
                    }
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        private async Task PasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsPasswordHide = false;
                PasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsPasswordHide = true;
                PasswordIcon = ImageStrings.password_hide;
            }
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.emailBlankAlert);
                }
                else if (!Helper.VerifyEmailAddress(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.please_enter_valid_email);
                    return isModelValid;
                }
                else if (string.IsNullOrWhiteSpace(MobileNumber))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.mobileNumberBlankAlert);
                }
                else if (string.IsNullOrWhiteSpace(Password))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.passwordBlankAlert);
                    return isModelValid;
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
