﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.BusinessLists;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class LoginPageViewModel : BaseViewModel
    {
        public LoginPageViewModel()
        {
            PageIcon = ImageStrings.ic_login_title_blue;
            PageTitle = TextStrings.sign_in;
        }

        public ICommand PasswordShowCommand => new Command<string>(async (obj) => await PasswordShowAction(obj));
        public ICommand ForgotPasswordCommand => new Command(async () => await ForgotPasswordAsync());
        public ICommand LoginCommand => new Command(async () => await LoginAsync());
        public ICommand SignupCommand => new Command(async () => await SignupAsync());
        public ICommand BackCommand => new Command(async () => await GoToBackAsync());

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                OnPropertyChanged(nameof(EmailAddress));
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _passwordIcon = ImageStrings.password_hide;
        public string PasswordIcon
        {
            get { return _passwordIcon; }
            set
            {
                _passwordIcon = value;
                OnPropertyChanged(nameof(PasswordIcon));
            }
        }

        private bool _isPasswordHide = true;
        public bool IsPasswordHide
        {
            get { return _isPasswordHide; }
            set
            {
                _isPasswordHide = value;
                OnPropertyChanged(nameof(IsPasswordHide));
            }
        }

        private async Task LoginAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    LoginRequest registerRequest = new LoginRequest();
                    registerRequest.email = EmailAddress;
                    registerRequest.password = Password;
                    string request = JsonConvert.SerializeObject(registerRequest);
                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAsync(ApiPathStrings.login_url, request);
                        await PopupNavigation.Instance.PopAsync();
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            LoginResponse response = JsonConvert.DeserializeObject<LoginResponse>(result.Item2);
                            if (response != null)
                            {
                                if (result.Item1 == "OK")
                                {
                                    if (response.data != null)
                                    {
                                        Constants.SaveRegisterUserId(response.data.id.ToString());
                                        var userData = JsonConvert.SerializeObject(response.data);
                                        //Constants.SaveUserData(userData);
                                        Constants.SetIsLogin(true);
                                        //await Application.Current.MainPage.DisplayAlert(TextStrings.app_name_display, response.message, "OK");

                                        if(!string.IsNullOrWhiteSpace(response.data.firstname) && !string.IsNullOrWhiteSpace(response.data.lastname))
                                        {
                                            await Application.Current.MainPage.Navigation.PopToRootAsync();
                                            await Application.Current.MainPage.Navigation.PushAsync(new UsersBusinessListPage());
                                        }
                                        else
                                        {
                                            await Application.Current.MainPage.Navigation.PopToRootAsync();
                                            await Application.Current.MainPage.Navigation.PushAsync(new AccountSettingPage());
                                        }
                                        IsBusy = false;
                                    }
                                    else
                                        await Helper.DisplayAlert("User data not found.!");
                                }
                                else
                                    await Helper.DisplayAlert(response.message);
                            }
                            else
                                await Helper.DisplayAlert("Error.!");
                        }
                        else
                            await Helper.DisplayAlert("Error.!");
                    }
                }
            }
            catch(Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        private async Task SignupAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await Application.Current.MainPage.Navigation.PushAsync(new SignupPage());
            IsBusy = false;
        }

        private async Task ForgotPasswordAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await Application.Current.MainPage.Navigation.PushAsync(new ForgotPasswordPage());
            IsBusy = false;
        }

        private async Task GoToBackAsync()
        {
            await App.CurrentInstance.LoadHomePage();
        }

        private async Task PasswordShowAction(string passwordImage)
        {
            if(passwordImage == ImageStrings.password_hide)
            {
                IsPasswordHide = false;
                PasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsPasswordHide = true;
                PasswordIcon = ImageStrings.password_hide;
            }
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Email Address/Mobile Number can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(Password))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.passwordBlankAlert);
                    return isModelValid;
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
