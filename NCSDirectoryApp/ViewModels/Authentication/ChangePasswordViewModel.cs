﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        public string enteredOtp;
        public ChangePasswordViewModel(string otp)
        {
            this.enteredOtp = otp;
            PageIcon = ImageStrings.ic_change_password_light;
            PageTitle = TextStrings.change_password;
            IsOldPasswordVisible = App.IsFromForgotPassword ? false : true;
        }

        public ICommand PasswordShowCommand => new Command<string>(async (obj) => await PasswordShowAction(obj));
        public ICommand NewPasswordShowCommand => new Command<string>(async (obj) => await NewPasswordShowAction(obj));
        public ICommand ConfirmPasswordShowCommand => new Command<string>(async (obj) => await ConfirmPasswordShowAction(obj));
        public ICommand SaveCommand => new Command(async () => await SavePasswordAction());

        private bool _isOldPasswordVisible;
        public bool IsOldPasswordVisible
        {
            get { return _isOldPasswordVisible; }
            set
            {
                _isOldPasswordVisible = value;
                OnPropertyChanged(nameof(IsOldPasswordVisible));
            }
        }

        private string _passwordIcon = ImageStrings.password_hide;
        public string PasswordIcon
        {
            get { return _passwordIcon; }
            set
            {
                _passwordIcon = value;
                OnPropertyChanged(nameof(PasswordIcon));
            }
        }

        private string _newPasswordIcon = ImageStrings.password_hide;
        public string NewPasswordIcon
        {
            get { return _newPasswordIcon; }
            set
            {
                _newPasswordIcon = value;
                OnPropertyChanged(nameof(NewPasswordIcon));
            }
        }

        private string _confirmPasswordIcon = ImageStrings.password_hide;
        public string ConfirmPasswordIcon
        {
            get { return _confirmPasswordIcon; }
            set
            {
                _confirmPasswordIcon = value;
                OnPropertyChanged(nameof(ConfirmPasswordIcon));
            }
        }

        private bool _isPasswordHide = true;
        public bool IsPasswordHide
        {
            get { return _isPasswordHide; }
            set
            {
                _isPasswordHide = value;
                OnPropertyChanged(nameof(IsPasswordHide));
            }
        }

        private bool _isNewPasswordHide = true;
        public bool IsNewPasswordHide
        {
            get { return _isNewPasswordHide; }
            set
            {
                _isNewPasswordHide = value;
                OnPropertyChanged(nameof(IsNewPasswordHide));
            }
        }

        private bool _isConfirmPasswordHide = true;
        public bool IsConfirmPasswordHide
        {
            get { return _isConfirmPasswordHide; }
            set
            {
                _isConfirmPasswordHide = value;
                OnPropertyChanged(nameof(IsConfirmPasswordHide));
            }
        }

        private string _oldPassword;
        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                _oldPassword = value;
                OnPropertyChanged(nameof(OldPassword));
            }
        }

        private string _newPassword;
        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                OnPropertyChanged(nameof(NewPassword));
            }
        }

        private string _confirmNewPassword;
        public string ConfirmNewPassword
        {
            get { return _confirmNewPassword; }
            set
            {
                _confirmNewPassword = value;
                OnPropertyChanged(nameof(ConfirmNewPassword));
            }
        }

        private async Task SavePasswordAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    if (App.IsFromForgotPassword)
                    {
                        await VerifyForgotPasswordOtp();
                    }
                    else
                    {
                        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                        changePasswordRequest.id = Constants.GetRegisterUserId();
                        changePasswordRequest.oldPassword = OldPassword;
                        changePasswordRequest.newPassword = NewPassword;
                        changePasswordRequest.confirmPassword = ConfirmNewPassword;
                        
                        string request = JsonConvert.SerializeObject(changePasswordRequest);
                        if (!string.IsNullOrWhiteSpace(request))
                        {
                            await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                            var result = await ApiService.PostAsync(ApiPathStrings.change_password, request);
                            await PopupNavigation.Instance.PopAsync(true);
                            if (!string.IsNullOrWhiteSpace(result.Item2))
                            {
                                if (result.Item1.ToLower() == "OK".ToLower())
                                {
                                    RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                                    if (response.status)
                                    {
                                        await Helper.DisplayAlert(response.message);
                                        OldPassword = NewPassword = ConfirmNewPassword = null;
                                        IsBusy = false;
                                    }
                                }
                                else
                                {
                                    ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                                    await Helper.DisplayAlert(response.message);
                                }
                            }
                            else
                                await Helper.DisplayAlert("Error.!");
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }


        private async Task VerifyForgotPasswordOtp()
        {
            if (!string.IsNullOrWhiteSpace(enteredOtp))
            {
                VerifyForgotPasswordRequest forgotPasswordRequest = new VerifyForgotPasswordRequest();
                forgotPasswordRequest.user_id = Constants.GetRegisterUserId();
                forgotPasswordRequest.otp = enteredOtp;
                forgotPasswordRequest.password = ConfirmNewPassword;
                
                string request = JsonConvert.SerializeObject(forgotPasswordRequest);
                
                if (!string.IsNullOrWhiteSpace(request))
                {
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.PostAsync(ApiPathStrings.verify_forgot_password_otp, request);
                    await PopupNavigation.Instance.PopAsync(true);
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        if (result.Item1.ToLower() == "OK".ToLower())
                        {
                            RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                            if (response.status)
                            {
                                await Helper.DisplayAlert("Your password has been reset successfully.");
                                OldPassword = NewPassword = ConfirmNewPassword = null;
                                await App.CurrentInstance.LoadLoginPage();
                                IsBusy = false;
                            }
                        }
                        else
                        {
                            ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                            await Helper.DisplayAlert(response.message);
                        }
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
            }
        }

        private async Task PasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsPasswordHide = false;
                PasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsPasswordHide = true;
                PasswordIcon = ImageStrings.password_hide;
            }
        }

        private async Task NewPasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsNewPasswordHide = false;
                NewPasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsNewPasswordHide = true;
                NewPasswordIcon = ImageStrings.password_hide;
            }
        }

        private async Task ConfirmPasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsConfirmPasswordHide = false;
                ConfirmPasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsConfirmPasswordHide = true;
                ConfirmPasswordIcon = ImageStrings.password_hide;
            }
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (IsOldPasswordVisible && string.IsNullOrWhiteSpace(OldPassword))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.oldPasswordBlankAlert);
                }
                else if (string.IsNullOrWhiteSpace(NewPassword))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.newPasswordBlankAlert);
                }
                else if (NewPassword == OldPassword)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Old password and New password can not be same !");
                }
                else if (string.IsNullOrWhiteSpace(ConfirmNewPassword))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.confirmPasswordBlankAlert);
                    return isModelValid;
                }
                else if (ConfirmNewPassword != NewPassword)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.invalidConfirmPassword);
                    return isModelValid;
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

    }
}
