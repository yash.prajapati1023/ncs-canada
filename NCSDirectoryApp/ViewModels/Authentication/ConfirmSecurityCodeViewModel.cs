﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class ConfirmSecurityCodeViewModel : BaseViewModel
    {
        public string generatedOtp;

        public ConfirmSecurityCodeViewModel(string otp)
        {
            this.generatedOtp = otp;
            PageIcon = App.IsFromForgotPassword ? ImageStrings.ic_change_password_light : ImageStrings.ic_login_title_blue;
            PageTitle = App.IsFromForgotPassword ? TextStrings.forget_password_title : TextStrings.sign_up;
        }

        public ICommand VerifyCodeCommand => new Command(async () => await VerifyCodeAction());
        public ICommand ResendCommand => new Command(async () => await ResendCodeAction());

        private string _otpCode;
        public string OtpCode
        {
            get { return _otpCode; }
            set
            {
                _otpCode = value;
                OnPropertyChanged(nameof(OtpCode));
                CheckConditions();
            }
        }

        private Color _otpCode1TextColor = ColorStrings.dark_blue;
        public Color OtpCode1TextColor
        {
            get { return _otpCode1TextColor; }
            set
            {
                _otpCode1TextColor = value;
                OnPropertyChanged(nameof(OtpCode1TextColor));
            }
        }

        private Color _otpCode2TextColor = ColorStrings.dark_blue;
        public Color OtpCode2TextColor
        {
            get { return _otpCode2TextColor; }
            set
            {
                _otpCode2TextColor = value;
                OnPropertyChanged(nameof(OtpCode2TextColor));
            }
        }

        private Color _otpCode3TextColor = ColorStrings.dark_blue;
        public Color OtpCode3TextColor
        {
            get { return _otpCode3TextColor; }
            set
            {
                _otpCode3TextColor = value;
                OnPropertyChanged(nameof(OtpCode3TextColor));
            }
        }

        private Color _otpCode4TextColor = ColorStrings.dark_blue;
        public Color OtpCode4TextColor
        {
            get { return _otpCode4TextColor; }
            set
            {
                _otpCode4TextColor = value;
                OnPropertyChanged(nameof(OtpCode4TextColor));
            }
        }

        private async Task VerifyCodeAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                bool isValid = await ValidateModel();
                var userId = Constants.GetRegisterUserId();
                if (isValid && !string.IsNullOrWhiteSpace(userId))
                {
                    if (App.IsFromForgotPassword)
                        await VerifyForgotPasswordOtp(userId);
                    else
                        await VerifyRegistrationOtp(userId);

                }
            }
            catch (Exception ex)
            {
                IsBusy = false;

            }
            IsBusy = false;

        }

        private async Task ResendCodeAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                if (!string.IsNullOrWhiteSpace(Constants.GetRegisterUserId()))
                {
                    ResendOtpRequest otpRequest = new ResendOtpRequest();
                    otpRequest.id = Constants.GetRegisterUserId();
                    otpRequest.isForgot = App.IsFromForgotPassword ? "yes" : "no";
                    var request = JsonConvert.SerializeObject(otpRequest);
                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAsync(ApiPathStrings.resend_otp, request);
                        await PopupNavigation.Instance.PopAsync();
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                            if(response != null)
                            {
                                await Helper.DisplayAlert(response.message);
                                generatedOtp = response.otp;
                                IsBusy = false;
                            }
                        }
                    }
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        private async Task VerifyRegistrationOtp(string userId)
        {
            if(OtpCode != generatedOtp)
            {
                await Helper.DisplayAlert("Invalid Security Code.!");
            }
            else
            {
                
                VerifyCodeRequest verifyCodeRequest = new VerifyCodeRequest();
                verifyCodeRequest.user_id = userId;
                verifyCodeRequest.otp = OtpCode;

                string request = JsonConvert.SerializeObject(verifyCodeRequest);
                if (!string.IsNullOrWhiteSpace(request))
                {
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.PostAsync(ApiPathStrings.verify_registration_otp_url, request);
                    await PopupNavigation.Instance.PopAsync();
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        LoginResponse response = JsonConvert.DeserializeObject<LoginResponse>(result.Item2);
                        if (response != null)
                        {
                            if (result.Item1 == "OK")
                            {
                                if (response.data != null)
                                {
                                    var userData = JsonConvert.SerializeObject(response.data);
                                    Constants.SaveUserData(userData);

                                    Constants.SetIsLogin(true);
                                    await Helper.DisplayAlert("You have successfully registered for the Newcomers Services Canada.");
                                    await App.CurrentInstance.LoadLoginPage();
                                    IsBusy = false;
                                }
                                else
                                    await Helper.DisplayAlert("User data not found.!");
                            }
                            else
                                await Helper.DisplayAlert(response.message);
                        }
                        else
                            await Helper.DisplayAlert("Error.!");
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
            }
            App.IsFromForgotPassword = false;
        }

        private async Task VerifyForgotPasswordOtp(string userId)
        {
            if (OtpCode != generatedOtp)
                await Helper.DisplayAlert("Invalid Otp Code.!");
            else
            {
                App.IsFromForgotPassword = true;
                await Application.Current.MainPage.Navigation.PushAsync(new ChangePasswordPage(OtpCode));
            }
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(OtpCode))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.otpBlankAlert);
                }
                else if (OtpCode.Length < 4)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.pleaseEnterValidOTP);
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public void CheckConditions()
        {
            if (!string.IsNullOrWhiteSpace(OtpCode))
            {
                if (OtpCode.Length == 1)
                {
                    OtpCode1TextColor = ColorStrings.dark_blue;
                    OtpCode2TextColor = ColorStrings.dark_red;
                    OtpCode3TextColor = ColorStrings.dark_blue;
                    OtpCode4TextColor = ColorStrings.dark_blue;
                }
                else if (OtpCode.Length == 2)
                {
                    OtpCode1TextColor = ColorStrings.dark_blue;
                    OtpCode2TextColor = ColorStrings.dark_blue;
                    OtpCode3TextColor = ColorStrings.dark_red;
                    OtpCode4TextColor = ColorStrings.dark_blue;
                }
                else if (OtpCode.Length == 3)
                {
                    OtpCode1TextColor = ColorStrings.dark_blue;
                    OtpCode2TextColor = ColorStrings.dark_blue;
                    OtpCode3TextColor = ColorStrings.dark_blue;
                    OtpCode4TextColor = ColorStrings.dark_red;
                }
                else
                {
                    OtpCode1TextColor = ColorStrings.dark_blue;
                    OtpCode2TextColor = ColorStrings.dark_blue;
                    OtpCode3TextColor = ColorStrings.dark_blue;
                    OtpCode4TextColor = ColorStrings.dark_red;
                }
            }
            else
            {
                OtpCode1TextColor = ColorStrings.dark_red;
                OtpCode2TextColor = ColorStrings.dark_blue;
                OtpCode3TextColor = ColorStrings.dark_blue;
                OtpCode4TextColor = ColorStrings.dark_blue;
            }
        }
    }
}