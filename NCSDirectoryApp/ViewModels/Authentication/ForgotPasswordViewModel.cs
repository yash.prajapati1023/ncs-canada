﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class ForgotPasswordViewModel : BaseViewModel
    {
        public ForgotPasswordViewModel()
        {
            PageTitle = TextStrings.forget_password_title;
            PageIcon = ImageStrings.ic_change_password_light;
        }

        public ICommand SendCommand => new Command(async () => await SendAction());

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                OnPropertyChanged(nameof(EmailAddress));
            }
        }


        private async Task SendAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
                    forgotPasswordRequest.phone_email = EmailAddress;
                    string request = JsonConvert.SerializeObject(forgotPasswordRequest);
                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAsync(ApiPathStrings.forgot_password, request);
                        await PopupNavigation.Instance.PopAsync();
                        //Response registerRespose = JsonConvert.DeserializeObject<Response>(response.Content);
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                            if (response != null)
                            {
                                if (result.Item1 == "OK")
                                {
                                    Constants.SaveRegisterUserId(response.id.ToString());
                                    App.IsFromForgotPassword = true;
                                    await Helper.DisplayAlert("Please verify the security code sent to your email address.!");
                                    await Application.Current.MainPage.Navigation.PushAsync(new ConfirmSecurityCodePage(response.otp));
                                    IsBusy = false;
                                    EmailAddress = null;
                                }
                                else
                                    await Helper.DisplayAlert(response.message);
                            }
                        }
                        else
                            await Helper.DisplayAlert("Error.!");
                    }
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.emailBlankAlert);
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
