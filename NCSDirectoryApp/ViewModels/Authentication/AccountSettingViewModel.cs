﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Authentication
{
    public class AccountSettingViewModel : BaseViewModel
    {
        public IFileService FileService;

        public AccountSettingViewModel()
        {
            PageIcon = ImageStrings.ic_change_password_light;
            PageTitle = "Account Settings";
            FileService = DependencyService.Get<IFileService>();
        }

        public ICommand PasswordShowCommand => new Command<string>(async (obj) => await PasswordShowAction(obj));
        public ICommand NewPasswordShowCommand => new Command<string>(async (obj) => await NewPasswordShowAction(obj));
        public ICommand ConfirmPasswordShowCommand => new Command<string>(async (obj) => await ConfirmPasswordShowAction(obj));
        public ICommand SaveCommand => new Command(async () => await SavePasswordAction());
        public ICommand UploadProfileCommand => new Command(async () => await UploadProfileImageAction());
        public ICommand BackCommand => new Command(async () => await GoToBackAsync());

        private bool _isLogoImgVisible = false;
        public bool IsLogoImgVisible
        {
            get { return _isLogoImgVisible; }
            set
            {
                _isLogoImgVisible = value;
                OnPropertyChanged(nameof(IsLogoImgVisible));
            }
        }

        private bool _isLogoTextVisible = true;
        public bool IsLogoTextVisible
        {
            get { return _isLogoTextVisible; }
            set
            {
                _isLogoTextVisible = value;
                OnPropertyChanged(nameof(IsLogoTextVisible));
            }
        }

        private string _passwordIcon = ImageStrings.password_hide;
        public string PasswordIcon
        {
            get { return _passwordIcon; }
            set
            {
                _passwordIcon = value;
                OnPropertyChanged(nameof(PasswordIcon));
            }
        }

        private string _newPasswordIcon = ImageStrings.password_hide;
        public string NewPasswordIcon
        {
            get { return _newPasswordIcon; }
            set
            {
                _newPasswordIcon = value;
                OnPropertyChanged(nameof(NewPasswordIcon));
            }
        }

        private string _confirmPasswordIcon = ImageStrings.password_hide;
        public string ConfirmPasswordIcon
        {
            get { return _confirmPasswordIcon; }
            set
            {
                _confirmPasswordIcon = value;
                OnPropertyChanged(nameof(ConfirmPasswordIcon));
            }
        }

        private bool _isPasswordHide = true;
        public bool IsPasswordHide
        {
            get { return _isPasswordHide; }
            set
            {
                _isPasswordHide = value;
                OnPropertyChanged(nameof(IsPasswordHide));
            }
        }

        private bool _isNewPasswordHide = true;
        public bool IsNewPasswordHide
        {
            get { return _isNewPasswordHide; }
            set
            {
                _isNewPasswordHide = value;
                OnPropertyChanged(nameof(IsNewPasswordHide));
            }
        }

        private bool _isConfirmPasswordHide = true;
        public bool IsConfirmPasswordHide
        {
            get { return _isConfirmPasswordHide; }
            set
            {
                _isConfirmPasswordHide = value;
                OnPropertyChanged(nameof(IsConfirmPasswordHide));
            }
        }

        private string _oldPassword;
        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                _oldPassword = value;
                OnPropertyChanged(nameof(OldPassword));
            }
        }

        private string _newPassword;
        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                OnPropertyChanged(nameof(NewPassword));
            }
        }

        private string _confirmNewPassword;
        public string ConfirmNewPassword
        {
            get { return _confirmNewPassword; }
            set
            {
                _confirmNewPassword = value;
                OnPropertyChanged(nameof(ConfirmNewPassword));
            }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged(nameof(FirstName));
            }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged(nameof(LastName));
            }
        }

        private string _profileImagePath;
        public string ProfileImagePath
        {
            get { return _profileImagePath; }
            set
            {
                _profileImagePath = value;
                OnPropertyChanged(nameof(ProfileImagePath));
            }
        }

        private ImageSource _userProfile;
        public ImageSource UserProfile
        {
            get { return _userProfile; }
            set
            {
                _userProfile = value;
                OnPropertyChanged(nameof(UserProfile));
            }
        }

        private async Task SavePasswordAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                    changePasswordRequest.id = Constants.GetRegisterUserId();
                    changePasswordRequest.oldPassword = OldPassword;
                    changePasswordRequest.newPassword = NewPassword;
                    changePasswordRequest.confirmPassword = ConfirmNewPassword;
                    changePasswordRequest.profile_image = ProfileImagePath;
                    changePasswordRequest.firstName = FirstName;
                    changePasswordRequest.lastName = LastName;

                    string request = JsonConvert.SerializeObject(changePasswordRequest);

                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAccountSettingData(ApiPathStrings.change_password, changePasswordRequest);
                        await PopupNavigation.Instance.PopAsync();
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            if (result.Item1.ToLower() == "OK".ToLower())
                            {
                                RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                                if (response.status || response.status2)
                                {

                                    await Helper.DisplayAlert("You have successfully updated your acccount.");
                                    await App.CurrentInstance.LoadHomePage();
                                    OldPassword = NewPassword = ConfirmNewPassword = null;
                                    IsLogoImgVisible = false;
                                    IsLogoTextVisible = true;
                                    IsBusy = false;
                                }
                            }
                            else
                            {
                                ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                                await Helper.DisplayAlert(response.message);
                            }
                        }
                        else
                            await Helper.DisplayAlert("Error.! Please check your entered details.");
                    }
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        private async Task PasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsPasswordHide = false;
                PasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsPasswordHide = true;
                PasswordIcon = ImageStrings.password_hide;
            }
        }

        private async Task GoToBackAsync()
        {
            await App.CurrentInstance.LoadHomePage();
        }

        private async Task NewPasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsNewPasswordHide = false;
                NewPasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsNewPasswordHide = true;
                NewPasswordIcon = ImageStrings.password_hide;
            }
        }

        private async Task ConfirmPasswordShowAction(string passwordImage)
        {
            if (passwordImage == ImageStrings.password_hide)
            {
                IsConfirmPasswordHide = false;
                ConfirmPasswordIcon = ImageStrings.password_show;
            }
            else
            {
                IsConfirmPasswordHide = true;
                ConfirmPasswordIcon = ImageStrings.password_hide;
            }
        }

        private async Task UploadProfileImageAction()
        {
            var action = await Application.Current.MainPage.DisplayActionSheet("Select Photo", "cancle", null, "Camera", "Gallery");
            switch (action)
            {
                case "Camera":
                    if (CrossMedia.Current.IsCameraAvailable && CrossMedia.Current.IsTakePhotoSupported)
                    {
                        var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium
                        });

                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        UserProfile = streamdata;
                        ProfileImagePath = file.Path;
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                case "Gallery":
                    if (CrossMedia.Current.IsPickPhotoSupported)
                    {
                        var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium,
                        });
                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        UserProfile = streamdata;
                        ProfileImagePath = file.Path;
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                default:
                    break;
            }
        }

        public async Task GetUserData()
        {
            var userData = Constants.GetUserData();
            if (!string.IsNullOrWhiteSpace(userData))
            {
                User user = JsonConvert.DeserializeObject<User>(userData);
                if (user != null)
                {
                    if (!string.IsNullOrWhiteSpace(user.firstname))
                        FirstName = user.firstname;
                    if (!string.IsNullOrWhiteSpace(user.lastname))
                        LastName = user.lastname;
                    if (!string.IsNullOrWhiteSpace(user.profile_image))
                    {
                        UserProfile = user.profile_image;
                        ProfileImagePath = await FileService.DownloadFile(user.profile_image);
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                    }
                }
            }
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(FirstName))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("First Name can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(LastName))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Last name can not be blank.!");
                }
                else if (!string.IsNullOrWhiteSpace(NewPassword) && string.IsNullOrWhiteSpace(OldPassword))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.oldPasswordBlankAlert);
                }
                else if (!string.IsNullOrWhiteSpace(NewPassword) && !string.IsNullOrWhiteSpace(OldPassword)
                    && NewPassword == OldPassword)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Old password and New password can not be same !");
                }
                else if (!string.IsNullOrWhiteSpace(NewPassword) && string.IsNullOrWhiteSpace(ConfirmNewPassword))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.confirmPasswordBlankAlert);
                }
                else if (!string.IsNullOrWhiteSpace(NewPassword) && !string.IsNullOrWhiteSpace(ConfirmNewPassword)
                    && ConfirmNewPassword != NewPassword)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.invalidConfirmPassword);
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
