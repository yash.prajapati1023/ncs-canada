﻿using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Resources;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Utilities;
using System;
using NCSDirectoryApp.Models.ResponseModels;
using Newtonsoft.Json;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;
using Rg.Plugins.Popup.Services;
using NCSDirectoryApp.Views.PopupPages;
using NCSDirectoryApp.Models.RequestModels;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using System.Linq;

namespace NCSDirectoryApp.ViewModels.JobList
{
    public class JobListPageViewModel : BaseViewModel
    {

        public bool IsNoMoreDataAvailable = false;

        public JobListPageViewModel()
        {
            PageTitle = "Jobs";
            PageIcon = ImageStrings.ic_job_blue;
        }

        public ICommand BackCommand => new Command(async () => await GoToBackAsync());
        public ICommand ViewJobCommand => new Command<object>(async (obj) => await ViewJobAction(obj));
        public ICommand SearchCommand => new Command(async () => await OpenSearchAction());

        private ObservableCollection<JobDetails> _jobsList = new ObservableCollection<JobDetails>();
        public ObservableCollection<JobDetails> JobsList
        {
            get { return _jobsList; }
            set
            {
                _jobsList = value;
                OnPropertyChanged(nameof(JobsList));
            }
        }

        private bool _isJobListVisible;
        public bool IsJobListVisible
        {
            get { return _isJobListVisible; }
            set
            {
                _isJobListVisible = value;
                OnPropertyChanged(nameof(IsJobListVisible));
            }
        }

        private bool _isNoDataTxtVisible;
        public bool IsNoDataTxtVisible
        {
            get { return _isNoDataTxtVisible; }
            set
            {
                _isNoDataTxtVisible = value;
                OnPropertyChanged(nameof(IsNoDataTxtVisible));
            }
        }

        private bool _isFromSearch;
        public bool IsFromSearch
        {
            get { return _isFromSearch; }
            set
            {
                _isFromSearch = value;
                OnPropertyChanged(nameof(IsFromSearch));
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private string _radius;
        public string Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                OnPropertyChanged(nameof(Radius));
            }
        }

        private string _searchKeyword;
        public string SearchKeyword
        {
            get { return _searchKeyword; }
            set
            {
                _searchKeyword = value;
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }

        private string _category;
        public string Category
        {
            get { return _category; }
            set
            {
                _category = value;
                OnPropertyChanged(nameof(Category));
            }
        }

        private int _pageCount = 1;
        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged(nameof(PageCount));
            }
        }

        public async Task ShowAllJobsList(int pageCount)
        {
            
            try
            {
                if (pageCount == 1)
                    JobsList = new ObservableCollection<JobDetails>();
                JobListRequest jobListRequest = new JobListRequest();
                jobListRequest.lat = Latitude;
                jobListRequest._long = Longitude;
                jobListRequest.radius = Radius;
                jobListRequest.search = SearchKeyword;
                jobListRequest.cat_id = Category;
                jobListRequest.page = pageCount.ToString();
                string jobrequest = JsonConvert.SerializeObject(jobListRequest);
                string request = jobrequest.Replace("_long", "long");
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.PostAsync(ApiPathStrings.get_job_list_url, request);
                await PopupNavigation.Instance.PopAsync();
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    if (result.Item1.ToLower() == "OK".ToLower())
                    {
                        JobsListResponse response = JsonConvert.DeserializeObject<JobsListResponse>(result.Item2);
                        if (response != null)
                        {
                            if (response.data != null && response.data.Count > 0)
                            {
                                IsNoMoreDataAvailable = false;
                                IsJobListVisible = true;
                                foreach (var job in response.data)
                                {
                                    JobsList.Add(job);
                                }
                            }
                            else if (response.data == null || response.data.Count == 0)
                            {
                                IsNoMoreDataAvailable = true;
                                if(pageCount == 1)
                                {
                                    IsNoDataTxtVisible = true;
                                    IsJobListVisible = false;
                                }
                            }
                        }
                        else
                        {
                            IsNoMoreDataAvailable = true;
                            if (pageCount == 1)
                            {
                                IsNoDataTxtVisible = true;
                                IsJobListVisible = false;
                            }
                        }
                    }
                    else
                    {
                        var response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                        await Helper.DisplayAlert(response.message);
                        Category = Latitude = Longitude = SearchKeyword = null;
                        if(pageCount == 1)
                        {
                            IsNoDataTxtVisible = true;
                            IsJobListVisible = false;
                        }
                    }
                }
                else
                {
                    IsNoMoreDataAvailable = true;
                    if (pageCount == 1)
                    {
                        IsNoDataTxtVisible = true;
                        IsJobListVisible = false;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task ViewJobAction(object obj)
        {
            try
            {
                var job = (JobDetails)obj;
                if (job != null)
                {
                    await Browser.OpenAsync("https://www.newcomersjobs.com/employee/jobs/" + job.id, BrowserLaunchMode.SystemPreferred);
                }
            }
            catch (Exception ex) { }
        }

        public async Task ItemAppearanceAction(object obj)
        {
            try
            {
                var job = (JobDetails)obj;
                if (JobsList != null && JobsList.Count > 0)
                {
                    if (job != null && job == JobsList[JobsList.Count - 1])
                    {
                        if (!IsNoMoreDataAvailable)
                        {
                            PageCount = PageCount + 1;
                            await ShowAllJobsList(PageCount);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task OpenSearchAction()
        {
            await PopupNavigation.Instance.PushAsync(new JobSearchPopupPage(this), true);
        }

        public async Task ApplyJobSearch(string searchKeyword, string location, string _category)
        {
            if (!string.IsNullOrWhiteSpace(location))
            {
                Geocoder coder = new Geocoder();
                IEnumerable<Position> approximateLocations = await coder.GetPositionsForAddressAsync(location);
                Position position = approximateLocations.FirstOrDefault();
                Latitude = position.Latitude.ToString();
                Longitude = position.Longitude.ToString();
                Radius = "20";
            }
            else
            {
                Latitude = null;
                Longitude = null;
            }
            this.Category = _category;
            PageCount = 1;
            SearchKeyword = searchKeyword;
            IsFromSearch = true;
            JobsList = new ObservableCollection<JobDetails>();
            await ShowAllJobsList(PageCount);
        }

        private async Task GoToBackAsync()
        {
            if (App.IsFromSearch)
            {
                Latitude = Longitude = Radius = SearchKeyword = null;
                PageCount = 1;
                await ShowAllJobsList(1);
                App.IsFromSearch = false;
            }
            else
                await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
