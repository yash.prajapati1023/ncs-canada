﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.BusinessDetails;
using NCSDirectoryApp.Views.BusinessListing;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.BusinessLists
{
    public class UsersBusinessListPageViewModel : BaseViewModel
    {
        public bool IsNoMoreDataAvailable = false;
        public List<BusinessData> businesses { get; set; }
        public UsersBusinessListPageViewModel()
        {
            PageIcon = ImageStrings.ic_briefcase_dark;
            PageTitle = TextStrings.my_business;
        }

        public ICommand BusinessSelectCommand => new Command<object>(async (obj) => await BusinessSelectAction(obj));
        public ICommand AddNewBusinessCommand => new Command(async () => await AddNewBusinessAction());
        public ICommand DeleteBusinessCommand => new Command<object>(async (obj) => await DeleteBusinessAction(obj));
        public ICommand BackCommand => new Command(async () => await GoToBackAsync());

        private ObservableCollection<BusinessData> _businessList = new ObservableCollection<BusinessData>();
        public ObservableCollection<BusinessData> BusinessList
        {
            get { return _businessList; }
            set
            {
                _businessList = value;
                OnPropertyChanged(nameof(BusinessList));
                if(_businessList!= null && _businessList.Count > 0)
                {
                    IsNoDataTxtVisible = false;
                    IsNoMoreDataAvailable = false;
                }
                else
                {
                    IsNoDataTxtVisible = true;
                    IsNoMoreDataAvailable = true;
                }
            }
        }

        private string _userId;
        public string UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                OnPropertyChanged(nameof(UserId));
            }
        }

        private int _pageCount = 1;
        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged(nameof(PageCount));
            }
        }

        private bool _isBusinessListVisible;
        public bool IsBusinessListVisible
        {
            get { return _isBusinessListVisible; }
            set
            {
                _isBusinessListVisible = value;
                OnPropertyChanged(nameof(IsBusinessListVisible));
            }
        }

        private bool _isNoDataTxtVisible;
        public bool IsNoDataTxtVisible
        {
            get { return _isNoDataTxtVisible; }
            set
            {
                _isNoDataTxtVisible = value;
                OnPropertyChanged(nameof(IsNoDataTxtVisible));
            }
        }

        private async Task BusinessSelectAction(object obj)
        {
            var selectedBusiness = (BusinessData)obj;
            if (selectedBusiness != null)
                await Application.Current.MainPage.Navigation.PushAsync(new BusinessListingPage(selectedBusiness));
        }

        private async Task DeleteBusinessAction(object obj)
        {
            try
            {
                var selectedBusiness = (BusinessData)obj;
                if (selectedBusiness != null)
                    await PopupNavigation.Instance.PushAsync(new AlertDialogPopupPage(selectedBusiness, this));
            }
            catch (Exception ex)
            {
            }

        }

        public async Task DeleteBusiness(BusinessData business)
        {
            if (business != null)
            {
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.GetAsync(ApiPathStrings.deleteBusiness + business.id);
                await PopupNavigation.Instance.PopAsync();
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    if (result.Item1.ToLower() == "OK".ToLower())
                    {
                        ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                        await Helper.DisplayAlert(response.message);
                        BusinessList.Remove(business);
                    }
                    else
                        await Helper.DisplayAlert(result.Item2);
                }
                else
                    await Helper.DisplayAlert("Error.!");
            }
        }

        public async Task LoadBusinessList(int pagecount)
        {
            try
            {
                if (pagecount == 1)
                    businesses = new List<BusinessData>();
                UserId = Constants.GetRegisterUserId();
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.GetAsync(ApiPathStrings.businessListByUserId + UserId + "/" + pagecount);
                    await PopupNavigation.Instance.PopAsync(true);
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        if (result.Item1.ToLower() == "OK".ToLower())
                        {
                            BusinessListResponse response = JsonConvert.DeserializeObject<BusinessListResponse>(result.Item2);
                            if (response.data != null && response.data.Count > 0)
                            {
                                IsBusinessListVisible = true;
                                IsNoDataTxtVisible = false;
                                IsNoMoreDataAvailable = false;
                                foreach (var business in response.data)
                                {
                                    var createdDate = Convert.ToDateTime(business.created_at, CultureInfo.InvariantCulture);
                                    int diffDays = Convert.ToInt32((DateTime.Now - createdDate).TotalDays);
                                    if (diffDays == 0)
                                    {
                                        int diffhours = Convert.ToInt32((DateTime.Now - createdDate).TotalHours);
                                        if (diffhours == 0)
                                        {
                                            int diffMinutes = Convert.ToInt32((DateTime.Now - createdDate).TotalMinutes);
                                            if (diffMinutes == 0)
                                            {
                                                int diffSeconds = Convert.ToInt32((DateTime.Now - createdDate).TotalSeconds);
                                                business.TotalDays = diffSeconds + " seconds ago";
                                            }
                                            else
                                                business.TotalDays = diffMinutes + " minutes ago";
                                        }
                                        else
                                            business.TotalDays = diffhours + " hours ago";
                                    }
                                    else
                                        business.TotalDays = diffDays + " days ago";
                                    businesses.Add(business);
                                }
                                //BusinessList = new ObservableCollection<BusinessData>(businesses.OrderByDescending(a => a.CreatedDate));
                            }
                            else if (response.data == null || response.data.Count == 0)
                            {
                                IsNoMoreDataAvailable = true;
                                if (pagecount == 1)
                                {
                                    IsNoDataTxtVisible = true;
                                    IsBusinessListVisible = false;
                                }
                            }
                        }
                        else
                        {
                            IsNoMoreDataAvailable = true;
                            if (pagecount == 1)
                            {
                                IsNoDataTxtVisible = true;
                                IsBusinessListVisible = false;
                            }

                        }
                    }
                    else
                    {
                        IsNoMoreDataAvailable = true;
                        if (pagecount == 1)
                        {
                            IsNoDataTxtVisible = true;
                            IsBusinessListVisible = false;
                        }
                        await Helper.DisplayAlert("Error.!");
                    }
                    try
                    {
                        await PopupNavigation.Instance.PopAsync(true);
                    }
                    catch(Exception ex)
                    {

                    }
                    if (businesses != null && businesses.Count > 0)
                        BusinessList = new ObservableCollection<BusinessData>(businesses.OrderByDescending(a => a.CreatedDate));
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task ItemAppearanceAction(object obj)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
        }

        private async Task GoToBackAsync()
        {
            await App.CurrentInstance.LoadHomePage();
        }

        private async Task AddNewBusinessAction()
        {
            if (BusinessList != null && BusinessList.Count == 5)
            {
                await Helper.DisplayAlert("You can only add 5 businesses.!");
            }
            else
                await Application.Current.MainPage.Navigation.PushAsync(new BusinessListingPage());
        }
    }
}
