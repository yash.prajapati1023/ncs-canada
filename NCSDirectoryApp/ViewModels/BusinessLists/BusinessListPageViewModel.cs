﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.BusinessDetails;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.BusinessLists
{
    public class BusinessListPageViewModel : BaseViewModel
    {
        public bool IsNoMoreDataAvailable = false;

        public ChildCategoryDetails SelectedCategory;
        public BusinessListPageViewModel(ChildCategoryDetails categoryDetails)
        {
            this.SelectedCategory = categoryDetails;
            PageTitle = categoryDetails.category_name;
            PageIcon = categoryDetails.category_icon;
        }

        public ICommand BackCommand => new Command(async () => await GoToBackAsync());
        public ICommand BusinessSelectCommand => new Command<object>(async (obj) => await BusinessSelectAction(obj));
        public ICommand SearchCommand => new Command(async () => await OpenSearchAction());
        public ICommand CallNowCommand => new Command(async (obj) => await CallNowAction(obj));
        public ICommand MapDirectionCommand => new Command(async (obj) => await MapDirectionAction(obj));

        private ObservableCollection<BusinessData> _businessList = new ObservableCollection<BusinessData>();
        public ObservableCollection<BusinessData> BusinessList
        {
            get { return _businessList; }
            set
            {
                _businessList = value;
                OnPropertyChanged(nameof(BusinessList));
            }
        }
        private int _pageCount = 1;
        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged(nameof(PageCount));
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private string _radius;
        public string Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                OnPropertyChanged(nameof(Radius));
            }
        }

        private string _searchKeyword;
        public string SearchKeyword
        {
            get { return _searchKeyword; }
            set
            {
                _searchKeyword = value;
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }

        private bool _isBusinessListVisible;
        public bool IsBusinessListVisible
        {
            get { return _isBusinessListVisible; }
            set
            {
                _isBusinessListVisible = value;
                OnPropertyChanged(nameof(IsBusinessListVisible));
            }
        }

        private bool _isNoDataTxtVisible;
        public bool IsNoDataTxtVisible
        {
            get { return _isNoDataTxtVisible; }
            set
            {
                _isNoDataTxtVisible = value;
                OnPropertyChanged(nameof(IsNoDataTxtVisible));
            }
        }

        private async Task BusinessSelectAction(object obj)
        {
            var selectedBusiness = (BusinessData)obj;
            if (selectedBusiness != null)
                await Application.Current.MainPage.Navigation.PushAsync(new BusinessDetailsPage(selectedBusiness));
        }

        private async Task CallNowAction(object obj)
        {
            try
            {
                var business = (BusinessData)obj;
                if (business != null && !string.IsNullOrWhiteSpace(business.contact_number))
                {
                    string number = business.contact_number.Replace("-", "");
                    number = number.Replace(" ", "");
                    number = number.Replace(".", "");
                    if (number.Length > 10)
                        number = number.Substring(0, 10);
                    PhoneDialer.Open(number);
                }
            }
            catch(Exception ex)
            {

            }
        }

        private async Task MapDirectionAction(object obj)
        {
            try
            {
                var business = (BusinessData)obj;
                if (business != null)
                {
                    if (!string.IsNullOrWhiteSpace(business.latitude) && !string.IsNullOrWhiteSpace(business.longitude))
                    {
                        var hasPermission = await Utils.CheckPermissions(Permission.Location);
                        if (!hasPermission)
                            return;
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                            if (!gpsStatus)
                            {
                                await Helper.DisplayAlert("Please turn on your location.!");
                                DependencyService.Get<ILocSettings>().OpenSettings();
                            }
                            else
                            {
                                double lat;
                                double _long;
                                Double.TryParse(business.latitude, out lat);
                                Double.TryParse(business.longitude, out _long);
                                if (!double.IsNaN(lat) && !double.IsNaN(_long))
                                {
                                    await GoogleMapHelper.ShowNavigationBuiltInMapAsync(Convert.ToDouble(business.latitude, CultureInfo.InvariantCulture), Convert.ToDouble(business.longitude, CultureInfo.InvariantCulture));
                                }
                            }
                        }
                        else
                        {
                            double lat;
                            double _long;
                            Double.TryParse(business.latitude, out lat);
                            Double.TryParse(business.longitude, out _long);
                            if (!double.IsNaN(lat) && !double.IsNaN(_long))
                            {
                                await GoogleMapHelper.ShowNavigationBuiltInMapAsync(Convert.ToDouble(business.latitude, CultureInfo.InvariantCulture), Convert.ToDouble(business.longitude, CultureInfo.InvariantCulture));
                            }
                        }
                        //double lat = Convert.ToDouble(business.latitude, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public async Task LoadBusinessList(int pagecount)
        {
            try
            {
                if (pagecount == 1)
                    BusinessList = new ObservableCollection<BusinessData>();
                if (App.userLat == 0 || App.userLong == 0)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;
                    var position = await locator.GetLastKnownLocationAsync();
                    if (position != null)
                    {
                        Latitude = position.Latitude.ToString();
                        Longitude = position.Longitude.ToString();
                    }
                }
                else
                {
                    Latitude = App.userLat.ToString();
                    Longitude = App.userLong.ToString();
                }
                JobListRequest jobListRequest = new JobListRequest();
                jobListRequest.lat = Latitude;
                jobListRequest._long = Longitude;
                jobListRequest.cat_id = SelectedCategory.id;
                jobListRequest.page = pagecount.ToString();
                string jobrequest = JsonConvert.SerializeObject(jobListRequest);
                string request = jobrequest.Replace("_long", "long");
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.PostAsync(ApiPathStrings.get_business_by_category, request);
                await PopupNavigation.Instance.PopAsync(true);
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    if (result.Item1.ToLower() == "OK".ToLower())
                    {
                        BusinessListResponse response = JsonConvert.DeserializeObject<BusinessListResponse>(result.Item2);
                        if (response.data != null && response.data.Count > 0)
                        {
                            IsNoDataTxtVisible = false;
                            IsNoMoreDataAvailable = false;
                            IsBusinessListVisible = true;
                            foreach (var business in response.data)
                            {
                                //var createdDate = Convert.ToDateTime(business.created_at, CultureInfo.InvariantCulture);
                                //int diffDays = Convert.ToInt32((DateTime.Now - createdDate).TotalDays);
                                //if (diffDays == 0)
                                //{
                                //    int diffhours = Convert.ToInt32((DateTime.Now - createdDate).TotalHours);
                                //    if (diffhours == 0)
                                //    {
                                //        int diffMinutes = Convert.ToInt32((DateTime.Now - createdDate).TotalMinutes);
                                //        if (diffMinutes == 0)
                                //        {
                                //            int diffSeconds = Convert.ToInt32((DateTime.Now - createdDate).TotalSeconds);
                                //            business.TotalDays = diffSeconds + " seconds ago";
                                //        }
                                //        else
                                //            business.TotalDays = diffMinutes + " minutes ago";
                                //    }
                                //    else
                                //        business.TotalDays = diffhours + " hours ago";
                                //}
                                //else
                                //    business.TotalDays = diffDays + " days ago";
                                var distance = Convert.ToDouble(business.distance, CultureInfo.InvariantCulture);
                                var round_distance = Math.Round(distance, 2);
                                business.TotalDays = round_distance + " KM";
                                if (business.contact_number.Length > 12)
                                    business.contact_number = business.contact_number.Substring(0, 12);
                                BusinessList.Add(business);
                            }
                        }
                        else if (response.data == null || response.data.Count == 0)
                        {
                            IsNoMoreDataAvailable = true;
                            if (pagecount == 1)
                            {
                                IsNoDataTxtVisible = true;
                                IsBusinessListVisible = false;
                            }
                        }
                    }
                    else
                    {
                        IsNoMoreDataAvailable = true;
                        if (pagecount == 1)
                        {
                            IsNoDataTxtVisible = true;
                            IsBusinessListVisible = false;
                        }
                    }
                }
                else
                {
                    IsNoMoreDataAvailable = true;
                    if (pagecount == 1)
                    {
                        IsNoDataTxtVisible = true;
                        IsBusinessListVisible = false;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task OpenSearchAction()
        {
            await PopupNavigation.Instance.PushAsync(new BusinessSearchPopupPage(this), true);
        }

        public async Task ItemAppearanceAction(object obj)
        {
            try
            {
                var business = (BusinessData)obj;
                if (BusinessList != null && BusinessList.Count > 0)
                {
                    if (business != null && business == BusinessList[BusinessList.Count - 1])
                    {
                        if (!IsNoMoreDataAvailable)
                        {
                            if (!App.IsFromSearch)
                            {
                                PageCount = PageCount + 1;
                                await LoadBusinessList(PageCount);
                            }
                            else
                            {
                                PageCount = PageCount + 1;
                                await LoadSearchList(PageCount);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task ApplyBusinessSearch(string latitude, string longitude, string search, string radius = null)
        {
            Latitude = latitude;
            Longitude = longitude;
            if(string.IsNullOrWhiteSpace(Latitude))
                Radius = null;
            else
                Radius = radius;
            App.IsFromSearch = true;
            PageCount = 1;
            SearchKeyword = search;
            BusinessList = new ObservableCollection<BusinessData>();
            if(string.IsNullOrWhiteSpace(Latitude) && string.IsNullOrWhiteSpace(Longitude)
                && string.IsNullOrWhiteSpace(SearchKeyword) && string.IsNullOrWhiteSpace(Radius))
            {
                IsNoDataTxtVisible = true;
                IsBusinessListVisible = false;
            }
            else
            {
                IsNoDataTxtVisible = false;
                IsBusinessListVisible = true;
                await LoadSearchList(1);
            }
        }

        public async Task LoadSearchList(int pageCount)
        {
            try
            {
                JobListRequest jobListRequest = new JobListRequest();
                jobListRequest.lat = Latitude;
                jobListRequest._long = Longitude;
                jobListRequest.cat_id = SelectedCategory.id;
                jobListRequest.radius = Radius;
                jobListRequest.page = pageCount.ToString();
                jobListRequest.search = SearchKeyword;
                string jobrequest = JsonConvert.SerializeObject(jobListRequest);
                string request = jobrequest.Replace("_long", "long");
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                var result = await ApiService.PostAsync(ApiPathStrings.businessListSearchByLatLong, request);
                await PopupNavigation.Instance.PopAsync(true);
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    if (result.Item1.ToLower() == "OK".ToLower())
                    {
                        BusinessListResponse response = JsonConvert.DeserializeObject<BusinessListResponse>(result.Item2);
                        if (response.data != null && response.data.Count > 0)
                        {
                            IsNoMoreDataAvailable = false;
                            IsBusinessListVisible = true;
                            foreach (var business in response.data)
                            {
                                //var createdDate = Convert.ToDateTime(business.created_at, CultureInfo.InvariantCulture);
                                //int diffDays = Convert.ToInt32((DateTime.Now - createdDate).TotalDays);
                                //if (diffDays == 0)
                                //{
                                //    int diffhours = Convert.ToInt32((DateTime.Now - createdDate).TotalHours);
                                //    if (diffhours == 0)
                                //    {
                                //        int diffMinutes = Convert.ToInt32((DateTime.Now - createdDate).TotalMinutes);
                                //        if (diffMinutes == 0)
                                //        {
                                //            int diffSeconds = Convert.ToInt32((DateTime.Now - createdDate).TotalSeconds);
                                //            business.TotalDays = diffSeconds + " seconds ago";
                                //        }
                                //        else
                                //            business.TotalDays = diffMinutes + " minutes ago";
                                //    }
                                //    else
                                //        business.TotalDays = diffhours + " hours ago";
                                //}
                                //else
                                //    business.TotalDays = diffDays + " days ago";
                                var distance = Convert.ToDouble(business.distance, CultureInfo.InvariantCulture);
                                var round_distance = Math.Round(distance, 2);
                                business.TotalDays = round_distance + " KM";
                                if (business.contact_number.Length > 12)
                                    business.contact_number = business.contact_number.Substring(0, 12);
                                BusinessList.Add(business);
                            }
                        }
                        else if (response.data == null || response.data.Count == 0)
                        {
                            IsNoMoreDataAvailable = true;
                            if (pageCount == 1)
                            {
                                IsNoDataTxtVisible = true;
                                IsBusinessListVisible = false;
                            }
                        }
                    }
                    else
                    {
                        IsNoMoreDataAvailable = true;
                        if (pageCount == 1)
                        {
                            IsNoDataTxtVisible = true;
                            IsBusinessListVisible = false;
                        }
                    }
                }
                else
                {
                    IsNoMoreDataAvailable = true;
                    if (pageCount == 1)
                    {
                        IsNoDataTxtVisible = true;
                        IsBusinessListVisible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
            }
        }


        private async Task GoToBackAsync()
        {
            if (App.IsFromSearch)
            {
                PageCount = 1;
                IsNoDataTxtVisible = false;
                IsBusinessListVisible = true;
                await LoadBusinessList(1);
                App.IsFromSearch = false;
            }
            else
                await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
