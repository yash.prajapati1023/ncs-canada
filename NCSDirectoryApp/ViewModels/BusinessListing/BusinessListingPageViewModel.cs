﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace NCSDirectoryApp.ViewModels.BusinessListing
{
    public class BusinessListingPageViewModel : BaseViewModel
    {
        public BusinessData SelectedBusinessData { get; set; }
        public bool isFirstTime { get; set; }
        public IFileService FileService;

        Regex WebsiteRegex = new Regex(@"(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?");
        Regex ContactNameRegex = new Regex(@"^[a-zA-Z ]*$");

        public BusinessListingPageViewModel(BusinessData business)
        {

            PageIcon = ImageStrings.ic_briefcase_light;
            PageTitle = "Business Listing";
            if (business != null)
            {
                this.SelectedBusinessData = business;
                BusinessId = SelectedBusinessData.id;
            }
            isFirstTime = true;
            FileService = DependencyService.Get<IFileService>();
            MessagingCenter.Subscribe<ObservableCollection<BusinessPhotos>>(this, "ImagesSelected", async (images) =>
            {
                if(UploadedImagesList != null && UploadedImagesList.Count > 0)
                {
                    IsUploadPhotoViewVisible = true;
                    foreach(var item in images)
                    {
                        UploadedImagesList.Add(item);
                    }
                    if(UploadedImagesList.Count > 5)
                    {
                        UploadedImagesList = new ObservableCollection<BusinessPhotos>(UploadedImagesList.Take(5));
                        await Helper.DisplayAlert("You can upload only 5 images.");
                    }
                }
                else
                {
                    UploadedImagesList = images;
                }
                if(UploadedImagesList != null && UploadedImagesList.Count > 0)
                    IsUploadPhotoViewVisible = true;
            });
        }

        public ICommand SaveCommand => new Command(async () => await SaveBusinessListingAction());
        public ICommand UploadPhotosCommand => new Command(async () => await UploadPhotosAction());
        public ICommand DeletePhotoCommand => new Command<object>(async (obj) => await DeletePhotoAction(obj));
        public ICommand UploadBusinessLogoCommand => new Command(async () => await UploadBusinessLogoAction());
        public ICommand IsActiveCommand => new Command<string>(async (obj) => await IsActiveSelectionAction(obj));
        public ICommand SelectCategoryCommand => new Command(async () => await OpenCategoryPopup());

        private double _editorHeight = 50;
        public double EditorHeight
        {
            get { return _editorHeight; }
            set
            {
                _editorHeight = value;
                OnPropertyChanged(nameof(EditorHeight));
            }
        }

        private EditorAutoSizeOption _editorAutoSizeType = EditorAutoSizeOption.TextChanges;
        public EditorAutoSizeOption EditorAutoSizeType
        {
            get { return _editorAutoSizeType; }
            set
            {
                _editorAutoSizeType = value;
                OnPropertyChanged(nameof(EditorAutoSizeType));
            }
        }

        private string _businessId;
        public string BusinessId
        {
            get { return _businessId; }
            set
            {
                _businessId = value;
                OnPropertyChanged(nameof(BusinessId));
            }
        }

        private string _logoImagePath;
        public string LogoImagePath
        {
            get { return _logoImagePath; }
            set
            {
                _logoImagePath = value;
                OnPropertyChanged(nameof(LogoImagePath));
            }
        }

        private byte[] _logoImageBytes;
        public byte[] LogoImageBytes
        {
            get { return _logoImageBytes; }
            set
            {
                _logoImageBytes = value;
                OnPropertyChanged(nameof(LogoImageBytes));
            }
        }

        private bool _isLogoTextVisible = true;
        public bool IsLogoTextVisible
        {
            get { return _isLogoTextVisible; }
            set
            {
                _isLogoTextVisible = value;
                OnPropertyChanged(nameof(IsLogoTextVisible));
            }
        }

        private bool _isLogoImgVisible = false;
        public bool IsLogoImgVisible
        {
            get { return _isLogoImgVisible; }
            set
            {
                _isLogoImgVisible = value;
                OnPropertyChanged(nameof(IsLogoImgVisible));
            }
        }

        private string _businessLogo;
        public string BusinessLogo
        {
            get { return _businessLogo; }
            set
            {
                _businessLogo = value;
                OnPropertyChanged(nameof(BusinessLogo));
            }
        }

        private bool _isUploadPhotoViewVisible = false;
        public bool IsUploadPhotoViewVisible
        {
            get { return _isUploadPhotoViewVisible; }
            set
            {
                _isUploadPhotoViewVisible = value;
                OnPropertyChanged(nameof(IsUploadPhotoViewVisible));
            }
        }

        private string _businessName;
        public string BusinessName
        {
            get { return _businessName; }
            set
            {
                _businessName = value;
                OnPropertyChanged(nameof(BusinessName));
            }
        }

        private string _aboutBusiness;
        public string AboutBusiness
        {
            get { return _aboutBusiness; }
            set
            {
                _aboutBusiness = value;
                OnPropertyChanged(nameof(AboutBusiness));
            }
        }

        private string _businessWebsite;
        public string BusinessWebsite
        {
            get { return _businessWebsite; }
            set
            {
                _businessWebsite = value;
                OnPropertyChanged(nameof(BusinessWebsite));
            }
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                OnPropertyChanged(nameof(EmailAddress));
            }
        }

        private string _contactPerson;
        public string ContactPerson
        {
            get { return _contactPerson; }
            set
            {
                _contactPerson = value;
                OnPropertyChanged(nameof(ContactPerson));
            }
        }

        private string _contactNumber;
        public string ContactNumber
        {
            get { return _contactNumber; }
            set
            {
                _contactNumber = value;
                OnPropertyChanged(nameof(ContactNumber));
            }
        }

        private string _addressLine1;
        public string AddressLine1
        {
            get { return _addressLine1; }
            set
            {
                _addressLine1 = value;
                OnPropertyChanged(nameof(AddressLine1));
            }
        }

        private string _addressLine2;
        public string AddressLine2
        {
            get { return _addressLine2; }
            set
            {
                _addressLine2 = value;
                OnPropertyChanged(nameof(AddressLine2));
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged(nameof(City));
            }
        }

        private string _province;
        public string Province
        {
            get { return _province; }
            set
            {
                _province = value;
                OnPropertyChanged(nameof(Province));
            }
        }

        private string _state;
        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                OnPropertyChanged(nameof(State));
            }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set
            {
                _country = value;
                OnPropertyChanged(nameof(Country));
            }
        }

        private string _postalCode;
        public string PostalCode
        {
            get { return _postalCode; }
            set
            {
                _postalCode = value;
                OnPropertyChanged(nameof(PostalCode));
            }
        }
        private ObservableCollection<BusinessPhotos> _uploadedImagesList = new ObservableCollection<BusinessPhotos>();
        public ObservableCollection<BusinessPhotos> UploadedImagesList
        {
            get { return _uploadedImagesList; }
            set
            {
                _uploadedImagesList = value;
                OnPropertyChanged(nameof(UploadedImagesList));
            }
        }

        private ObservableCollection<ChildCategoryDetails> _businessSubCategoryList;
        public ObservableCollection<ChildCategoryDetails> BusinessSubCategoryList
        {
            get { return _businessSubCategoryList; }
            set
            {
                _businessSubCategoryList = value;
                OnPropertyChanged(nameof(BusinessSubCategoryList));
            }
        }

        private ChildCategoryDetails _selectedBusinessCategory;
        public ChildCategoryDetails SelectedBusinessCategory
        {
            get { return _selectedBusinessCategory; }
            set
            {
                _selectedBusinessCategory = value;
                OnPropertyChanged(nameof(SelectedBusinessCategory));
            }
        }

        private string _isActiveRadioImageSource = ImageStrings.ic_radio_fill;
        public string IsActiveRadioImageSource
        {
            get { return _isActiveRadioImageSource; }
            set
            {
                _isActiveRadioImageSource = value;
                OnPropertyChanged(nameof(IsActiveRadioImageSource));
            }
        }

        private string _isInActiveRadioImageSource = ImageStrings.ic_radio_un_fill;
        public string IsInActiveRadioImageSource
        {
            get { return _isInActiveRadioImageSource; }
            set
            {
                _isInActiveRadioImageSource = value;
                OnPropertyChanged(nameof(IsInActiveRadioImageSource));
            }
        }

        private bool _isBusinessActive = true;
        public bool IsBusinessActive
        {
            get { return _isBusinessActive; }
            set
            {
                _isBusinessActive = value;
                OnPropertyChanged(nameof(IsBusinessActive));
            }
        }

        private string _selectedSubCategory = TextStrings.business_category;
        public string SelectedSubCategory
        {
            get { return _selectedSubCategory; }
            set
            {
                _selectedSubCategory = value;
                OnPropertyChanged(nameof(SelectedSubCategory));
            }
        }

        private async Task IsActiveSelectionAction(string value)
        {
            if (value == "IsActive")
            {
                IsBusinessActive = true;
                IsActiveRadioImageSource = ImageStrings.ic_radio_fill;
                IsInActiveRadioImageSource = ImageStrings.ic_radio_un_fill;
            }
            else
            {
                IsBusinessActive = false;
                IsActiveRadioImageSource = ImageStrings.ic_radio_un_fill;
                IsInActiveRadioImageSource = ImageStrings.ic_radio_fill;
            }
        }

        public async Task SetBusinessCategories()
        {
            try
            {
                //var locationAddress = "B-5 Natvar Park Society Near Varia Prajapati Wadi Nadiad Kheda Gujarat India 387002";

                ////How to get Latitude and Longitude from address?
                //Geocoder coder = new Geocoder();
                //IEnumerable<Position> approximateLocations = await coder.GetPositionsForAddressAsync(locationAddress);
                //Position position = approximateLocations.FirstOrDefault();
                
                if (App.AllBusinessSubCategories == null || App.AllBusinessSubCategories.Count == 0)
                {
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.GetAsync(ApiPathStrings.get_all_business_categories_url);
                    await PopupNavigation.Instance.PopAsync(true);
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        if (result.Item1.ToLower() == "OK".ToLower())
                        {
                            BusinessCategoriesResponse response = JsonConvert.DeserializeObject<BusinessCategoriesResponse>(result.Item2);
                            if (response != null && response.status)
                            {
                                if (response.data != null && response.data.level_one != null && response.data.level_one.Count > 0)
                                {
                                    App.AllBusinessParentCategories = new List<ParentCategoryDetails>();
                                    App.AllBusinessSubCategories = new List<ChildCategoryDetails>();
                                    foreach (var parent in response.data.level_one)
                                    {
                                        App.AllBusinessParentCategories.Add(parent);
                                        if (parent.level_two != null && parent.level_two.Count > 0)
                                        {
                                            foreach (var subCat in parent.level_two)
                                            {
                                                App.AllBusinessSubCategories.Add(subCat);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                            await Helper.DisplayAlert(response.message);
                        }
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
                if (BusinessSubCategoryList == null || BusinessSubCategoryList.Count == 0)
                {
                    BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>();
                    foreach (var businessCategory in App.AllBusinessSubCategories)
                    {
                        BusinessSubCategoryList.Add(businessCategory);
                    }
                    BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>(BusinessSubCategoryList.OrderBy(a => a.category_name));
                }
                
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PopAsync(true);
            }
        }

        private async Task SaveBusinessListingAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    List<string> businessPhotosList = new List<string>();
                    foreach (var photo in UploadedImagesList)
                    {
                        businessPhotosList.Add(photo.FilePath);
                    }
                    //var formContent = new FormUrlEncodedContent(new[]
                    //{
                    //    new KeyValuePair<string, string>("user_id", userid),
                    //    new KeyValuePair<string, string>("category_id", SelectedBusinessCategory.id),
                    //    new KeyValuePair<string, string>("business_name", BusinessName),
                    //    new KeyValuePair<string, string>("business_about", AboutBusiness),
                    //    new KeyValuePair<string, string>("website", BusinessWebsite),
                    //    new KeyValuePair<string, string>("email", EmailAddress),
                    //    new KeyValuePair<string, string>("contact_person", ContactPerson),
                    //    new KeyValuePair<string, string>("contact_number", ContactNumber),
                    //    new KeyValuePair<string, string>("address_line1", AddressLine1),
                    //    new KeyValuePair<string, string>("address_line2", AddressLine2),
                    //    new KeyValuePair<string, string>("city",City),
                    //    new KeyValuePair<string, string>("province",Province),
                    //    new KeyValuePair<string, string>("postalcode", PostalCode)
                    //});
                    BusinessData business = new BusinessData();
                    business.id = BusinessId;
                    business.business_id = BusinessId;
                    business.user_id = Constants.GetRegisterUserId();
                    business.category_id = SelectedBusinessCategory.id;
                    business.business_name = BusinessName;
                    business.business_about = AboutBusiness;
                    business.website = BusinessWebsite;
                    business.email = EmailAddress;
                    business.contact_person = ContactPerson;
                    business.contact_number = ContactNumber;
                    business.address_line1 = AddressLine1;
                    business.address_line2 = AddressLine2;
                    business.city = City;
                    business.province = Province;
                    business.postalcode = PostalCode;
                    business.upload_photos = JsonConvert.SerializeObject(businessPhotosList);
                    business.business_logo = LogoImagePath;
                    business.BusinessPhotosList = UploadedImagesList;
                    business.is_active = IsBusinessActive ? "1" : "0";
                    var locationAddress = AddressLine1 + " " + AddressLine2 + " " + City + " " + Province + " " + PostalCode;

                    //How to get Latitude and Longitude from address?
                    Geocoder coder = new Geocoder();
                    IEnumerable<Position> approximateLocations = await coder.GetPositionsForAddressAsync(locationAddress);
                    Position position = approximateLocations.FirstOrDefault();
                    business.latitude = position.Latitude.ToString();
                    business.longitude = position.Longitude.ToString();

                    if (App.IsInternet)
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostBusinessListingData(ApiPathStrings.free_business_listing, business);
                        await PopupNavigation.Instance.PopAsync(true);
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            if (result.Item1.ToLower() == "OK".ToLower())
                            {
                                RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                                await Helper.DisplayAlert(response.message);
                                if (response.status)
                                {
                                    UploadedImagesList = null;
                                    BusinessName = AboutBusiness = ContactNumber = ContactPerson = BusinessWebsite
                                        = EmailAddress = AddressLine1 = AddressLine2 = City = Province = PostalCode
                                        = Country = State = null;
                                    BusinessLogo = null;
                                    SelectedBusinessCategory = null;
                                    IsLogoImgVisible = false;
                                    IsLogoTextVisible = true;
                                    await Application.Current.MainPage.Navigation.PopAsync();
                                }
                            }
                            else
                            {
                                ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                                await Helper.DisplayAlert(response.message);
                            }
                        }
                        else
                            await Helper.DisplayAlert("Error:");
                    }
                    else
                        await Helper.DisplayAlert("Please check your internet connection.!");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                await PopupNavigation.Instance.PopAsync(true);
            }
            IsBusy = false;
        }

        private async Task DeletePhotoAction(object obj)
        {
            var image = (BusinessPhotos)obj;
            if (image != null)
            {
                UploadedImagesList.Remove(image);
                if (UploadedImagesList.Count == 0)
                    IsUploadPhotoViewVisible = false;
            }
        }

        private async Task UploadBusinessLogoAction()
        {
            var action = await Application.Current.MainPage.DisplayActionSheet("Select Photo", "cancle", null, "Camera", "Gallery");
            switch (action)
            {
                case "Camera":
                    if (CrossMedia.Current.IsCameraAvailable && CrossMedia.Current.IsTakePhotoSupported)
                    {
                        var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium,
                        });

                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        BusinessLogo = file.Path;
                        LogoImagePath = file.Path;
                        LogoImageBytes = imageBytes;
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                case "Gallery":
                    if (CrossMedia.Current.IsPickPhotoSupported)
                    {
                        var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium,
                        });
                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        BusinessLogo = file.Path;
                        LogoImagePath = file.Path;
                        LogoImageBytes = imageBytes;
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                default:
                    break;
            }
        }

        private async Task UploadPhotosAction()
        {
            if (UploadedImagesList == null || UploadedImagesList.Count == 0)
                UploadedImagesList = new ObservableCollection<BusinessPhotos>();
            if (UploadedImagesList.Count >= 5)
            {
                await Helper.DisplayAlert("You can upload only 5 images.");
                return;
            }
            var action = await Application.Current.MainPage.DisplayActionSheet("Select Photo", "cancle", null, "Camera", "Gallery");
            switch (action)
            {
                case "Camera":
                    if (CrossMedia.Current.IsCameraAvailable && CrossMedia.Current.IsTakePhotoSupported)
                    {
                        var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium,
                        });

                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        BusinessPhotos businessPhoto = new BusinessPhotos();
                        businessPhoto.UploadedImageSource = streamdata;
                        businessPhoto.FilePath = file.Path;
                        businessPhoto.ImageBytes = imageBytes;
                        UploadedImagesList.Add(businessPhoto);
                        IsUploadPhotoViewVisible = true;
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                case "Gallery":
                    if (CrossMedia.Current.IsPickPhotoSupported)
                    {
                        var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            PhotoSize = PhotoSize.Medium
                        });
                        if (file == null)
                            return;
                        Console.WriteLine("File Path: {0}", file.Path);
                        byte[] imageBytes = File.ReadAllBytes(file.Path);
                        string base64String = Convert.ToBase64String(imageBytes);
                        var streamdata = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        BusinessPhotos businessPhoto = new BusinessPhotos();
                        businessPhoto.UploadedImageSource = streamdata;
                        businessPhoto.FilePath = file.Path;
                        businessPhoto.ImageBytes = imageBytes;
                        UploadedImagesList.Add(businessPhoto);
                        IsUploadPhotoViewVisible = true;
                        //await DependencyService.Get<IMediaService>().OpenGallery();
                    }
                    else
                    {
                        await Helper.DisplayAlert("Not Supported");
                    }
                    break;
                default:
                    break;
            }
        }

        public async Task LoadBusinessData()
        {
            try
            {
                if (isFirstTime)
                {
                    isFirstTime = false;
                    
                    if (SelectedBusinessData != null)
                    {
                        BusinessId = SelectedBusinessData.id;
                        BusinessLogo = SelectedBusinessData.business_logo;
                        if (!SelectedBusinessData.business_logo.Contains("placeholder.png"))
                            LogoImagePath = await FileService.DownloadFile(SelectedBusinessData.business_logo);
                        else
                            LogoImagePath = null;
                        IsLogoImgVisible = true;
                        IsLogoTextVisible = false;
                        BusinessName = SelectedBusinessData.business_name;
                        SelectedBusinessCategory = BusinessSubCategoryList.Where(a => a.id == SelectedBusinessData.category_id).FirstOrDefault();
                        SelectedSubCategory = SelectedBusinessCategory.category_name;
                        AboutBusiness = SelectedBusinessData.business_about;
                        BusinessWebsite = SelectedBusinessData.website;
                        EmailAddress = SelectedBusinessData.email;
                        ContactPerson = SelectedBusinessData.contact_person;
                        ContactNumber = SelectedBusinessData.contact_number;
                        AddressLine1 = SelectedBusinessData.address_line1;
                        AddressLine2 = SelectedBusinessData.address_line2;
                        City = SelectedBusinessData.city;
                        Province = SelectedBusinessData.province;
                        PostalCode = SelectedBusinessData.postalcode;
                        List<string> photos = JsonConvert.DeserializeObject<List<string>>(SelectedBusinessData.upload_photos);

                        UploadedImagesList = new ObservableCollection<BusinessPhotos>();
                        if (photos != null && photos.Count > 0)
                        {
                            foreach (var img in photos)
                            {
                                IsUploadPhotoViewVisible = true;
                                var path = await FileService.DownloadFile(img);

                                UploadedImagesList.Add(new BusinessPhotos() { UploadedImageSource = img, FilePath = path });
                            }
                        }
                        if (SelectedBusinessData.is_active == "1")
                        {
                            IsBusinessActive = true;
                            IsActiveRadioImageSource = ImageStrings.ic_radio_fill;
                            IsInActiveRadioImageSource = ImageStrings.ic_radio_un_fill;
                        }
                        else
                        {
                            IsBusinessActive = false;
                            IsActiveRadioImageSource = ImageStrings.ic_radio_un_fill;
                            IsInActiveRadioImageSource = ImageStrings.ic_radio_fill;
                        }
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
            }
        }

        private async Task OpenCategoryPopup()
        {
            if(BusinessSubCategoryList != null && BusinessSubCategoryList.Count > 0)
                await PopupNavigation.Instance.PushAsync(new SelectCategoryPopupPage(BusinessSubCategoryList, this));
            else
            {
                await SetBusinessCategories();
                await PopupNavigation.Instance.PushAsync(new SelectCategoryPopupPage(BusinessSubCategoryList, this));
            }
        }

        public async Task SelectCategoryAction()
        {

        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrEmpty(Constants.GetRegisterUserId()))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Please login or sign up first in app.!");
                }
                else if (string.IsNullOrWhiteSpace(BusinessName))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Business name can not be blank.!");
                }
                else if (SelectedBusinessCategory == null || string.IsNullOrWhiteSpace(SelectedSubCategory))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Please select business category.!");
                }
                else if (string.IsNullOrWhiteSpace(AboutBusiness))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("About business / service offer can not be blank.!");
                }
                else if(!string.IsNullOrWhiteSpace(BusinessWebsite) && !WebsiteRegex.IsMatch(BusinessWebsite))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Please enter valid Website.!");
                }
                else if (string.IsNullOrWhiteSpace(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.emailBlankAlert);
                }
                else if (!Helper.VerifyEmailAddress(EmailAddress))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert(TextStrings.please_enter_valid_email);
                }
                else if (string.IsNullOrWhiteSpace(ContactPerson))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Contact Person can not be blank.!");
                }
                else if(!string.IsNullOrWhiteSpace(ContactPerson) && !ContactNameRegex.IsMatch(ContactPerson))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Please enter valid Contact Person.!");
                }
                else if (string.IsNullOrWhiteSpace(ContactNumber))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Contact Number can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(AddressLine1))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("AddressLine1 can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(City))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("City can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(PostalCode))
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Postal Code can not be blank.!");
                }
                else if(IsActiveRadioImageSource == ImageStrings.ic_radio_un_fill
                    && IsInActiveRadioImageSource == ImageStrings.ic_radio_un_fill)
                {
                    isModelValid = false;
                    await Helper.DisplayAlert("Please select the status of your business.");
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                await PopupNavigation.Instance.PopAsync(true);
                return false;
            }
        }
    }
}