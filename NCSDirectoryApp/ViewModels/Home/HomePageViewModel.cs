﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.BusinessModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.AllBusinessCategories;
using NCSDirectoryApp.Views.Authentication;
using NCSDirectoryApp.Views.BusinessListing;
using NCSDirectoryApp.Views.BusinessLists;
using NCSDirectoryApp.Views.ContactUs;
using NCSDirectoryApp.Views.JobList;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.Home
{
    public class HomePageViewModel : BaseViewModel
    {
        public HomePageViewModel()
        {
        }

        public ICommand ShowMenuCommand => new Command(async () => await ShowMenuAction());
        public ICommand SelectBusinessCommand => new Command<object>(async (obj) => await BusniessCategorySelectAction(obj));
        public ICommand ContactUsCommand => new Command(async () => await ContactUsAction());
        public ICommand HomeCommand => new Command(async () => await HomeAction());
        public ICommand ShowMoreCommand => new Command(async () => await ShowMoreAction());
        public ICommand ListaBusinessCommand => new Command(async () => await ListaBusinessAction());

        private ObservableCollection<ChildCategoryDetails> _businessSubCategoryList;
        public ObservableCollection<ChildCategoryDetails> BusinessSubCategoryList
        {
            get { return _businessSubCategoryList; }
            set
            {
                _businessSubCategoryList = value;
                OnPropertyChanged(nameof(BusinessSubCategoryList));
            }
        }

        private double _categoryListHeight;
        public double CategoryListHeight
        {
            get { return _categoryListHeight; }
            set
            {
                _categoryListHeight = value;
                OnPropertyChanged(nameof(CategoryListHeight));
            }
        }

        private async Task ShowMenuAction()
        {
            MessagingCenter.Send(EventArgs.Empty, "Open Menu");
        }

        private async Task ContactUsAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await Application.Current.MainPage.Navigation.PushAsync(new ContactUsPage());
            IsBusy = false;
        }

        private async Task HomeAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await App.CurrentInstance.LoadHomePage();
            IsBusy = false;
        }

        private async Task ShowMoreAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await Application.Current.MainPage.Navigation.PushAsync(new AllBusinessCategoriesPage());
            IsBusy = false;
        }

        private async Task BusniessCategorySelectAction(object obj)
        {
            var selectedCategory = (ChildCategoryDetails)obj;
            if (selectedCategory != null)
            {
                if (selectedCategory.category_name.ToLower() == "Jobs".ToLower())
                    await Application.Current.MainPage.Navigation.PushAsync(new JobListPage());
                else
                    await Application.Current.MainPage.Navigation.PushAsync(new BusinessListPage(selectedCategory));
            }
        }

        private async Task ListaBusinessAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            if (Constants.GetIsLogin())
                await Application.Current.MainPage.Navigation.PushAsync(new UsersBusinessListPage());
            else
            {
                //await Helper.DisplayAlert("Please Login First.!", true);
                await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
            }
            IsBusy = false;
        }

        public async Task GetAllBusinessList()
        {
            try
            {
                var categories = Constants.GetHomeScreenCategories();
                if (!string.IsNullOrWhiteSpace(categories))
                    App.HomeScreenCategories = JsonConvert.DeserializeObject<List<ChildCategoryDetails>>(categories);
                if (App.HomeScreenCategories == null || App.HomeScreenCategories.Count == 0)
                {
                    if (App.IsInternet)
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.GetAsync(ApiPathStrings.getHomeScreenCategories);
                        await PopupNavigation.Instance.PopAsync();
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            if (result.Item1.ToLower() == "OK".ToLower())
                            {
                                HomeScreenCategoryResponse response = JsonConvert.DeserializeObject<HomeScreenCategoryResponse>(result.Item2);
                                if (response != null && response.status)
                                {
                                    if (response.data != null && response.data.Count > 0)
                                    {
                                        App.HomeScreenCategories = new List<ChildCategoryDetails>();
                                        foreach (var parent in response.data)
                                        {
                                            App.HomeScreenCategories.Add(parent);
                                        }
                                        var homeScreenCategories = JsonConvert.SerializeObject(App.HomeScreenCategories);
                                        Constants.SaveHomeScreenCategories(homeScreenCategories);
                                    }
                                }
                                else
                                    await Helper.DisplayAlert("Data not found.!");
                            }
                            else
                            {
                                ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                                await Helper.DisplayAlert(response.message);
                            }
                        }
                        else
                            await Helper.DisplayAlert("Can't find data!");
                    }
                    else
                        await Helper.DisplayAlert("Please check your internet connection.!");
                }

                BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>();
                foreach (var businessCategory in App.HomeScreenCategories)
                {
                    BusinessSubCategoryList.Add(businessCategory);
                }
                CategoryListHeight = (BusinessSubCategoryList.Count / 4) * 105;
                //await PopupNavigation.Instance.PopAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
            }
        }
    }
}