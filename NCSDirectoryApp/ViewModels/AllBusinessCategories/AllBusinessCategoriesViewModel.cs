﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.BusinessLists;
using NCSDirectoryApp.Views.JobList;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.AllBusinessCategories
{
    public class AllBusinessCategoriesViewModel : BaseViewModel
    {
        public AllBusinessCategoriesViewModel()
        {
        }

        public ICommand ParentCatSelectCommand => new Command<object>(async (obj) => await ParentCatSelectAction(obj));
        public ICommand SelectBusinessSubCatCommand => new Command<object>(async (obj) => await BusniessCategorySelectAction(obj));

        private ObservableCollection<ChildCategoryDetails> _businessSubCategoryList;
        public ObservableCollection<ChildCategoryDetails> BusinessSubCategoryList
        {
            get { return _businessSubCategoryList; }
            set
            {
                _businessSubCategoryList = value;
                OnPropertyChanged(nameof(BusinessSubCategoryList));
            }
        }

        private ObservableCollection<ChildCategoryDetails> _searchItemsList;
        public ObservableCollection<ChildCategoryDetails> SearchItemsList
        {
            get { return _searchItemsList; }
            set
            {
                _searchItemsList = value;
                OnPropertyChanged(nameof(SearchItemsList));
            }
        }

        private ObservableCollection<ParentCategoryDetails> _businessParentCategoryList;
        public ObservableCollection<ParentCategoryDetails> BusinessParentCategoryList
        {
            get { return _businessParentCategoryList; }
            set
            {
                _businessParentCategoryList = value;
                OnPropertyChanged(nameof(BusinessParentCategoryList));
            }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                OnPropertyChanged(nameof(SearchText));
                SearchBusinessCategories(value);
            }
        }

        private bool _isSearchResultVisible = false;
        public bool IsSearchResultVisible
        {
            get { return _isSearchResultVisible; }
            set
            {
                _isSearchResultVisible = value;
                OnPropertyChanged(nameof(IsSearchResultVisible));
            }
        }

        private bool _isAllResultVisible = true;
        public bool IsAllResultVisible
        {
            get { return _isAllResultVisible; }
            set
            {
                _isAllResultVisible = value;
                OnPropertyChanged(nameof(IsAllResultVisible));
            }
        }

        private bool _isSearchItemVisible;
        public bool IsSearchItemVisible
        {
            get { return _isSearchItemVisible; }
            set
            {
                _isSearchItemVisible = value;
                OnPropertyChanged(nameof(IsSearchItemVisible));
            }
        }

        private bool _isNoDataTextVisible;
        public bool IsNoDataTextVisible
        {
            get { return _isNoDataTextVisible; }
            set
            {
                _isNoDataTextVisible = value;
                OnPropertyChanged(nameof(IsNoDataTextVisible));
            }
        }

        public async Task ShowAllCategories()
        {
            try
            {
                BusinessParentCategoryList = new ObservableCollection<ParentCategoryDetails>();
                BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>();
                ParentCategoryDetails parentCategory = new ParentCategoryDetails();
                parentCategory.category_name = "All";
                parentCategory.IsCategorySelected = true;
                BusinessParentCategoryList.Add(parentCategory);
                if (App.AllBusinessParentCategories == null || App.AllBusinessParentCategories.Count == 0
                    || App.AllBusinessSubCategories == null || App.AllBusinessSubCategories.Count == 0)
                {
                    await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                    var result = await ApiService.GetAsync(ApiPathStrings.get_all_business_categories_url);
                    await PopupNavigation.Instance.PopAsync(true);
                    if (!string.IsNullOrWhiteSpace(result.Item2))
                    {
                        if (result.Item1.ToLower() == "OK".ToLower())
                        {
                            BusinessCategoriesResponse response = JsonConvert.DeserializeObject<BusinessCategoriesResponse>(result.Item2);
                            if (response != null && response.status)
                            {
                                if (response.data != null && response.data.level_one != null && response.data.level_one.Count > 0)
                                {
                                    App.AllBusinessParentCategories = new List<ParentCategoryDetails>();
                                    App.AllBusinessSubCategories = new List<ChildCategoryDetails>();
                                    foreach (var parent in response.data.level_one)
                                    {
                                        parent.IsCategorySelected = false;
                                        App.AllBusinessParentCategories.Add(parent);
                                        if (parent.level_two != null && parent.level_two.Count > 0)
                                        {
                                            foreach (var subCat in parent.level_two)
                                            {
                                                App.AllBusinessSubCategories.Add(subCat);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ResponseModel response = JsonConvert.DeserializeObject<ResponseModel>(result.Item2);
                            await Helper.DisplayAlert(response.message);
                        }
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
                App.AllBusinessParentCategories = App.AllBusinessParentCategories.OrderBy(a => a.category_name).ToList();
                App.AllBusinessSubCategories = App.AllBusinessSubCategories.OrderBy(a => a.category_name).ToList();
                foreach (var parent in App.AllBusinessParentCategories)
                {
                    parent.IsCategorySelected = false;
                    BusinessParentCategoryList.Add(parent);
                }
                foreach (var businessCategory in App.AllBusinessSubCategories)
                {
                    BusinessSubCategoryList.Add(businessCategory);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task ParentCatSelectAction(object obj)
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                var selecedParentCategory = (ParentCategoryDetails)obj;
                if (selecedParentCategory != null)
                {
                    foreach (var parent in BusinessParentCategoryList)
                    {
                        if (parent == selecedParentCategory)
                            parent.IsCategorySelected = true;
                        else
                            parent.IsCategorySelected = false;
                    }
                    BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>();
                    if (App.AllBusinessSubCategories != null && App.AllBusinessSubCategories.Count > 0)
                    {
                        if (selecedParentCategory.category_name != "All")
                        {
                            foreach (var subcategory in App.AllBusinessSubCategories)
                            {
                                if (subcategory.parent_id == selecedParentCategory.id)
                                {
                                    BusinessSubCategoryList.Add(subcategory);
                                }
                            }
                        }
                        else
                        {
                            foreach (var subcategory in App.AllBusinessSubCategories)
                            {
                                BusinessSubCategoryList.Add(subcategory);
                            }
                        }
                    }
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
            }
            IsBusy = false;
        }

        private async Task BusniessCategorySelectAction(object obj)
        {
            var selectedCategory = (ChildCategoryDetails)obj;
            if (selectedCategory != null)
            {
                if (selectedCategory.category_name.ToLower() == "Jobs".ToLower())
                    await Application.Current.MainPage.Navigation.PushAsync(new JobListPage());
                else
                    await Application.Current.MainPage.Navigation.PushAsync(new BusinessListPage(selectedCategory));
            }
        }

        public async Task SearchBusinessCategories(string value)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(value) && App.AllBusinessSubCategories != null && App.AllBusinessSubCategories.Count > 0)
                {
                    IsSearchResultVisible = true;
                    IsAllResultVisible = false;
                    SearchItemsList = new ObservableCollection<ChildCategoryDetails>();
                    foreach (var category in App.AllBusinessSubCategories)
                    {
                        if (category.category_name.ToLower().Contains(value.ToLower()))
                            SearchItemsList.Add(category);
                    }
                    if (SearchItemsList.Count == 0)
                    {
                        IsSearchItemVisible = false;
                        IsNoDataTextVisible = true;
                    }
                    else
                    {
                        IsSearchItemVisible = true;
                        IsNoDataTextVisible = false;
                    }
                }
                else
                {
                    IsAllResultVisible = true;
                    IsSearchResultVisible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}