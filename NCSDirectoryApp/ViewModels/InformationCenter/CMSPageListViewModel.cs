﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.InformationCenter;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.InformationCenter
{
    public class CMSPageListViewModel : BaseViewModel
    {
        public CMSPageListViewModel()
        {
            PageTitle = TextStrings.information_center;
            PageIcon = ImageStrings.ic_about_light;
        }

        public ICommand PageTypeSelectCommand => new Command<object>(async (obj) => await PageTypeSelectAction(obj));

        private ObservableCollection<CMSPageData> _cMSPagesList;
        public ObservableCollection<CMSPageData> CMSPagesList
        {
            get { return _cMSPagesList; }
            set
            {
                _cMSPagesList = value;
                OnPropertyChanged(nameof(CMSPagesList));
            }
        }


        private async Task PageTypeSelectAction(object obj)
        {
            var CmsPage = (CMSPageData)obj;
            await Application.Current.MainPage.Navigation.PushAsync(new InformationCenterPage(CmsPage));
        }

        public async Task LoadPageList()
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());

                var result = await ApiService.GetAsync(ApiPathStrings.cmsPagesUrl);
                await PopupNavigation.Instance.PopAsync();
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    CMSPageResponse response = JsonConvert.DeserializeObject<CMSPageResponse>(result.Item2);
                    if (response != null)
                    {
                        if (result.Item1 == "OK")
                        {
                            if (response.data != null && response.data.Count > 0)
                            {
                                CMSPagesList = new ObservableCollection<CMSPageData>();
                                foreach(var item in response.data)
                                {
                                    if(item.page_title.ToLower().Contains("Useful".ToLower()))
                                    {
                                        CMSPagesList.Add(item);
                                    }
                                }
                            }
                            else
                            {
                                await Helper.DisplayAlert("User data not found.!");
                            }
                        }
                        else
                        {
                            await Helper.DisplayAlert(response.message);
                        }
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
                else
                    await Helper.DisplayAlert("Error.!");
            }
            catch (Exception ex)
            {
            }
        }
    }
}
