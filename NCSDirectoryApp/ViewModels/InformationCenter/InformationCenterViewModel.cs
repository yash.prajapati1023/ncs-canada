﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.InformationCenter
{
    public class InformationCenterViewModel : BaseViewModel
    {
        public CMSPageData CurrentPage { get; set; }

        public InformationCenterViewModel(CMSPageData cMSPage)
        {
            PageTitle = TextStrings.information_center;
            PageIcon = ImageStrings.ic_about_light;
            SelectedPageType = cMSPage.page_title;
            CurrentPage = cMSPage;
        }

        public ICommand PageTypeSelectCommand => new Command<string>(async (obj) => await PageTypeSelectAction(obj));
        public ICommand SelectItemCommand => new Command<object>(async (obj) => await SelectItemAction(obj));

        private string _selectedPageType;
        public string SelectedPageType
        {
            get { return _selectedPageType; }
            set
            {
                _selectedPageType = value;
                OnPropertyChanged(nameof(SelectedPageType));
            }
        }

        private ObservableCollection<UsefulItems> _usefulItemsList;
        public ObservableCollection<UsefulItems> UsefulItemsList
        {
            get { return _usefulItemsList; }
            set
            {
                _usefulItemsList = value;
                OnPropertyChanged(nameof(UsefulItemsList));
            }
        }

        private async Task PageTypeSelectAction(string pageType)
        {
            UsefulItemsList = new ObservableCollection<UsefulItems>();
            if(pageType == "Websites")
            {
                SelectedPageType = TextStrings.useful_websites;
                await CallUsefulItemApi(ApiPathStrings.usefulwebsites_url);
            }
            else if(pageType == "MobilePhones")
            {
                SelectedPageType = TextStrings.useful_phone_no;
                await CallUsefulItemApi(ApiPathStrings.usefulphonenumbers_url);
            }
        }

        private async Task SelectItemAction(object obj)
        {
            try
            {
                var selectedItem = (UsefulItems)obj;
                if (selectedItem != null)
                {
                    if (SelectedPageType.ToLower().Contains("website".ToLower()))
                    {
                        Device.OpenUri(new Uri(selectedItem.UsefulItemTitle));
                    }
                    else if (SelectedPageType.ToLower().Contains("phone".ToLower()))
                    {
                        try
                        {
                            PhoneDialer.Open(selectedItem.UsefulItemTitle);
                        }
                        catch (ArgumentNullException anEx)
                        {
                            // Number was null or white space
                            await Helper.DisplayAlert("" + anEx.Message);
                        }
                        catch (FeatureNotSupportedException ex)
                        {
                            // Phone Dialer is not supported on this device.
                            await Helper.DisplayAlert("" + ex.Message);

                        }
                        catch (Exception ex)
                        {
                            await Helper.DisplayAlert("" + ex.Message);
                        }
                    }
                }
            }
            catch(Exception ex) { }
        }

        public async Task CallUsefulItemApi(string id)
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());

                var result = await ApiService.GetAsync(ApiPathStrings.cmsPagesUrl +"/"+id);
                await PopupNavigation.Instance.PopAsync();
                if (!string.IsNullOrWhiteSpace(result.Item2))
                {
                    CMSPageResponse response = JsonConvert.DeserializeObject<CMSPageResponse>(result.Item2);
                    if (response != null)
                    {
                        if (result.Item1 == "OK")
                        {
                            if (response.data != null)
                            {
                                if (SelectedPageType == TextStrings.useful_phone_no)
                                    await ShowUsefulPhoneNos(response.data.FirstOrDefault().content);
                                else
                                    await ShowUsefulWebsites(response.data.FirstOrDefault().content);
                            }
                            else
                            { 
                                await Helper.DisplayAlert("User data not found.!");
                            }
                        }
                        else
                        {
                            await Helper.DisplayAlert(response.message);
                        }
                    }
                    else
                        await Helper.DisplayAlert("Error.!");
                }
                else
                    await Helper.DisplayAlert("Error.!");
            }
            catch (Exception ex)
            {
            }
        }

        public async Task ShowUsefulPhoneNos(string content)
        {
            if (!string.IsNullOrWhiteSpace(content))
            {
                var splitString = content.Split('\n');
                UsefulItemsList = new ObservableCollection<UsefulItems>();
                foreach (var item in splitString)
                {
                    if (item.Contains("<p>") && item.Contains("</p>"))
                    {
                        var txt1 = item.Replace("\r", "");
                        var txt2 = txt1.Replace("<p>", "");
                        var newItm = txt2.Replace("</p>", "");
                        UsefulItems obj = new UsefulItems();
                        obj.UsefulItemIcon = ImageStrings.ic_smartphone;
                        obj.UsefulItemTitle = newItm;
                        UsefulItemsList.Add(obj);
                    }
                }
            }
            else
            {
                await PopupNavigation.Instance.PopAsync(true);
                await Helper.DisplayAlert("No any useful phone numbers available.!");
                
            }
        }

        public async Task ShowUsefulWebsites(string content)
        {
            if (!string.IsNullOrWhiteSpace(content))
            {
                var splitString = content.Split(',');
                UsefulItemsList = new ObservableCollection<UsefulItems>();
                foreach (var item in splitString)
                {
                    /*if (item.Contains("<p>") && item.Contains("</p>"))
                    {
                        item.Replace("\r", "");
                        UsefulItems obj = new UsefulItems();
                        obj.UsefulItemIcon = ImageStrings.ic_link;
                        obj.UsefulItemTitle = item;
                        UsefulItemsList.Add(obj);
                    }*/
                    var txt1 = item.Replace("\r", "");
                    var txt2 = txt1.Replace("<p>", "");
                    var newItm = txt2.Replace("</p>", "");
                    UsefulItems obj = new UsefulItems();
                    obj.UsefulItemIcon = ImageStrings.ic_globe;
                    obj.UsefulItemTitle = newItm;
                    obj.UsefulLink = newItm;
                    UsefulItemsList.Add(obj);
                }
            }
            else
            {
                await PopupNavigation.Instance.PopAsync(true);
                await Helper.DisplayAlert("No any useful websites available.!");

            }
        }
    }
}