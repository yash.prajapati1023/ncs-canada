﻿using NCSDirectoryApp.ApiServices;
using NCSDirectoryApp.Models.RequestModels;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.PopupPages;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.ContactUs
{
    public class ContactUsPageViewModel : BaseViewModel
    {
        public ContactUsPageViewModel()
        {
            PageIcon = ImageStrings.ic_contact_light;
            PageTitle = TextStrings.contact_us;
        }

        public ICommand SendComand => new Command(async () => await SendAction());

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                OnPropertyChanged(nameof(EmailAddress));
            }
        }

        private string _mobileNumber;
        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                _mobileNumber = value;
                OnPropertyChanged(nameof(MobileNumber));
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private string _subject;
        public string Subject
        {
            get { return _subject; }
            set
            {
                _subject = value;
                OnPropertyChanged(nameof(Subject));
            }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        private async Task SendAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            
            try
            {
                bool isValid = await ValidateModel();
                if (isValid)
                {
                    ContactUsRequest contactUsRequest = new ContactUsRequest();
                    contactUsRequest.name = Name;
                    contactUsRequest.email = EmailAddress;
                    contactUsRequest.message = Message;
                    contactUsRequest.phone = MobileNumber;
                    contactUsRequest.subject = Subject;

                    string request = JsonConvert.SerializeObject(contactUsRequest);
                    if (!string.IsNullOrWhiteSpace(request))
                    {
                        await PopupNavigation.Instance.PushAsync(new LoaderPopupPage());
                        var result = await ApiService.PostAsync(ApiPathStrings.contact_us_url, request);
                        await PopupNavigation.Instance.PopAsync();
                        if (!string.IsNullOrWhiteSpace(result.Item2))
                        {
                            RegisterResponse response = JsonConvert.DeserializeObject<RegisterResponse>(result.Item2);
                            if(response != null)
                            {
                                if (result.Item1 == "OK")
                                {
                                    App.IsFromForgotPassword = false;
                                    await Helper.DisplayAlert("Your details are sent successfully. Our representative will contact you soon.");
                                    IsBusy = false;
                                    Name = EmailAddress = MobileNumber = Message = Subject = null;
                                    await App.CurrentInstance.LoadHomePage();
                                }
                                else
                                    await Helper.DisplayAlert(response.message);
                            }
                        }
                        else
                            await Helper.DisplayAlert("Error.!");
                    }
                }
            }
            catch(Exception ex)
            {
                IsBusy = false;
            }
            IsBusy = false;
        }

        public async Task<bool> ValidateModel()
        {
            bool isModelValid = true;
            try
            {
                if (string.IsNullOrWhiteSpace(Name))
                {
                    isModelValid = false;
                    
                    await Helper.DisplayAlert(TextStrings.nameBlankAlert);
                }
                else if (string.IsNullOrWhiteSpace(EmailAddress))
                {
                    isModelValid = false;

                    await Helper.DisplayAlert(TextStrings.emailBlankAlert);
                }
                else if (!Helper.VerifyEmailAddress(EmailAddress))
                {
                    isModelValid = false;

                    await Helper.DisplayAlert(TextStrings.please_enter_valid_email);
                    return isModelValid;
                }
                else if (string.IsNullOrWhiteSpace(MobileNumber))
                {
                    isModelValid = false;
                    
                    await Helper.DisplayAlert("Contact Number can not be blank.!");
                }
                else if (string.IsNullOrWhiteSpace(Subject))
                {
                    isModelValid = false;

                    await Helper.DisplayAlert(TextStrings.subjectBlankAlert);
                    return isModelValid;
                }
                else if (string.IsNullOrWhiteSpace(Message))
                {
                    isModelValid = false;
                    
                    await Helper.DisplayAlert(TextStrings.messageBlankAlert);
                    return isModelValid;
                }
                return isModelValid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
