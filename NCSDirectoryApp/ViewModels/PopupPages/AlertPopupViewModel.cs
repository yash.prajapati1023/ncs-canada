﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.Views.Authentication;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class AlertPopupViewModel : BaseViewModel
    {
        public bool IsLoginAlert { get; set; }

        public AlertPopupViewModel(string message, bool isLoginAlert)
        {
            AlertMessage = message;
            this.IsLoginAlert = isLoginAlert;
        }

        public ICommand OkCommand => new Command(async () => await OkAction());

        private string _alertMessage;
        public string AlertMessage
        {
            get { return _alertMessage; }
            set
            {
                _alertMessage = value;
                OnPropertyChanged(nameof(AlertMessage));
            }
        }

        private async Task OkAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            App.IsPopupOpen = false;
            await PopupNavigation.Instance.PopAsync(true);
            if(IsLoginAlert)
                DependencyService.Get<ILocSettings>().OpenSettings();
            IsBusy = false;
        }
    }
}
