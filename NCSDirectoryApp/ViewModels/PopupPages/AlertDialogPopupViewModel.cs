﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Enums;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.ViewModels.BusinessLists;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class AlertDialogPopupViewModel : BaseViewModel
    {
        public BusinessData SelectedBusiness { get; set; }
        UsersBusinessListPageViewModel PageViewModel { get; set; }
        public PageType SelectedPage { get; set; }
        public AlertDialogPopupViewModel(BusinessData business, UsersBusinessListPageViewModel pageViewModel, PageType pageType = PageType.Default)
        {
            this.SelectedBusiness = business;
            this.PageViewModel = pageViewModel;
            this.SelectedPage = pageType;
            if (SelectedPage == PageType.Logout)
                Message = "Are you sure you want to Logout?";
        }

        public ICommand YesCommand => new Command(async () => await YesAction());
        public ICommand NoCommand => new Command(async () => await NoAction());

        private string _message = "Are you sure you want to delete this?";
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        async Task YesAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await PopupNavigation.Instance.PopAsync();
            if(SelectedPage == PageType.Logout)
            {
                Constants.SaveUserData(null);
                Constants.SetIsLogin(false);
                Constants.SaveRegisterUserId(null);
                await App.CurrentInstance.LoadHomePage();
            }
            else
            {
                await PageViewModel.DeleteBusiness(SelectedBusiness);
            }
            IsBusy = false;
        }
        async Task NoAction()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await PopupNavigation.Instance.PopAsync();
            IsBusy = false;
        }
    }
}