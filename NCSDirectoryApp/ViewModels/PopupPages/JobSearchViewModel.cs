﻿using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.ViewModels.JobList;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class JobSearchViewModel : BaseViewModel
    {
        public JobListPageViewModel jobListPageViewModel;
        public JobSearchViewModel(JobListPageViewModel _viewModel)
        {
            this.jobListPageViewModel = _viewModel;
        }

        public ICommand ApplyCommand => new Command(async () => await ApplySearchAction());

        private string _jobTitle;
        public string JobTitle
        {
            get { return _jobTitle; }
            set
            {
                _jobTitle = value;
                OnPropertyChanged(nameof(JobTitle));
            }
        }

        private string _location;
        public string Location
        {
            get { return _location; }
            set
            {
                _location = value;
                OnPropertyChanged(nameof(Location));
            }
        }

        private string _category;
        public string Category
        {
            get { return _category; }
            set
            {
                _category = value;
                OnPropertyChanged(nameof(Category));
            }
        }

        private async Task ApplySearchAction()
        {
            await PopupNavigation.Instance.PopAsync(true);
            App.IsFromSearch = true;
            await jobListPageViewModel.ApplyJobSearch(JobTitle, Location, Category);
        }
    }
}