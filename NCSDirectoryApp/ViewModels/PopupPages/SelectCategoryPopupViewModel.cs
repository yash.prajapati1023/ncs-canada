﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Models.ResponseModels;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.ViewModels.BusinessListing;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class SelectCategoryPopupViewModel : BaseViewModel
    {
        ObservableCollection<ChildCategoryDetails> SubCategoryList;
        BusinessListingPageViewModel BusinessListingPageView;
        bool IsFromCategorySelection { get; set; } = false;

        public SelectCategoryPopupViewModel(ObservableCollection<ChildCategoryDetails> subCategoryList, BusinessListingPageViewModel viewPage)
        {
            this.SubCategoryList = subCategoryList;
            this.BusinessListingPageView = viewPage;
        }

        public ICommand CategorySelectCommand => new Command<object>(async (obj) => await CategorySelectAction(obj));
        public ICommand SelectCommand => new Command(async () => await SelectAction());

        private ObservableCollection<ChildCategoryDetails> _businessSubCategoryList;
        public ObservableCollection<ChildCategoryDetails> BusinessSubCategoryList
        {
            get { return _businessSubCategoryList; }
            set
            {
                _businessSubCategoryList = value;
                OnPropertyChanged(nameof(BusinessSubCategoryList));
            }
        }

        private ChildCategoryDetails _selectedSubCategory;
        public ChildCategoryDetails SelectedSubCategory
        {
            get { return _selectedSubCategory; }
            set
            {
                _selectedSubCategory = value;
                OnPropertyChanged(nameof(SelectedSubCategory));
            }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                OnPropertyChanged(nameof(SearchText));
                SearchAction();
            }
        }

        private async Task CategorySelectAction(object obj)
        {
            var selectedCategory = (ChildCategoryDetails)obj;
            if(selectedCategory != null)
            {
                SelectedSubCategory = selectedCategory;
                foreach(var item in SubCategoryList)
                {
                    if(item == SelectedSubCategory)
                    {
                        if (SelectedSubCategory.IsSelected)
                        {
                            item.IsSelected = SelectedSubCategory.IsSelected = false;
                            item.SelectIconImage = SelectedSubCategory.SelectIconImage = ImageStrings.ic_radio_un_fill;
                        }
                        else
                        {
                            item.IsSelected = SelectedSubCategory.IsSelected = true;
                            item.SelectIconImage = SelectedSubCategory.SelectIconImage = ImageStrings.ic_radio_fill;
                        }
                    }
                    else
                    {
                        item.IsSelected = false;
                        item.SelectIconImage = ImageStrings.ic_radio_un_fill;
                    }
                }
                //foreach (var item in BusinessSubCategoryList)
                //{
                //    if (item == SelectedSubCategory)
                //    {
                //        if (SelectedSubCategory.IsSelected)
                //        {
                //            item.IsSelected = SelectedSubCategory.IsSelected = false;
                //            item.SelectIconImage = SelectedSubCategory.SelectIconImage = ImageStrings.ic_radio_un_fill;
                //        }
                //        else
                //        {
                //            item.IsSelected = SelectedSubCategory.IsSelected = true;
                //            item.SelectIconImage = SelectedSubCategory.SelectIconImage = ImageStrings.ic_radio_fill;
                //        }
                //    }
                //    else
                //    {
                //        item.IsSelected = false;
                //        item.SelectIconImage = ImageStrings.ic_radio_un_fill;
                //    }
                //}
                //IsFromCategorySelection = true;
                //if (!string.IsNullOrWhiteSpace(SearchText))
                //    SearchText = "";
                //BusinessSubCategoryList = SubCategoryList;
                //MessagingCenter.Send(EventArgs.Empty, "ScrollToCategory");
                //IsFromCategorySelection = false;
            }
        }

        public void LoadCategoryList()
        {
            BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>();
            BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>(this.SubCategoryList.OrderBy(a => a.category_name));
        }

        public void SearchAction()
        {
            BusinessSubCategoryList = new ObservableCollection<ChildCategoryDetails>(this.SubCategoryList.Where(a => a.category_name.ToLower().Contains(SearchText.ToLower())).OrderBy(a => a.category_name));
            //if (!IsFromCategorySelection)
            //    MessagingCenter.Send(EventArgs.Empty, "ScrollToTop");
        }

        public async Task SelectAction()
        {
            if(SelectedSubCategory != null && SelectedSubCategory.IsSelected)
            {
                BusinessListingPageView.SelectedBusinessCategory = SelectedSubCategory;
                BusinessListingPageView.SelectedSubCategory = SelectedSubCategory.category_name;
                BusinessListingPageView.BusinessSubCategoryList = SubCategoryList;
                await PopupNavigation.Instance.PopAsync();
            }
            else
            {
                await PopupNavigation.Instance.PopAsync();
                await Helper.DisplayAlert("Please select any category.!");
            }
        }
    }
}