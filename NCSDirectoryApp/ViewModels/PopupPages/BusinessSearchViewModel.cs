﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Interfaces;
using NCSDirectoryApp.Resources;
using NCSDirectoryApp.Utilities;
using NCSDirectoryApp.ViewModels.Base;
using NCSDirectoryApp.ViewModels.BusinessLists;
using NCSDirectoryApp.Views.PopupPages;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class BusinessSearchViewModel : BaseViewModel
    {
        BusinessListPageViewModel businessListPageViewModel;
        public BusinessSearchViewModel(BusinessListPageViewModel _viewModel)
        {
            this.businessListPageViewModel = _viewModel;
        }

        public ICommand ApplyCommand => new Command(async () => await ApplySearchAction());
        public ICommand FindNearMeCommand => new Command(async () => await ApplyFindNearMeAction());

        private string _searchKeyword;
        public string SearchKeyword
        {
            get { return _searchKeyword; }
            set
            {
                _searchKeyword = value;
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged(nameof(City));
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private async Task ApplyFindNearMeAction()
        {
            try
            {
                if (App.IsInternet)
                {
                    var hasPermission = await Utils.CheckPermissions(Permission.Location);
                    if (!hasPermission)
                        return;
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        bool gpsStatus = DependencyService.Get<ILocSettings>().isGpsAvailable();
                        if (!gpsStatus)
                        {
                            await Helper.DisplayAlert("Please turn on your location.!");
                            DependencyService.Get<ILocSettings>().OpenSettings();
                        }
                    }
                    if(App.userLat == 0 || App.userLong == 0)
                    {
                        var locator = CrossGeolocator.Current;
                        locator.DesiredAccuracy = 100;

                        var position = await locator.GetLastKnownLocationAsync();

                        if (position != null)
                        {
                            Latitude = position.Latitude.ToString();
                            Longitude = position.Longitude.ToString();
                        }

                    }
                    else
                    {
                        Latitude = App.userLat.ToString();
                        Longitude = App.userLong.ToString();
                    }
                    
                    App.IsFromSearch = true;
                    await PopupNavigation.Instance.PopAsync();
                    await businessListPageViewModel.ApplyBusinessSearch(Latitude, Longitude, null, "20");
                    
                }
                else
                    await Helper.DisplayAlert("Please check your internet connection.!");
            }
            catch (Exception ex)
            {

            }
        }

        private async Task ApplySearchAction()
        {
            if(string.IsNullOrWhiteSpace(City) && string.IsNullOrWhiteSpace(SearchKeyword))
                await Helper.DisplayAlert("Please enter keyword or city, province or postal code.!");
            else
            {
                if (!string.IsNullOrWhiteSpace(City) || SearchKeyword.ToLower().Contains("near me".ToLower()))
                {
                    try
                    {
                        //Geocoder coder = new Geocoder();
                        //IEnumerable<Position> approximateLocations = await coder.GetPositionsForAddressAsync(SearchKeyword);
                        //Position position = approximateLocations.FirstOrDefault();
                        if (App.userLat == 0 || App.userLong == 0)
                        {
                            var locator = CrossGeolocator.Current;
                            locator.DesiredAccuracy = 100;
                            var position = await locator.GetLastKnownLocationAsync();
                            if (position != null)
                            {
                                Latitude = position.Latitude.ToString();
                                Longitude = position.Longitude.ToString();
                            }
                        }
                        else
                        {
                            Latitude = App.userLat.ToString();
                            Longitude = App.userLong.ToString();
                        }
                    }
                    catch(Exception ex) { }
                }
                App.IsFromSearch = true;
                await PopupNavigation.Instance.PopAsync(true);
                if (string.IsNullOrWhiteSpace(SearchKeyword))
                    SearchKeyword = City;
                await businessListPageViewModel.ApplyBusinessSearch(Latitude, Longitude, SearchKeyword, "20");
            }
            
        }
    }
}
