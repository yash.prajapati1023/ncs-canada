﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using NCSDirectoryApp.Models;
using NCSDirectoryApp.ViewModels.Base;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace NCSDirectoryApp.ViewModels.PopupPages
{
    public class FullScreenImageViewModel : BaseViewModel
    {
        ObservableCollection<BusinessPhotos> BusinessPhotosList;
        BusinessPhotos SelectedBusinessPhoto;

        public FullScreenImageViewModel(ObservableCollection<BusinessPhotos> photosList, BusinessPhotos selectedPhoto)
        {
            this.SelectedBusinessPhoto = selectedPhoto;
            this.BusinessPhotosList = photosList;
        }

        public ICommand CloseCommand => new Command(async () => await CloseAction());
        public ICommand CurrentImageChangeCommand => new Command<object>(async (obj) => await CurrentImageChangeAction(obj));
        public ICommand PrevCommand => new Command(async () => await PrevAction());
        public ICommand NextCommand => new Command(async () => await NextAction());

        private ObservableCollection<BusinessPhotos> _imagesList;
        public ObservableCollection<BusinessPhotos> ImagesList
        {
            get { return _imagesList; }
            set
            {
                _imagesList = value;
                OnPropertyChanged(nameof(ImagesList));
            }
        }

        private BusinessPhotos _currentImage;
        public BusinessPhotos CurrentImage
        {
            get { return _currentImage; }
            set
            {
                _currentImage = value;
                OnPropertyChanged(nameof(CurrentImage));
            }
        }

        private bool _isNextArrowVisible;
        public bool IsNextArrowVisible
        {
            get { return _isNextArrowVisible; }
            set
            {
                _isNextArrowVisible = value;
                OnPropertyChanged(nameof(IsNextArrowVisible));
            }
        }

        private bool _isPrevArrowVisible;
        public bool IsPrevArrowVisible
        {
            get { return _isPrevArrowVisible; }
            set
            {
                _isPrevArrowVisible = value;
                OnPropertyChanged(nameof(IsPrevArrowVisible));
            }
        }

        private string _imageCountText = "1/5";
        public string ImageCountText
        {
            get { return _imageCountText; }
            set
            {
                _imageCountText = value;
                OnPropertyChanged(nameof(ImageCountText));
            }
        }

        public void LoadPhotosList()
        {
            ImagesList = new ObservableCollection<BusinessPhotos>();
            ImagesList = BusinessPhotosList;
            if(SelectedBusinessPhoto != null)
            {
                CurrentImage = SelectedBusinessPhoto;
            }
            ImageCountText = "" + (ImagesList.IndexOf(SelectedBusinessPhoto) + 1) + "/" + ImagesList.Count;
            if (ImagesList.Count - 1 > ImagesList.IndexOf(SelectedBusinessPhoto))
                IsNextArrowVisible = true;
            else
                IsNextArrowVisible = false;
            if (ImagesList.IndexOf(SelectedBusinessPhoto) == 0)
                IsPrevArrowVisible = false;
            else
                IsPrevArrowVisible = true;
        }

        private async Task CloseAction()
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private async Task CurrentImageChangeAction(object obj)
        {
            if (obj != null)
            {
                //var view = (BusinessPhotos)obj;
                ImageCountText = "" + (ImagesList.IndexOf(CurrentImage) + 1) + "/" + ImagesList.Count;
                if (ImagesList.Count - 1 > ImagesList.IndexOf(CurrentImage))
                    IsNextArrowVisible = true;
                else
                    IsNextArrowVisible = false;
                if (ImagesList.IndexOf(CurrentImage) == 0)
                    IsPrevArrowVisible = false;
                else
                    IsPrevArrowVisible = true;
            }
        }

        private async Task PrevAction()
        {
            var index = ImagesList.IndexOf(CurrentImage);
            if (index == 0)
                CurrentImage = ImagesList[ImagesList.Count - 1];
            else
                CurrentImage = ImagesList[index - 1];

        }

        private async Task NextAction()
        {
            var index = ImagesList.IndexOf(CurrentImage);
            if (index == ImagesList.Count - 1)
                CurrentImage = ImagesList[0];
            else
                CurrentImage = ImagesList[index + 1];
        }
    }
}