﻿using System;
namespace NCSDirectoryApp.Interfaces
{
    public interface ILocSettings
    {
        void OpenSettings();
        bool isGpsAvailable();
    }
}
