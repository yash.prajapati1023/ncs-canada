﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace NCSDirectoryApp.Interfaces
{
    public interface IFileService
    {
        Task<string> SavePicture(string name, Stream data, string location = "temp");
        Task<string> DownloadFile(string url);
    }
}
