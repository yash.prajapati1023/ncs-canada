﻿using System;
namespace NCSDirectoryApp.Interfaces
{
    public interface IStatusBar
    {
        void SetLightStatusBarColor();
    }
}
