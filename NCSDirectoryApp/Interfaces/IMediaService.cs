﻿using System;
using System.Threading.Tasks;

namespace NCSDirectoryApp.Interfaces
{
    public interface IMediaService
    {
        Task OpenGallery();
    }
}
