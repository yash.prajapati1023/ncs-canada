﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using NCSDirectoryApp.Droid.DependencyServices;
using NCSDirectoryApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileService))]
namespace NCSDirectoryApp.Droid.DependencyServices
{
    public class FileService : IFileService
    {
        public FileService()
        {
        }

        public async Task<string> SavePicture(string name, Stream data, string location = "temp")
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            documentsPath = Path.Combine(documentsPath, "Orders", location);
            Directory.CreateDirectory(documentsPath);

            string filePath = Path.Combine(documentsPath, name);

            byte[] bArray = new byte[data.Length];
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (data)
                {
                    data.Read(bArray, 0, (int)data.Length);
                }
                int length = bArray.Length;
                fs.Write(bArray, 0, length);
            }
            return filePath;
        }

        async public Task<string> DownloadFile(string url)
        {
            string folderPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Images", "temp");
            string fileName = url.ToString().Split('/').Last();
            string filePath = System.IO.Path.Combine(folderPath, fileName);
            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadDataCompleted += (s, e) =>
                {
                    Directory.CreateDirectory(folderPath);

                    File.WriteAllBytes(filePath, e.Result);
                    
                };
                webClient.DownloadDataAsync(new Uri(url));
                return filePath;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}