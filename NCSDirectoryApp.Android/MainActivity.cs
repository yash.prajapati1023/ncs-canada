﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using AndroidX.AppCompat.App;
using Plugin.CurrentActivity;
using NCSDirectoryApp.Interfaces;
using FFImageLoading.Forms.Platform;
using System;
using FFImageLoading;
using AndroidX.Core.Content;
using AndroidX.Core.App;
using System.Linq;
using Plugin.Permissions;
using NCSDirectoryApp.Utilities;
using Xamarin.Essentials;
using Android.Database;
using Android.Widget;
using Android.Provider;
using Xamarin.Forms;
using System.Collections.Generic;
using Android.Content;
using NCSDirectoryApp.Droid.DependencyServices;
using System.Collections.ObjectModel;
using NCSDirectoryApp.Models;
using Android;

namespace NCSDirectoryApp.Droid
{
    [Activity(Label = "NCS Canada", Icon = "@mipmap/ic_launcher", RoundIcon = "@mipmap/ic_launcher",
        Theme = "@style/MainTheme", MainLauncher = false, LaunchMode =LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation =ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static int OPENGALLERYCODE = 123;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            AppCompatDelegate.DefaultNightMode = AppCompatDelegate.ModeNightNo;
            base.OnCreate(savedInstanceState);
            NativeMedia.Platform.Init(this, savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.Camera) != Permission.Granted
                || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.ReadExternalStorage) != Permission.Granted
                || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.WriteExternalStorage) != Permission.Granted
                || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.AccessFineLocation) != Permission.Granted
                || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.AccessCoarseLocation) != Permission.Granted
                || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.Internet) != Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new System.String[] { Android.Manifest.Permission.Camera,
                    Android.Manifest.Permission.ReadExternalStorage,
                    Android.Manifest.Permission.WriteExternalStorage,
                    Android.Manifest.Permission.Internet,
                    Android.Manifest.Permission.AccessCoarseLocation}, 1);
            }
            else
                App.isLocationPermission = true;
            if (PackageManager.CheckPermission(Manifest.Permission.ReadExternalStorage, PackageName) != Permission.Granted
            && PackageManager.CheckPermission(Manifest.Permission.WriteExternalStorage, PackageName) != Permission.Granted)
            {
                var permissions = new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage };
                RequestPermissions(permissions, 1);
            }
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Xamarin.FormsMaps.Init(this, savedInstanceState);
            Rg.Plugins.Popup.Popup.Init(this);
            Acr.UserDialogs.UserDialogs.Init(this);
            CachedImageRenderer.Init(true);
            CachedImageRenderer.InitImageViewHandler();
            var config = new FFImageLoading.Config.Configuration()
            {
                VerboseLogging = false,
                VerbosePerformanceLogging = false,
                VerboseMemoryCacheLogging = false,
                VerboseLoadingCancelledLogging = false,
                Logger = new CustomLogger(),
            };
            ImageService.Instance.Initialize(config);
            Xamarin.Forms.DependencyService.Get<IStatusBar>().SetLightStatusBarColor();
            App.DeviceHeight = (double)Resources.DisplayMetrics.HeightPixels / (double)Resources.DisplayMetrics.Density;
            App.DeviceWidth = (double)Resources.DisplayMetrics.WidthPixels / (double)Resources.DisplayMetrics.Density;
            VersionTracking.Track();
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            for (int i = 0; i < permissions.Count(); i++)
            {
                if (permissions[i].ToString() == "android.permission.CAMERA")
                {
                    if (grantResults.Length > 0 && grantResults[i] == Permission.Granted)
                    {
                        //App.isCamera = true;
                    }
                }
                if (permissions[i].ToString() == "android.permission.WRITE_EXTERNAL_STORAGE")
                {
                    if (grantResults.Length > 0 && grantResults[i] == Permission.Granted)
                    {
                        //App.storage = true;
                    }
                }
                if (permissions[i].ToString() == "android.permission.READ_EXTERNAL_STORAGE")
                {
                    if (grantResults.Length > 0 && grantResults[i] == Permission.Granted)
                    {
                        //App.storage = true;
                    }
                }
                if (permissions[i].ToString() == "android.permission.INTERNET")
                {
                    if (grantResults.Length > 0 && grantResults[i] == Permission.Granted)
                    {
                        //App.storage = true;
                    }
                }
                if (permissions[i].ToString() == "android.permission.ACCESS_COARSE_LOCATION")
                {
                    if (grantResults.Length > 0 && grantResults[i] == Permission.Granted)
                    {
                        App.isLocationPermission = true;
                    }
                }
            }
        }
        public class CustomLogger : FFImageLoading.Helpers.IMiniLogger
        {
            public void Debug(string message)
            {
            }

            public void Error(string errorMessage)
            {
            }

            public void Error(string errorMessage, Exception ex)
            {
                Error(errorMessage + System.Environment.NewLine + ex.ToString());
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Constants.SaveUserData(null);
            Constants.SetIsLogin(false);
            Constants.SaveRegisterUserId(null);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (NativeMedia.Platform.CheckCanProcessResult(requestCode, resultCode, data))
                NativeMedia.Platform.OnActivityResult(requestCode, resultCode, data);
            base.OnActivityResult(requestCode, resultCode, data);
            
            if (requestCode == OPENGALLERYCODE && resultCode == Result.Ok)
            {
                ObservableCollection<BusinessPhotos> images = new ObservableCollection<BusinessPhotos>();

                if (data != null)
                {
                    ClipData clipData = data.ClipData;
                    if (clipData != null)
                    {
                        if(clipData.ItemCount <= 5)
                        {
                            for (int i = 0; i < clipData.ItemCount; i++)
                            {
                                ClipData.Item item = clipData.GetItemAt(i);
                                Android.Net.Uri uri = item.Uri;
                                var path = GetRealPathFromURI(uri);

                                if (path != null)
                                {
                                    //Rotate Image
                                    var imageRotated = ImageHelpers.RotateImage(path);
                                    var newPath = ImageHelpers.SaveFile("TmpPictures", imageRotated, System.DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                    images.Add(new BusinessPhotos()
                                    {
                                        FilePath = newPath,
                                        UploadedImageSource = newPath
                                    });
                                }
                            }
                            MessagingCenter.Send<ObservableCollection<BusinessPhotos>>(images, "ImagesSelected");
                        }
                        else
                            Toast.MakeText(Xamarin.Forms.Forms.Context, "You can upload max 5 images", ToastLength.Long).Show();
                    }
                    else
                    {
                        Android.Net.Uri uri = data.Data;
                        var path = GetRealPathFromURI(uri);

                        if (path != null)
                        {
                            //Rotate Image
                            var imageRotated = ImageHelpers.RotateImage(path);
                            var newPath = ImageHelpers.SaveFile("TmpPictures", imageRotated, System.DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                            images.Add(new BusinessPhotos() { FilePath = newPath, UploadedImageSource = newPath });
                        }
                        MessagingCenter.Send<ObservableCollection<BusinessPhotos>>(images, "ImagesSelected");
                    }

                    
                }
                
            }
        }



        public String GetRealPathFromURI(Android.Net.Uri contentURI)
        {
            try
            {
                ICursor imageCursor = null;
                string fullPathToImage = "";

                imageCursor = ContentResolver.Query(contentURI, null, null, null, null);
                imageCursor.MoveToFirst();
                int idx = imageCursor.GetColumnIndex(MediaStore.Images.ImageColumns.Data);

                if (idx != -1)
                {
                    fullPathToImage = imageCursor.GetString(idx);
                }
                else
                {
                    ICursor cursor = null;
                    var docID = DocumentsContract.GetDocumentId(contentURI);
                    var id = docID.Split(':')[1];
                    var whereSelect = MediaStore.Images.ImageColumns.Id + "=?";
                    var projections = new string[] { MediaStore.Images.ImageColumns.Data };

                    cursor = ContentResolver.Query(MediaStore.Images.Media.InternalContentUri, projections, whereSelect, new string[] { id }, null);
                    if (cursor.Count == 0)
                    {
                        cursor = ContentResolver.Query(MediaStore.Images.Media.ExternalContentUri, projections, whereSelect, new string[] { id }, null);
                    }
                    var colData = cursor.GetColumnIndexOrThrow(MediaStore.Images.ImageColumns.Data);
                    cursor.MoveToFirst();
                    fullPathToImage = cursor.GetString(colData);
                }
                return fullPathToImage;
            }
            catch (Exception ex)
            {
                Toast.MakeText(Xamarin.Forms.Forms.Context, "Unable to get path", ToastLength.Long).Show();

            }

            return null;

        }

    }
}