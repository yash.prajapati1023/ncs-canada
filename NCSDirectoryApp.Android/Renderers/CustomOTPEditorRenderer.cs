﻿using System;
using Android.Content;
using NCSDirectoryApp.Droid.Renderers;
using NCSDirectoryApp.Views.CustomViews;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomOtpEditor), typeof(CustomOTPEditorRenderer))]
namespace NCSDirectoryApp.Droid.Renderers
{
    public class CustomOTPEditorRenderer : EditorRenderer
    {
        public CustomOTPEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SetCursorVisible(false);
                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}