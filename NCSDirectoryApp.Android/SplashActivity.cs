﻿using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using NCSDirectoryApp.Interfaces;
using Plugin.CurrentActivity;

namespace NCSDirectoryApp.Droid
{
    [Activity(Label = "NCS Canada", MainLauncher = true, Icon = "@mipmap/ic_launcher", RoundIcon = "@mipmap/ic_launcher",
        Theme = "@style/SplashTheme", ScreenOrientation = ScreenOrientation.Portrait,
        NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Xamarin.Forms.DependencyService.Get<IStatusBar>().SetLightStatusBarColor();
            SetContentView(Resource.Layout.SplashLayout);
            // Create your application here
            ThreadPool.QueueUserWorkItem(o => LoadActivity());
        }
        //async protected override void OnResume()
        //{
        //    base.OnResume();
        //    await Task.Delay(2000);
        //    Intent objHome = new Intent(this, typeof(MainActivity));
        //    StartActivity(objHome);
        //}

        void LoadActivity()
        {
            Thread.Sleep(3000);

            RunOnUiThread(() => StartActivity(typeof(MainActivity)));
        }
    }
}
